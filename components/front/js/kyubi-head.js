$(document).ready(function() {
    $(document).ajaxStart(function() {
        $("#overlay").show();
    });
    $(document).ajaxStop(function() {
        $("#overlay").hide();
    });
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        }
    });
    $.extend({
        root: function() {
            return $('meta[name="root_url"]').attr('content');
        },
        postformdata: function(url, formdata) {
            data = $.ajax({
                url: url,
                type: "POST",
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                error: function(jq, status, message) {
                    alert('Sorry There a problem with your request');
                }
            });
            return data;
        },
        postdata: function(url, formdata) {
            data = $.ajax({
                url: url,
                type: "POST",
                data: formdata
            });
            return data;
        },
        getdata: function(url, data) {
            data = $.ajax({
                url: url,
                type: "GET",
                data: data
            });
            return data;
        },
        validemail: function(email) {
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            return pattern.test(email);
        },
        settimeoutalert: function() {
            window.setTimeout(function() {
                $(".alert alert-remove").fadeTo(1500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 2000);
        },
        growl: function() {
            grw = $('.growl-alert');
            if (grw.length > 0) {
                $.bootstrapGrowl(grw.data('message'), {
                    ele: 'body',
                    type: grw.data('type'),
                    offset: {
                        from: 'top',
                        amount: 100
                    },
                    align: 'right',
                    width: 250,
                    delay: 5000,
                    allow_dismiss: true,
                    stackup_spacing: 10
                });
            }
        },
        growl_alert: function(text, type) {
            $.bootstrapGrowl(text, {
                ele: 'body',
                type: type,
                offset: {
                    from: 'top',
                    amount: 100
                },
                align: 'right',
                width: 250,
                delay: 5000,
                allow_dismiss: true,
                stackup_spacing: 10
            });
            $.settimeoutalert();
        },
        delay: (function() {
            var timer = 0;
            return function(callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })(),
        select2: function() {
            $(".select2").select2({
                placeholder: "--Select One--"
            });
        }
    });
    $.growl();
    var miniCartContainer = $('.open-cart-popup');
    var miniCart = $('.cart-box > .popup-container > .mini-cart');
    var totalCart = $('.total-cart');
    var cartParts = $('.cart-parts');
    var homeParts = $('.home-parts');
    var needLoad = 0;
    $.extend({
        coreCart: function() {
            $(document).on('click', '.add-cart', function() {
                that = this;
                var productAttr = $('.product-attr.active');
                var productQty = $('.product-qty').val();
                var productId = $(that).data('id');
                var qty = productQty;
                var attr = [];
                var weight = $('.weight').data('value');
                $('.load-container').fadeIn();
                productAttr.each(function(i, v) {
                    var attrId = $(this).data('id');
                    attr.push(attrId);
                });
                var data = {
                    'id': productId,
                    'attr': attr,
                    'qty': qty,
                    'weight': weight
                };
                $(that).attr('disabled', true);
                $.postdata($.root() + 'v1/add-cart', data).success(function(res) {
                    if (res.result.status == 'available') {
                        $('.load-container').fadeOut();
                        $.emptyMiniCart();
                        $.setTotalCart();
                        $("html, body").animate({
                            scrollTop: 0
                        }, 600);
                        setTimeout(function() {
                            $(miniCartContainer).trigger('mouseover');
                        }, 1000);

                         setTimeout(function() {
                            location.reload();
                        }, 1800);
                    }
                    $('.load-container').fadeOut();
                    $.growl_alert(res.result.text, res.result.type);
                    $(that).attr('disabled', false);
                }).error(function(res) {
                    $('.loader-custom').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                    $(that).attr('disabled', false);
                });
            });
            $(document).on('click', '.add-cart-front', function() {
                that = this;
                var productId = $(that).data('id');
                var producttarget = $(that).data('name');
                var productAttr = $('.product-attr_'+producttarget+'_'+productId);
                var productQty = $('.product-qty').val();
                var qty = productQty;
                var attr = [];
                var weight = $('.weight').data('value');
                $('.load-container').fadeIn();
                productAttr.each(function(i, v) {
                    var attrId = $(this).data('id');
                    attr.push(attrId);
                });
                var data = {
                    'id': productId,
                    'attr': attr,
                    'qty': qty,
                    'weight': weight
                };
                $(that).attr('disabled', true);
                $.postdata($.root() + 'v1/add-cart', data).success(function(res) {
                    if (res.result.status == 'available') {
                        $('.load-container').fadeOut();
                        $.emptyMiniCart();
                        $.setTotalCart();
                        $("html, body").animate({
                            scrollTop: 0
                        }, 600);

                        setTimeout(function() {
                            $(miniCartContainer).trigger('mouseover');
                        }, 1000);

                        setTimeout(function() {
                            location.reload();
                        }, 1800);

                    }
                    $('.load-container').fadeOut();
                    $.growl_alert(res.result.text, res.result.type);
                    $(that).attr('disabled', false);
                }).error(function(res) {
                    $('.loader-custom').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                    $(that).attr('disabled', false);
                });
            });
            $(document).on('click', '.remove-mini-cart', function() {
                var id = $(this).data('id');
                var data = {
                    'id': id
                };
                $('.mini-cart-loader').show();
                $.postdata($.root() + 'v1/remove-cart', data).success(function(res) {
                    $.emptyMiniCart();
                    $.setTotalCart();
                }).error(function(res) {
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('click', '.remove-cart', function() {
                var id = $(this).data('id');
                var data = {
                    'id': id
                };
                $('.loader-custom').fadeIn();
                $.postdata($.root() + 'v1/remove-cart', data).success(function(res) {
                    $('.loader-custom').fadeOut();
                    $.setTotalCart();
                    $.cartParts();
                }).error(function(res) {
                    $('.loader-custom').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('click', '.home_product_icon_n', function() {
                var id = $(this).data('id');
                 var data = {
                    'id': id
                };

                $('.load-container').fadeIn();

                $.postdata($.root() + 'v1/add_wish', data).success(function(res) {
                    $(this).removeClass('home_product_icon_n');
                    $(this).addClass('home_product_icon_y');
                    $('.load-container').fadeOut();
                    $.growl_alert("Add success");

                    setTimeout(function() {
                        location.reload();
                    }, 600);
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('click', '.home_product_icon_y', function() {
                var id = $(this).data('id');
                 var data = {
                    'id': id
                };

                $('.load-container').fadeIn();

                $.postdata($.root() + 'v1/remove_wish', data).success(function(res) {
                    $(this).removeClass('home_product_icon_y');
                    $(this).addClass('home_product_icon_n');
                    $('.load-container').fadeOut();
                    $.growl_alert("Remove success");
                    setTimeout(function() {
                        location.reload();
                    }, 600);
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('click', '.minus-product', function() {
                var parent = $(this).parent();
                var qty = $('.qty-product').val();
                if (qty > 0) {
                   $('.qty-product').val(qty);
                    var id = $(this).data('id');
                    $('.load-container').fadeIn();

                    $.delay(function() {
                        var update_qty = qty;
                        var data = {
                            'id': id,
                            'qty': update_qty,
                        };

                        $.postdata($.root() + 'v1/update-cart', data).success(function(res) {
                            $.cartParts();
                            $.setTotalCart();
                            $('.load-container').fadeOut();
                            $.growl_alert("Update Success");
                        }).error(function(res) {
                            $('.load-container').fadeOut();
                            $.growl_alert("Sorry, there a problem with your request");
                        });
                    }, 1000);
                }
            });
            $(document).on('click', '.plus-product', function() {
                var parent = $(this).parent();
                var qty = $('.qty-product').val();
                var id = $(this).data('id');

                $('.load-container').fadeIn();

                $.delay(function() {
                    var update_qty = qty;
                    var data = {
                        'id': id,
                        'qty': update_qty,
                    };

                    $.postdata($.root() + 'v1/update-cart', data).success(function(res) {
                        if(res.result.status == 'no_qty'){
                            $.cartParts();
                            $.setTotalCart();
                            $('.load-container').fadeOut();
                            $.growl_alert("Stock not enough.", "danger");
                        }else{
                            $.cartParts();
                            $.setTotalCart();
                            $('.load-container').fadeOut();
                            $.growl_alert("Update Success");
                        }
                        
                    }).error(function(res) {
                        $('.load-container').fadeOut();
                        $.growl_alert("Sorry, there a problem with your request");
                    });
                }, 1000);
            });
            $(document).on('keydown', '.checkout-notes', function() {
                var that = $(this);
                $.delay(function() {
                    var notes = that.val();
                    data = {
                        'notes': notes
                    };
                    $.postdata($.root() + 'v1/update-checkout-notes', data).success(function(res) {});
                }, 1000);
            });
            if ($('.choose-delivery-address-type').val() == 'existing') {
                $('.delivery-address').removeAttr('required');
            }
            if ($('.choose-billing-address-type').val() == 'same') {
                $('.billing-address').removeAttr('required');
            }
            $(document).on('click', '.choose-delivery-address-type', function() {
                var id = $(this).data('id');
                var action = $(this).val();
                var data = {
                    'id': id,
                    'action': action
                };
                $.postdata($.root() + 'v1/update-delivery-address', data).success(function(res) {
                    if (action == 'existing') {
                        $('.delivery-address').removeAttr('required');
                        $('.new-delivery-address').slideUp();
                    } else {
                        $('.delivery-address').attr('required', true);
                        $('.new-delivery-address').slideDown();
                    }
                });
            });
            $(document).on('click', '.choose-billing-address-type', function() {
                var action = $(this).is(':checked');
                var data = {
                    'action': action
                };
                if (action == false) {
                    $('.billing-address').attr('required', true);
                    $('.new-billing-address').slideDown();
                } else {
                    $('.billing-address').removeAttr('required');
                    $('.new-billing-address').slideUp();
                }
            });
            $(document).on('change', '.get-city', function(e, ret) {
                var target = $(this).data('target');
                var id = $(this).val();
                var temp = '<option value="">Select Your City</option>'
                var data = {
                    id: id
                };
                // var province_name = $('.chk_province  :selected').data('name');
                // console.log(province_name);

                $('.load-container').fadeIn();
                $.getdata($.root() + 'v1/get-city', data).success(function(res) {
                    var city = res.city;
                    city.rajaongkir.results.forEach(function(v, i) {
                        temp += '<option value="' + v.city_id + '" data-name="' + v.city_name + '">' + v.city_name + '</option>';
                    });
                    $('.' + target).html(temp);
                    $('.load-container').fadeOut();

                    // $('#chk_province_name').val();
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            // $(document).on('change', '.get-subdistrict', function(e, ret) {
            //     var target = $(this).data('target');
            //     var id = $(this).val();
            //     var temp = '<option value=""></option>'
            //     var data = {
            //         id: id
            //     };
            //     $('.load-container').fadeIn();
            //     $.getdata($.root() + 'v1/get-subdistrict', data).success(function(res) {
            //         var sub_district = res.sub_district;
            //         sub_district.forEach(function(v, i) {
            //             temp += '<option value="' + v.sub_district_id + '">' + v.name + '</option>';
            //         });
            //         $('.' + target).html(temp);
            //         if (ret) {
            //             $('.load-container').fadeOut();
            //             $('.' + target).val(ret.sub_district_id).trigger('change');
            //         } else {
            //             $('.load-container').fadeOut();
            //             $('.' + target).select2('val', sub_district[0].sub_district_id);
            //         }
            //     }).error(function(res) {
            //         $('.load-container').fadeOut();
            //         $.growl_alert("Sorry, there a problem with your request");
            //     });
            // });
            $(document).on('change', '.get-sub_district', function(e, ret) {
                var target = $(this).data('target');
                var id = $(this).val();
                var temp = '<option value="">Select Your Sub District</option>'
                var data = {
                    id: id
                };
                var chk_city_1 = $('.chk_city_1 option:selected').text();
                // var city_name = $('.get-sub_district option:selected').text();
                $('#chk_city_name').val(chk_city_1);
                $('.load-container').fadeIn();
                $.getdata($.root() + 'v1/get-subdistrict', data).success(function(res) {
                    var subdistrict = res.subdisctrict;
                    subdistrict.rajaongkir.results.forEach(function(v, i) {
                        temp += '<option value="' + v.subdistrict_id + '" data-name="' + v.subdistrict_name + '">' + v.subdistrict_name + '</option>';
                    });
                    $('.' + target).html(temp);
                    $('.load-container').fadeOut();
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('change', '.get-courier', function(e) {
                var target = $(this).data('target');
                var id = $('.city-delivery').val();
                var weight = $('.ch_weight').val();
                var temp1 = '<option value="">Select Your Courier</option>'
                var data = {
                    'id': id,
                    'weight': weight
                };

                $('.load-container').fadeIn();
                $.getdata($.root() + 'v1/get-courier', data).success(function(res) {
                    var courier = res.courier;
                    $.each(courier, function(index, value) {
                        temp1 += '<option class="chk_courier" value="' + value.cost.value + '" data-name="' + value.code.toUpperCase() + ' - ' + value.name + '">' + value.code.toUpperCase() + ' - ' + value.name + '</option>';
                        $('.' + target).html(temp1);
                        $('.load-container').fadeOut();
                    });
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('change', '.get-Nsub_district', function(e, ret) {
                var target = $(this).data('target');
                var id = $(this).val();
                var temp = '<option value="">Select Your Sub District</option>'
                var data = {
                    id: id
                };
                $('.load-container').fadeIn();
                $.getdata($.root() + 'v1/get-subdistrict', data).success(function(res) {
                    var subdistrict = res.subdisctrict;
                    subdistrict.rajaongkir.results.forEach(function(v, i) {
                        temp += '<option value="' + v.subdistrict_id + '" data-name="' + v.subdistrict_name + '">' + v.subdistrict_name + '</option>';
                    });
                    $('.' + target).html(temp);
                    $('.load-container').fadeOut();
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('change', '.get-Ncity', function(e, ret) {
                var target = $(this).data('target');
                var id = $(this).val();
                var temp = '<option value="">Select Your City</option>'
                var data = {
                    id: id
                };
                var province_name = $('.get-Ncity option:selected').text();
                $('#chk_province_name').val(province_name);

                $('.load-container').fadeIn();
                $.getdata($.root() + 'v1/get-city', data).success(function(res) {
                    var city = res.city;
                    city.rajaongkir.results.forEach(function(v, i) {
                        temp += '<option value="' + v.city_id + '" data-name="' + v.city_name + '">' + v.city_name + '</option>';
                    });
                    $('.' + target).html(temp);
                    $('.load-container').fadeOut();
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('change', '.get-Ncourier', function(e) {
                var target = $(this).data('target');
                var id = $('.city-Ndelivery').val();
                var weight = $('.ch_weight').val();
                var temp1 = '<option value="">Select Your Courier</option>'
                var data = {
                    'id': id,
                    'weight': weight
                };
                var chk_subdistrict1 = $('.chk_subdistrict1 option:selected').text();
                $('#chk_subdistrict_name').val(chk_subdistrict1);
                $('.load-container').fadeIn();
                $.getdata($.root() + 'v1/get-courier', data).success(function(res) {
                    var courier = res.courier;
                    $.each(courier, function(index, value) {
                        temp1 += '<option class="chk_courier" value="' + value.cost.value + '" data-name="' + value.code.toUpperCase() + ' - ' + value.name + '">' + value.code.toUpperCase() + ' - ' + value.name + '</option>';
                        $('.' + target).html(temp1);
                        $('.load-container').fadeOut();
                    });
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('focusout', '.upd_cart', function(e) {
                var rs_check;
                var AttrId = $(this).data('id');
                var qty = $(this).val();
                $('.loader-custom').fadeIn();
           
                if (qty > 0) {
                    rs_check = 1;
                } else {
                    rs_check = 0;
                }

                if (rs_check == 1) {
                    var data = {
                        'id': AttrId,
                        'qty': qty,
                    };

                    $.postdata($.root() + 'v1/c_update-cart', data).success(function(res) {
                        $.cartParts2(function(result) {
                            $.setTotalCart();
                            $('.loader-custom').fadeOut();
                            
                            if (result == 1) {
                                var respon = res.result;

                                var id = $(this).data('id');
                                if (respon.status == 'failed') {
                                    $('.st_stock' + id).html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>' + respon.name + ' Not Enough, Stock left : '+ respon.qty +'</p></div>');
                                    $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Stock Not Enough, Stock left : '+ respon.qty +'</p></div>');
                                } else{
                                    $('.st_success').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Update Success</p></div>');
                                }
                            }
                        });
                    }).error(function(res) {
                        $('.loader-custom').fadeOut();
                        $.growl_alert("Sorry, there a problem with your request");
                    });
                } else {
                    $('.loader-custom').fadeOut();
                    $("html, body").animate({
                        scrollTop: 0
                    }, 600);
                    $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Minimal Quantity 1</p></div>');
                }
            });
            $(document).on('click', '.check_coupon', function(e) {
                var coupon = $('#ch_coupon').val();
                var data = {
                    'coupon': coupon
                };

                $('.load-container').fadeIn();
                $.postdata($.root() + 'v1/check_coupon', data).success(function(res) {
                    var res = res.result;
                    if (res['status'] == 'failed') {
                        $('.load-container').fadeOut();
                        $('.coupon_message').addClass('form-error');
                        $('.coupon_message').focus();
                        $('.coupon_message').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>' + res['message'] + '</p></div>');
                    } else {
                        $.setTotalCart();
                        $.cartParts();
                        $('.load-container').fadeOut();
                        $("html, body").animate({
                            scrollTop: 0
                        }, 600);
                        $.growl_alert("Coupon Added");
                    }
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('click', '.remove_coupon', function() {
                var id = $(this).data('id');
                var data = {
                    'id': id
                };
                $('.load-container').fadeIn();
           
                $.postdata($.root() + 'v1/remove_coupon', data).success(function(res) {
                    $.setTotalCart();
                    $.cartParts();
                    $('.load-container').fadeOut();
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            $(document).on('click', '.check_cost', function(e) {
                var getcost = $('#ch_courier').val();
                var data = {
                    'value': getcost
                };
                $('.load-container').fadeIn();
                $.getdata($.root() + 'v1/get_cost', data).success(function(res) {
                    var shipping = 'Rp ' + Number(getcost).toLocaleString('en');
                    var result = 'Rp ' + Number(res).toLocaleString('en');
                    $('.load-container').fadeOut();
                    $('.shipping_cost').html(shipping);
                    $('.up_total').html(result);
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });
            // $(document).on('change', '.chk_province', function() {
            //     var val = $('.chk_province  :selected').data('name');
            //     $('#chk_Nprovince').val(val);
            // });
            // $(document).on('change', '.chk_city', function() {
            //     var val = $('.chk_city  :selected').data('name');
            //     $('#chk_Ncity').val(val);
            // });
            // $(document).on('change', '.chk_subdistrict', function() {
            //     var val = $('.chk_subdistrict  :selected').data('name');
            //     $('#chk_Nsubdistrict').val(val);
            // });
            $(document).on('change', '.chk_shipping', function() {
                var val = $('.chk_shipping  :selected').data('name');
                $('#chk_courier').val(val);
            });
            $(document).on('change', '.chk_Nshipping', function() {
                var val = $('.chk_Nshipping  :selected').data('name');
                $('#chk_Ncourier').val(val);
            });
            $(document).on('change', '.chk_payment_method', function() {
                var val = $(this).val();
                var id = $('.chk_payment_method').attr('id');
                // console.log($(this).val());
                if(val == 1 || val == 2){
                    $('.chk_bank').show();
                }else{
                    $('.chk_bank').hide();
                }
                // $('#chk_Ncourier').val(val);
            });
            $(document).on('click', '.choose-address', function() {
                var action = $(this).is(':checked');
                if (action == false) {
                    $('.ch_new_address').show();
                    $('.ch_your_address').hide();
                } else {
                    $('.ch_new_address').hide();
                    $('.ch_your_address').show();
                }
            });
            $(document).on('keyup', '#hd_search', function() {
                var val = $('#hd_search').val();
                var data = {
                    search: val
                };
                var load = '';

                if (val.length >= 1) {
                    load = '  <div class="loader-container"><div class="cssload-spin-box-mini"></div></div>';
                    $('.rs_search').css("display", "block");
                    $('.rs_search').html(load);

                    $.getdata($.root() + 'v1/live_search', data).success(function(res) {
                        data = res.result;
                        var tmp = '';
                        if (res.result.length > 1) {
                            data.forEach(function(v, i) {
                                tmp += '<p class="rs_search_p" id="s_' + i + '" onclick="search_function(' + i + ')">' + v.name + '</p>';
                            });
                        } else {
                            tmp = '<p>No Result</p>'
                        }

                        $('.loader-container').fadeOut();

                        $('.rs_search').css("display", "block");
                        $('.rs_search').html(tmp);
                    }).error(function(res) {
                        tmp = '<p>Sorry, there a problem with your request.</p>'
                        $('.rs_search').css("display", "block");
                        $('.rs_search').html(tmp);
                    });
                }
            });
            $(document).on('change', '#ch_inv', function() {
                var val = $('#ch_inv').val();
                var data = {
                    inv: val
                };
                $('.loader-custom').fadeIn();
                if (val.length == 11) {
                    $.getdata($.root() + 'v1/live_inv', data).success(function(res) {
                        data = res.result;
                        if (data['status'] == "success") {
                            $('#ch_inv').attr('readonly', true);
                            $('#chk_banks').attr('readonly', true);
                            $('#chk_names').val(data['name']);
                            $('#chk_amounts').val(data['amount']);
                            var temp = '<option value="' + data['bank_id'] + '">' + data['bank_name'] + '</option>';
                            $('#chk_banks').html(temp);
                            $('.loader-custom').fadeOut();
                        } else if (data['status'] == "failed") {
                            $('.loader-custom').fadeOut();
                            $('.ch_message').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Nomor Invoice anda Tidak ditemukan</p></div>');
                        }
                    }).error(function(res) {
                        $('.loader-custom').fadeOut();
                        $.growl_alert("Sorry, there a problem with your request");
                    });
                } else if (val.length > 0 && val.length < 11) {
                    $('.loader-custom').fadeOut();
                    $('.ch_message').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Nomor Invoice anda Tidak sesuai</p></div>');
                }
            });
            $(document).mouseup(function(e) {
                var container = $(".rs_search");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                }
            });
            $(document).on('click', '.hm_Qc', function() {
                var id = $(this).data('pr');
                var data = {
                    id: id
                };
                $('.loader-Qc').fadeIn();
                $.getdata($.root() + 'v1/pr_img', data).success(function(res) {
                    $('.loader-Qc').fadeOut();
                    var pmg = res.pr_img;
                    var temp = '';
                    pmg.forEach(function(v, i) {
                        temp += '<div class="swiper-slide"><img src="' + $.root() + '/components/front/images/product/' + id + '/' + v.images_name + '" class="img-responsive img_center" /></div>'
                    });
                    $('.pr_img_' + id).html(temp);
                    var swiper = new Swiper('.swiper-container', {
                        pagination: '.swiper-pagination',
                        direction: 'vertical',
                        paginationClickable: true,
                        nextButton: '.swiper-button-next',
                        slidesPerView: 'auto',
                        spaceBetween: 30,
                        mousewheelControl: true,
                        centeredSlides: true,
                    });
                }).error(function(res) {
                    $('.loader-Qc').fadeOut();
                    $('#modal_' + id).modal('hide');
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });

            $(document).on('change', '.stk_ch', function(){
                var ids = $('.stk_ch :selected').val();

                $('.load-container').fadeIn();

                var data = {
                    'id_pro': ids
                };

                $.postdata($.root() + 'v1/ch_st_pro', data).success(function(res) {
                    if(res.result.status == 'success'){
                        $('.product-attr').removeClass('active');
                        $('.pro_atr_'+ids).addClass('active');

                        if($('.pros_stock').hasClass('product_stock_av')){
                           $('.pros_stock').removeClass('product_stock_av');
                        } else{
                           $('.pros_stock').removeClass('product_stock_hb');
                        }

                        if(res.result.stock_status == 'tersedia'){
                            $('.pros_stock').addClass('product_stock_av');
                            $('.pros_stock').text('Stock Tersedia');
                        } else{
                            $('.pros_stock').addClass('product_stock_hb');
                            $('.pros_stock').text('Stock Habis');
                        }

                        $('.load-container').fadeOut();
                    } else if(res.result.status == 'failed'){
                        $('.load-container').fadeOut();
                        $.growl_alert("Sorry, there a problem with your request");
                    }
                }).error(function(res) {
                    $('.load-container').fadeOut();
                    $.growl_alert("Sorry, there a problem with your request");
                });
            });

            $(document).on('change', '.get-delivery-service-sub', function() {
                var val = $(this).val();
                var data = {
                    id: val
                };
                $.getdata($.root() + 'v1/get-delivery-service-sub', data).success(function(res) {
                    data = res.delivery_service_sub;
                    var tmp = '';
                    data.forEach(function(v, i) {
                        tmp += '<label class="checkbox-entry radio">' + '<input type="radio" name="delivery_service_sub" class="choose-delivery-service-sub" value="' + v.id + '"> <span class="check"></span>' + v.name + '</label>';
                    });
                    $('.delivery-service-sub').html(tmp);
                });
            });
            $(document).on('change', '.choose-delivery-service-sub', function() {
                var val = $(this).val();
                alert(val);
            });
            if ($('.cart-box').length > 0) {
                $(document).on('mouseover', miniCartContainer, function() {
                    if ($(miniCart).is(':empty')) {
                        needLoad += 1;
                    }
                    if (needLoad == 1) {
                        $('.mini-cart-loader').show();
                        setTimeout(function() {
                            $.miniCart();
                        }, 1000);
                    }
                });
            }
        },
        miniCart: function() {
            if ($('.cart-box').length > 0) {
                $.getdata($.root() + 'parts/mini-cart').success(function(res) {
                    $(miniCart).html(res);
                    needLoad = 0;
                    $('.mini-cart-loader').hide();
                }).error(function(err) {
                    needLoad = 0;
                    $.growl_alert("Sorry, there a problem with your request");
                });
            }
        },
        emptyMiniCart: function() {
            $(miniCart).html('');
            needLoad = 0;
        },
        setTotalCart: function() {
            $.getdata($.root() + 'v1/total-cart').success(function(res) {
                $(totalCart).html(res);
                $.miniCart();
            }).error(function(err) {
                $.growl_alert("Sorry, there a problem with your request");
            });
        },
        cartParts: function() {
            $.getdata($.root() + 'parts/cart').success(function(res) {
                cartParts.html(res);
                $('.cart-loader').fadeOut();
            }).error(function(err) {
                $.growl_alert("Sorry, there a problem with your request");
            });
        },
        cartParts2: function(callback) {
            $.getdata($.root() + 'parts/cart').success(function(res) {
                cartParts.html(res);
                $('.cart-loader').fadeOut();
                callback(1);
            }).error(function(err) {
                $.growl_alert("Sorry, there a problem with your request");
            });
        },
    })
    $.coreCart();
    // $.prohover();
    setTimeout(function() {
        $.setTotalCart();
        if ($(cartParts).length > 0) {
            $.cartParts();
        }
    }, 3000);
});