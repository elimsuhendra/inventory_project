<?php namespace digipos\Http\Controllers\Front;

use Request;
use Cache;
use Cookie;
use Hash;
use DB;

use digipos\Libraries\Cart;

use digipos\models\Bank_account;
use digipos\models\Contact;
use digipos\models\Pages;
// use digipos\models\Flash_deal;
use digipos\models\Post;
use digipos\models\Config;
use digipos\models\Customer_cart;
use digipos\models\Socialmedia;
use digipos\models\Payment_method;
use digipos\models\Product;
use digipos\models\Product_category;
use digipos\models\Product_data_category;
use digipos\models\Product_data_attribute;
use digipos\models\Product_attribute_data;
use digipos\models\Product_data_attribute_master;
use digipos\models\Price_rule;
use digipos\models\Voucher;
use digipos\models\Orderhd;
use digipos\models\Ordervoucher;
use digipos\models\Delivery_service;
use digipos\models\Customer_wishlist;

use Illuminate\Support\Facades\Mail;

class ShukakuController extends Controller {

    public function __construct(){
        parent::__construct();        
    }

    public function initData(){
        //Set Global
        $query_config = Config::where('config_type','front')->orwhere('config_type','both');
        $config = $this->cache_query('config',$query_config,'get');
        foreach($config as $c){
            $this->data[$c->name] = $c->value;
            $this->data['email_config'][$c->name] = $c->value;
        }

        $this->data['ctc'] = Contact::select('value', 'image')->get(); 
       
        // $this->data['social_media']     = $this->cache_query('social_media',Socialmedia::where('status','y')->orderBy('order','asc')->select('image','url'),'get');
        $this->data['payment_methods']  = $this->cache_query('payment_method',Payment_method::where('status','y')->orderBy('order','asc')->select('payment_method_name','image'),'get');

        $this->data['product_category'] = Product_category::select('product_category.*')->where('status', 'y')->where('category_id','0')->get();

        $this->data['product_subcategory'] = Product_category::where('status', 'y')->where('category_id','!=','0')->orderBy('category_id')->orderBy('category_name')->get(); 

        $this->data['product_subcategory2'] = Product_category::where('status', 'y')->where('category_id','!=','0')->orderBy('category_id')->orderBy('category_name')->groupBy('category_id')->select(DB::raw('count(*) as category_id_count, category_id'))->get(); 
        // dd($this->data['product_subcategory2']);
    }

    public function generateCartIdentifier(){
        $identifier                     = Cookie::get(env('APP_CART_KEY'));
        if(!$identifier){
            $identifier                 = Hash::make(env('APP_CART_KEY')).str_random(3);
        }else{
            $identifier                 = decrypt($identifier);
        }
        
        $cek_identifier                 = Customer_cart::where('identifier',$identifier)->first();
        if(!$cek_identifier){
            $customer_cart              = new Customer_cart;
            $customer_cart->identifier  = $identifier;
            $customer_cart->customer_id = auth()->guard($this->guard)->check() ? auth()->guard($this->guard)->user()->id : '';
            $customer_cart->expired     = date('Y-m-d H:i:s',strtotime('+1 years -6 days'));
            $customer_cart->save();
            Cookie::queue(Cookie::make(env('APP_CART_KEY'),$identifier,1440*30*12));
        }
    }
    
    /*
    public function generateMeta(){
        $this->data['meta_full_image']                  = asset('components/both/images/web/'.$this->data['web_logo']);                    
        $this->data['facebook_meta']['og:title']        = $this->data['web_title'].' | '.$this->data['web_name'];
        $this->data['facebook_meta']['og:site_name']    = $this->data['web_name'];
        $this->data['facebook_meta']['og:url']          = Request::url();
        $this->data['facebook_meta']['og:type']         = "article";
        $this->data['facebook_meta']['og:locale']       = "id_ID";
        $this->data['facebook_meta']['og:image']        = $this->data['meta_full_image'];
        $this->data['facebook_meta']['og:description']  = $this->data['web_description'];

        $this->data['twitter_meta']['twitter:card']          = "summary_large_image";
        $this->data['twitter_meta']['twitter:site']          = "@".$this->data['web_name'];
        $this->data['twitter_meta']['twitter:creator']       = "@".$this->data['web_name'];
        $this->data['twitter_meta']['twitter:url']           = Request::url();
        $this->data['twitter_meta']['twitter:title']         = $this->data['web_name'];
        $this->data['twitter_meta']['twitter:image']         = $this->data['meta_full_image'];
        $this->data['twitter_meta']['twitter:description']   = $this->data['web_description'];
    } */

    // Custom Controller
    public function generateCusMeta(){
        $default_title = Config::where('name', 'web_title')->first();
        $default_description = Config::where('name', 'web_description')->first();
        $default_keywords = Config::where('name', 'web_keywords')->first();
        
        $this->data['met_title'] = $default_title->value;
        $this->data['met_description'] = $default_description->value;
        $this->data['met_keywords'] = $default_keywords->value;

        $this->data['met_tl'] = '';
        $this->data['met_des'] = '';
        $this->data['met_key'] = '';
    }
    
    public function generateMenu(){
        $this->data['menus'] = Pages::select('pages_name', 'pages_name_alias')->where([['status', 'y'],['header', 'pages']])->orderBy('order', 'asc')->get();
    }

    // public function generateHeader(){
    //     $this->data['main_category'] = product_category::with('product_data_category')->where([['category_id', 0],['status', 'y']])->orderBy('id', 'desc')->limit(6)->get();
    //     $this->data['head_contact'] = Config::where('name', 'phone')->first();
    // }

    public function generateFooter(){
       $this->data['bank_account'] = bank_account::where('status', 'y')->whereIn('id',[1,2])->get();
    }

    public function generateCategory(){
        $get_cat = Product_category::where('status', 'y')->where('category_id', 0)->orderBy('id', 'asc')->get();
        $result = [];

        foreach($get_cat as $q => $gets){
            $category = $this->setCategory($gets->id);

            if($category != ""){
                $result[$q] = [
                            'id'            => $gets->id,
                            'name'          => $gets->category_name,
                            'name_alias'    => $gets->category_name_alias,
                            'sub_category'  => $category,
                          ]; 
            } else{
                $check_pro = Product_data_category::where('product_category_id', $gets->id)->count();

                if($check_pro > 0){
                    $result[$q] = [
                                    'id'            => $gets->id,
                                    'name'          => $gets->category_name,
                                    'name_alias'    => $gets->category_name_alias,
                                    'sub_category'  => $category,
                                  ]; 
                }
            }
        }
        $this->data['category_shop']    = $result;
    }

    public function generateColor(){
        $color     = [
                        '0' => '#0A4A59', 
                        '1' => '#272E42',
                        '2' => '#604168',
                        '3' => '#A31313',
                        '4' => '#2C5B0D',
                        '5' => '#B5600B'
                    ];

        return $color;
    }

    public function setCategory($gets){
        $get_cat = Product_category::join('product_data_category', 'product_category.id', 'product_data_category.product_category_id')->where('product_category.category_id', $gets)->select('product_category.*')->distinct()->get();

        if(count($get_cat) > 0){
            foreach($get_cat as $k => $gets2){
                $category = $this->setSubCategory($gets2->id);

                $result[$k] = [
                        'id'            => $gets2->id,
                        'name'          => $gets2->category_name,
                        'name_alias'    => $gets2->category_name_alias,
                        'sub_sub_category'  => $category,
                      ]; 
            }
        } else{
            $result = '';
        }

        return $result;
    }

    public function setSubCategory($gets){
        $get_cat = Product_category::join('product_data_category', 'product_category.id', 'product_data_category.product_category_id')->where('product_category.category_id', $gets)->select('product_category.*')->distinct()->get();

        if(count($get_cat) > 0){
            foreach($get_cat as $q => $gets){
                $result[$q] = [
                                'id'            => $gets->id,
                                'name'          => $gets->category_name,
                                'name_alias'    => $gets->category_name_alias,
                              ]; 
            }
        } else{
            $result = '';
        }
        return $result;
    }

    public function setCategoryContent(){
        $category = Product_category::select('category_name', 'category_name_alias', 'image')->where([['status', 'y'], ['category_id', 0]])->limit(10)->get();
        return $category;
    }

    public function checkPriceruleProduct($content, $action, $limit){
        $result = '';
        // var_dump($content);
        if(count($content) > 0){
            switch ($action) {
                case "pro_default":
                    $result = NULL;
                    foreach($content as $q => $ct){
                        $price_rule = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $ct->id]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('price_rule.updated_at', 'desc')->first();

                        if($price_rule != null){
                            $r_type     = $price_rule->reduction_type;
                            $r_amount   = $price_rule->reduction_amount;

                            if($r_type == "p"){
                                $get_disc = $r_amount / 100;
                                $res_disc = $ct->price * $get_disc;
                                
                                if($res_disc > $ct->price){
                                    $discount = 0;
                                    $status_disc  = "y";
                                    $discount_type = "p";
                                } else{
                                    $discount = $ct->price - $res_disc;
                                    $status_disc  = "y";
                                    $discount_type = "p";
                                }
                            } else if($r_type == "v"){
                                if($r_amount > $ct->price){
                                    $discount = 0;
                                    $status_disc  = "y";
                                    $discount_type = "v";
                                } else{
                                    $discount = $ct->price - $r_amount;
                                    $status_disc  = "y";
                                    $discount_type = "v";
                                }
                            }

                        } else{
                            $discount     = "";
                            $status_disc  = "n";
                            $r_amount = "";
                            $discount_type = "";
                        }

                        $category = Product_data_attribute::join('product_attribute_data', 'product_data_attribute.product_attribute_data_id', 'product_attribute_data.id')->where('product_data_attribute.product_data_attribute_master_id', $ct->pdm)->select('product_attribute_data.id')->get();
                        $cate = [];

                        foreach($category as $cats){
                            array_push($cate, $cats->id);
                        }

                        $wishlist = $this->get_wish($ct->id);

                        $temp = [
                                    'id'                => $ct->id,
                                    'name'              => $ct->name,
                                    'name_alias'        => $ct->name_alias,
                                    'image'             => $ct->image,
                                    'category'          => $ct->category_name,
                                    'attr_id'           => $cate,
                                    'weight'            => $ct->weight,
                                    'price'             => $ct->price,
                                    'discount'          => $discount,
                                    'discount_amount'   => $r_amount,
                                    'discount_status'   => $status_disc,
                                    'discount_type'     => $discount_type,
                                    'status_wishlist'   => $wishlist,
                                ];

                        $result[] = (object)$temp;
                    } 

                    return $result;
                    break;
                case "promo":
                    $offset = 0;
                    $page = 1;
                    $per_page = 1;

                    if($content > 0){
                        $check_rl = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule_product.id_product')->orderBy('price_rule.updated_at', 'desc')->first();

                        $check_rl_zero = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")], ['price_rule_product.id_product', 0]])->select('price_rule_product.id_product', 'price_rule.updated_at')->orderBy('price_rule.updated_at', 'desc')->first();

                        if($check_rl->id_product == 0){
                            $product_promo = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y']])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
                            $product_promo_ac = "pro_default";

                            $count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

                            $count = $count[0]->count;

                            $result = $this->checkPriceruleProduct($product_promo, $product_promo_ac, $limit);
                            break;
                        } else{
                            $arr_rule = [];
                            $arr_not_rule = [];
                            $arr_join = [];
                            $check_ar_l = 0;

                            $get_id_promo = DB::select(DB::raw("select bkm_price_rule_product.id_product from bkm_price_rule inner join bkm_price_rule_product on bkm_price_rule_product.id_price_rule = bkm_price_rule.id where bkm_price_rule.status = 'y' and bkm_price_rule.type = 'discount' and bkm_price_rule.valid_from <= '".date("Y-m-d h:i:s")."' and bkm_price_rule.valid_to >= '".date("Y-m-d h:i:s")."' and bkm_price_rule.updated_at > '".$check_rl_zero->updated_at."' order by bkm_price_rule.updated_at desc limit ".$limit." offset ".$per_page." "));

                            $count_prom = count($get_id_promo);
                            $cal = $page * $limit;
                            $cal1 = $cal - $count_prom;

                            if($count_prom >= $cal){
                                foreach($get_id_promo as $gt_id_pr){
                                    array_push($arr_rule, $gt_id_pr->id_product);
                                }

                                $arr_rule = implode(',', $arr_rule);

                                $product_promo = DB::select(DB::raw("select bkm_product.*, bkm_product_category.category_name, bkm_product_data_attribute_master.id as pdm from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_product_category on bkm_product_category.id = bkm_product.product_category_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id in (".$arr_rule.") group by bkm_product_data_attribute_master.product_id order by field (bkm_product.id, ".$arr_rule.") ")); 
                            } else{
                                if($cal1 > 0){
                                    $cal2 = $cal1 - $limit;

                                    if($cal2 > 0){
                                        $cus_offset = $cal2 - 1;
                                        $get_product = DB::select(DB::raw("select distinct bkm_product.id from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id not in ((select bkm_price_rule_product.id_product from bkm_price_rule inner join bkm_price_rule_product on bkm_price_rule_product.id_price_rule = bkm_price_rule.id where bkm_price_rule.status = 'y' and bkm_price_rule.type = 'discount' and bkm_price_rule.valid_from <= '".date("Y-m-d h:i:s")."' and bkm_price_rule.valid_to >= '".date("Y-m-d h:i:s")."' and bkm_price_rule.updated_at > '".$check_rl_zero->updated_at."'  order by bkm_price_rule.updated_at desc)) order by bkm_product.created_at desc limit ".$limit." offset ".$cus_offset." "));

                                        foreach($get_product as $gt_pro){
                                            array_push($arr_not_rule, $gt_pro->id); 
                                        }

                                        $arr_not_rule = implode(',', $arr_not_rule);

                                        $product_promo = DB::select(DB::raw("select bkm_product.*, bkm_product_category.category_name, bkm_product_data_attribute_master.id from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_product_category on bkm_product_category.id = bkm_product.product_category_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id in (".$arr_not_rule.") group by bkm_product_data_attribute_master.product_id order by field (bkm_product.id, ".$arr_not_rule.") ")); 
                                    } else if($cal2 < 0){
                                        $cus_limit = $limit + $cal2;

                                        $get_product = DB::select(DB::raw("select distinct bkm_product.id from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id not in ((select bkm_price_rule_product.id_product from bkm_price_rule inner join bkm_price_rule_product on bkm_price_rule_product.id_price_rule = bkm_price_rule.id where bkm_price_rule.status = 'y' and bkm_price_rule.type = 'discount' and bkm_price_rule.valid_from <= '".date("Y-m-d h:i:s")."' and bkm_price_rule.valid_to >= '".date("Y-m-d h:i:s")."' and bkm_price_rule.updated_at > '".$check_rl_zero->created_at."'  order by bkm_price_rule.updated_at desc)) order by bkm_product.created_at desc limit ".$cus_limit."  offset 0"));

                                        foreach($get_id_promo as $gt_id_pr){
                                            array_push($arr_rule, $gt_id_pr->id_product);
                                        }

                                        foreach($get_product as $gt_pro){
                                            array_push($arr_not_rule, $gt_pro->id); 
                                        }

                                        $arr_join = array_merge($arr_rule,$arr_not_rule);
                                        $arr_join = implode(',', $arr_join);

                                        $product_promo = DB::select(DB::raw("select bkm_product.*, bkm_product_category.category_name, bkm_product_data_attribute_master.id as pdm from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_product_category on bkm_product_category.id = bkm_product.product_category_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id in (".$arr_join.") group by bkm_product_data_attribute_master.product_id order by field (bkm_product.id, ".$arr_join.") ")); 
                                    }
                                } 
                            }

                            $product_promo_ac = "pro_default";
                            $count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

                            $count = $count[0]->count;

                            $result = $this->checkPriceruleProduct($product_promo, $product_promo_ac, $limit);
                        }
                    } else{
                        $product_promo = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('price_rule_product', 'price_rule_product.id_product', 'product.id')->join('price_rule', 'price_rule_product.id_price_rule', 'price_rule.id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y'], ['price_rule.status', 'y'], ['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
                        $product_promo_ac = "pro_default";

                        $count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_price_rule_product on bkm_price_rule_product.id_product = bkm_product.id inner join bkm_price_rule on bkm_price_rule_product.id_price_rule = bkm_price_rule.id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_price_rule.status = 'y' and bkm_price_rule.type = 'discount' and bkm_price_rule.valid_from <= '".date("Y-m-d h:i:s")."' and bkm_price_rule.valid_to >= '".date("Y-m-d h:i:s")."' GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

                        $count = $count[0]->count;

                        $result = $this->checkPriceruleProduct($product_promo, $product_promo_ac, $limit);
                    }

                    return $result;
                    break;
                default:
                    return $result;
            }
        } else{
            return $result;
        }
    }

    public function get_wish($ids){
        if(auth()->guard($this->guard)->check()){
            $id = auth()->guard($this->guard)->user()->id;
            $get_wish = Customer_wishlist::select('product_id')->where('customer_id', $id)->first();

            if(count($get_wish) > 0){
                $conv_wish = json_decode($get_wish->product_id);

                if(count($conv_wish) > 0){
                    if(in_array($ids, $conv_wish)){
                        $wishlist = 'y';
                    } else{
                        $wishlist = 'n';
                    }
                } else{
                    $wishlist = 'n';
                }
            } else{
                $wishlist = 'n';
            }
        } else{
            $wishlist = 'n';
        }

        return $wishlist;
    }


    public function goes_well(){
        // $disco = [];
        // $product = Product::join('product_data_attribute_master', 'product.id', 'product_data_attribute_master.product_id')->select('product.*', 'product_data_attribute_master.stock')->where([['product.status', 'y'],['product_data_attribute_master.stock', '>', 0]])->with('thumbnail')->inRandomOrder()->limit(5)->get();

        // foreach($product as $q => $pro){
        //     $disc = Discount::select('product_id', 'amount')->where([['product_id', $pro->id], ['status', 'y'],['valid_from', '<=', date("Y-m-d")],['valid_to', '>=', date("Y-m-d")]])->first();

        //     if(count($disc) > 0){
        //         $disco[$q] = [
        //                         'product_id'        => $disc->product_id,
        //                         'discount_amount'   => $disc->amount,
        //                      ];
        //     }
        // }

        // if(count($disco) > 0){
        //     $this->data['discount'] = $disco;
        // } else{
        //     $this->data['discount'] = '';
        // }

        // $count_product = count($product);

        // $this->data['goes_well'] = $product;

        // $this->data['attr'] = product::where('status', 'y')->with('product_data_attribute_master.product_data_attribute.product_attribute_data')->get();    
    }
    // End Custom Controller

    public function cache_query($name,$query,$type='',$time = 60){
        $c = Cache::remember($name, $time, function() use($query,$type){
            if (!empty($type)){
                if ($type == 'first'){
                    $q = $query->first();
                }else{
                    $q = $query->get();
                }
                return $q;
            }else{
                return $query;
            }
        });
        //Cache::flush();
        return $c;
    }

       public function generate_product_attribute($product){
        $temp_data              = [];
        foreach($product->product_data_attribute_master as $pda){
            foreach($pda->product_data_attribute as $pd){
                $product_attribute_data             = $pd->product_attribute_data;
                $product_attribute_data_attribute   = $pd->product_attribute_data->attribute;
                $id             = $product_attribute_data->id;
                $name             = $product_attribute_data->name;
                $value          = $product_attribute_data->value;
                $order          = $product_attribute_data->order;
                $attribute_name = $product_attribute_data_attribute->attribute_name;
                $attribute_type = $product_attribute_data_attribute->attribute_type;
                if(!array_key_exists($attribute_name, $temp_data)){
                    $temp_data[$attribute_name]         = [
                                                            'name'  => $attribute_name,
                                                            'type'  => $attribute_type,
                                                            'order' => $product_attribute_data_attribute->order
                                                        ];
                }

                if(array_key_exists('data', $temp_data[$attribute_name])){
                    $cus_ch = 0;
                    foreach($temp_data[$attribute_name]['data'] as $ada){
                        if($ada['value'] == $value){
                            $cus_ch++;
                            break;
                        }

                        if($cus_ch == 0){
                            $temp_data[$attribute_name]['data'][]   = ['id' => $id,'name'=>$name,'value' => $value,'order' => $order];    
                        }
                    }
                }else{
                    $temp_data[$attribute_name]['data'][]       = ['id' => $id,'name'=>$name,'value' => $value,'order' => $order];
                }
            }
        }

        $temp_data  = array_values(array_sort($temp_data, function ($value) {
            return $value['order'];
        }));
        foreach($temp_data as $q => $t){
            $temp_data[$q]['data']  = array_values(array_sort($t['data'], function ($value) {
                return $value['order'];
            }));
        }
        return $temp_data;
    }

    public function check_stock($request){
        $id         = $request['id'];
        $attr       = $request['attr'];
        $qty        = $request['qty'];
        $action     = $request['action'] ?? '';

        $product    = Product::where('id',$id)->with('product_data_attribute_master.product_data_attribute')->first();

        if(!$product){
            return ['status' => 'no_product'];
        }


        foreach($product->product_data_attribute_master as $pda){
            $product_data_attribute     = $pda->product_data_attribute->pluck('product_attribute_data_id')->toArray();
            $check                      = array_diff($attr,$product_data_attribute);
            if(count($check) == 0){
                $request->request->add(['product_data_attribute_master_id' => $pda->id]);
                if($action == 'add'){
                    $cur_qty            = Cart::get_qty($pda->id);
                    $qty                += (int)$cur_qty;
                }else if($action == 'update'){
                    $qty                = (int)$qty;
                }

                $product_qty            = $pda->stock;
                if($product_qty == 0){ 
                    return ['status' => 'out_of_stock'];
                }else{
                    if($qty > $product_qty){
                        $res    = ['status' => 'no_qty','qty' => $product_qty];
                    }else{
                        $res    = ['status' => 'available','qty' => $product_qty];
                    }
                    return $res;
                }
            }
        }

        return ['status' => 'attr_not_found'];
    }

    // Custom Check Stock 
    public function c_check_stock($request){
        $stock = Product_data_attribute_master::select('stock')->where('id', $request->id)->first();

        if($stock->stock >= $request->qty){
            $res = ['status' => 'available', 'qty' => $request->qty, 'cr_id' => $request->id];
        } else {
            $res = ['status' => 'Not Enough', 'qty' => $stock->stock, 'cr_id' => $request->id,];
        }
        
        return $res;   
    }

    public function ch_check_stock(){
        $check = Cart::get_cart();
        $result = [];
        if(count($check) > 0){
            foreach($check as $q => $ch) {
                $stock = Product_data_attribute_master::where('id', $q)->first();

                if($stock->stock >= $ch['qty']) {
                    $result[$q] = ['status' => 'available'];
                } else {
                     $result[$q] = ['status' => 'not Enough', 'name' => $ch['data']['name'], 'qty' => $stock->stock];
                }
                
            }
        }

        return $result;
    }

    public function ch_check_voucher($request){
        $id = $request;
        $get_cart =  Cart::get_checkout_data();

        $get_coupon = Voucher::select('total', 'voucher_code', 'min_checkout', 'total_use_per_user')->where([['id', $id],['valid_from', '<=', date('Y-m-d')],['valid_to', '>=', date('Y-m-d')],['status', 'y']])->first();

        if(count($get_coupon) > 0){
            $user = auth()->guard($this->guard)->user()->id;
            $count_coupon = Ordervoucher::where('voucher_code', $get_coupon->voucher_code)->count();
            $count_user = orderhd::join('ordervoucher', 'orderhd.id', 'ordervoucher.orderhd_id')->where('orderhd.customer_id', $user)->count();

            $min_checkout = number_format(($get_coupon->min_checkout),0,',','.');

            if($get_coupon->total_use_per_user <= $count_user) {
                $result = [
                            'id' => 1,
                            'message' => 'Sorry Limit Per user is '.$get_coupon->total_use_per_user.'',
                          ];
            } else if ($get_coupon->total <= $count_coupon){
                $result = [
                            'id' => 1,
                            'message' => 'Sorry Voucher Out of Stock'
                          ];
            } else if($get_coupon->min_checkout > $get_cart['sub_total']){
                $result = [
                            'id' => 1,
                            'message' => 'Sorry min Checkout Rp. '.$get_coupon->min_checkout.'',
                          ];
            } else{
                $result = [
                            'id' => 0,
                            'message' => 'sukses'
                          ];
            }
        } else{
            $result = [
                        'id' => 1,
                        'message' => 'Voucher Not Valid'
                     ];
        }

        return $result;
    }
    // End Custom Stock

    public function check_voucher($request){
       $id = $request['coupon'];
       $coupon = Voucher::where([['voucher_code', $id],['valid_from', '<=', date('Y-m-d')],['valid_to', '>=', date('Y-m-d')],['status', 'y']])->first();
       
       if($coupon != NULL) {
            $vo_used = Ordervoucher::where('voucher_code', $id)->count();
            $checkout_data = Cart::get_checkout_data();
            $user = auth()->guard($this->guard)->user()->id;
            $count_user = orderhd::join('ordervoucher', 'orderhd.id', 'ordervoucher.orderhd_id')->where('orderhd.customer_id', $user)->count();

            if($coupon->total_use_per_user <= $count_user){
                $rs_respon = ['status' => 'failed', 'message' => 'Sorry, Limit per User is '.$coupon->total_use_per_user.''];        
            } else if( $vo_used >= $coupon->total){ 
                $rs_respon = ['status' => 'failed', 'message' => 'Sorry, Voucher stock already empty'];  
            } else if( $checkout_data['total'] <= $coupon->min_checkout){
                $rs_respon = ['status' => 'failed', 'message' => 'Sorry min Checkout Rp '.$coupon->min_checkout.'']; 
            } else if($vo_used <= $coupon->total && $checkout_data['total'] >= $coupon->min_checkout){
                if($coupon->discount_type == "v") {
                    $type = "v";
                    $value = $coupon->amount;
               } else {
                    $type = "p";
                    $value = $coupon->amount / 100;
               }
                $rs_coupon = [
                                 'id' => $coupon->id,
                                 'coupon_name' => $coupon->voucher_name,
                                 'amount' => $coupon->amount,
                                 'value' => $value,
                                 'type' => $type,
                                 'qty' => 1
                               ];

                Cart::set_coupon($rs_coupon);
                $rs_respon = ['status' => 'success'];
            }else{
                $rs_respon = ['status' => 'failed', 'message' => 'Coupon Tidak Valid'];        
            }            
       } else{
             $rs_respon = ['status' => 'failed', 'message' => 'Coupon Tidak Valid'];
       }

       return $rs_respon;
    }

    public function generateCart($request){
        $checkout_sub_total = 0;
        $checkout_weight    = 0;
        $total              = 0;
        $coupon             = 0;
        $prc                = 0;
        $cart_categorys     = '';
        $cart_subcategorys  = '';
        $cart               = Cart::get_cart($request);

        if($cart){
            foreach($cart as $q => $c){
                $id         = $c['product_id'];
                $attr       = $c['attr'];
                $qty        = (int)$c['qty'];
                $weight     = (float)$c['weight'];
                $request->request->add(['id' => $id,'attr' => $attr,'qty' => $qty,'weight' => $weight,'action' => 'generate']);
                $result     = $this->check_stock($request);

                if($result['status'] == 'available'){
                    if($qty > $result['qty']){
                        $this->update_cart(['id' => $id,'attr' => $attr, 'qty' => $result['qty'], 'weight' => $weight, 'product_data_attribute_master_id' => $q]);
                    }
                    $product    = Product::where('id',$id)
                                    ->with('primary_images','product_data_attribute_master.product_data_attribute.product_attribute_data.attribute')
                                    ->where('status','y')
                                    ->first();
                    $attribute          = Product_attribute_data::whereIn('id',$attr)->with('attribute')->get();

                    $check_categorys = Product_category::select('id','category_name', 'category_id')->where('id', $product->product_category_id)->first();

                    if(count($check_categorys) > 0){
                        if($check_categorys->category_id == 0){
                            $cart_categorys = $check_categorys->category_name;
                            $cart_subcategorys = '';
                        } else{
                            $get_categorys = Product_category::select('category_name')->where('id', $check_categorys->category_id)->first();

                            if(count($get_categorys) > 0){
                                $cart_categorys = $get_categorys->category_name;
                                $cart_subcategorys = $check_categorys->category_name;
                            } else{
                                $cart_subcategorys = $check_categorys->category_name;
                                $cart_subcategorys = '';
                            }
                        }
                    } 

                    // $reseller =  Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date('Y-m-d h:i:s')],['price_rule.valid_to', '>=', date('Y-m-d h:i:s')],['price_rule_product.id_product', $product->id],['price_rule.from_qty', '>=', $qty]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date('Y-m-d h:i:s')],['price_rule.valid_to', '>=', date('Y-m-d h:i:s')],['price_rule.from_qty', '>=', $qty]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('updated_at', 'desc')->orderBy('from_qty', 'desc')->first();

                    $price_rule = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $product->id]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('updated_at', 'desc')->first();

                    $temp_attribute     = [];
                    foreach($attribute as $at){
                        $temp_attribute[]   =  [
                                                'attribute' => $at->attribute->attribute_name,
                                                'type'      => $at->attribute->attribute_type,
                                                'value'     => $at->value
                                            ];
                    }

                    $reseller = $this->reseller_func($product->id, $product->price, $qty);

                    if(count($reseller) > 0){
                        $prc = $reseller->amount;
                    } else{
                        $prc = $product->price;
                    }

                    if(count($price_rule) > 0){
                        if($price_rule->reduction_type == "v"){
                            if($prc > $price_rule->reduction_amount){
                                $prc = $prc - $price_rule->reduction_amount;
                            } else{
                                $prc = 0;
                            }
                        } else if($price_rule->reduction_type == "p"){
                            $get_pr = $price_rule->reduction_amount / 100;
                            $conv = $prc * $get_pr;
                            $prc = $prc - $conv;
                        }
                    }
                    
                    $sub_total = $prc * $qty;

                    $checkout_weight += ($weight * 1000) * $qty;

                    $data_product       = [
                        'id'        => $product->id,
                        'images'    => $product->image,
                        'attribute' => $temp_attribute,
                        'name'      => $product->name,
                        'name_alias'=> $product->name_alias,
                        'price'     => (int)$prc,
                        'sub_total' => $sub_total,
                        'category'     => $cart_categorys,
                        'sub_category' => $cart_subcategorys,
                    ];

                    $checkout_sub_total += $sub_total;
                    $request->request->add(['data_product' => $data_product]);
                    Cart::set_product_data($request);
                }else{
                    Cart::remove($q);
                }
            }
            $get_coupon                 = Cart::get_coupon();
            $total                      = $checkout_sub_total;
            $checkout_data              = Cart::get_checkout_data();

            if($get_coupon) {
                if($get_coupon['type'] == "p") {
                    $coupon = $total * $get_coupon['value'];
                    $total = $total - $coupon;
                } else {
                    $coupon = $get_coupon['value'];

                    if($total > $coupon){
                        $total = $total - $coupon;
                    } else if($coupon > $total){
                        $total = 0;
                    }

                }
            }


            $checkout_data = [
                                'sub_total' => $checkout_sub_total,
                                'total_weight' => $checkout_weight,
                                'coupon_id' => $get_coupon['id'] ?? '', 
                                'coupon_name' => $get_coupon['coupon_name'] ?? '',   
                                'coupon' => $coupon,
                                'coupon_amount' => $get_coupon['amount'] ?? '',
                                'coupon_type' => $get_coupon['type'] ?? '',
                                'coupon_qty' => $get_coupon['qty'] ?? '',
                                'total' => $total
                            ];
            
            Cart::set_checkout_data($checkout_data);

            $cart           = Cart::get_cart();       
            $checkout_data  = Cart::get_checkout_data();  
            return ['cart' => $cart,'checkout_data' => $checkout_data];
        }else{
            return 'no_data';
        }
    }

    public function generateCart2($request){
        $checkout_sub_total = 0;
        $checkout_weight    = 0;
        $total              = 0;
        $coupon             = 0;
        $prc                = 0;
        $cart_categorys     = '';
        $cart               = Cart::get_cart($request);

        if($cart){
            foreach($cart as $q => $c){
                $id             = $c['product_id'];
                $attr           = $c['attr'];
                $qty            = (int)$c['qty'];
                $weight         = (float)$c['weight'];
                $request->request->add(['id' => $id,'attr' => $attr,'qty' => $qty, 'weight' => $weight,'action' => 'generate']);
                $result     = $this->check_stock($request);
                
                $product    = Product::where('id',$id)
                                ->with('primary_images','product_data_attribute_master.product_data_attribute.product_attribute_data.attribute')
                                ->where('status','y')
                                ->first();
                $attribute          = Product_attribute_data::whereIn('id',$attr)->with('attribute')->get();

                $check_categorys = Product_category::select('category_name')->where('id', $product->product_category_id)->first();

                $cart_categorys = $check_categorys->category_name;

                // $reseller =  Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date('Y-m-d h:i:s')],['price_rule.valid_to', '>=', date('Y-m-d h:i:s')],['price_rule_product.id_product', $product->id],['price_rule.from_qty', '>=', $qty]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date('Y-m-d h:i:s')],['price_rule.valid_to', '>=', date('Y-m-d h:i:s')],['price_rule.from_qty', '>=', $qty]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('updated_at', 'desc')->orderBy('from_qty', 'desc')->first();

                $price_rule = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $product->id]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('updated_at', 'desc')->first();

                $temp_attribute     = [];
                foreach($attribute as $at){
                    $temp_attribute[]   =  [
                                            'attribute' => $at->attribute->attribute_name,
                                            'name'      => $at->name,
                                            'type'      => $at->attribute->attribute_type,
                                            'value'     => $at->value,
                                        ];
                }

                $reseller = $this->reseller_func($product->id, $product->price, $qty);

                if(count($reseller) > 0){
                    $prc = $reseller->amount;
                } else{
                    $prc = $product->price;
                }

                if(count($price_rule) > 0){
                    if($price_rule->reduction_type == "v"){
                        if($prc > $price_rule->reduction_amount){
                            $prc = $prc - $price_rule->reduction_amount;
                        } else{
                            $prc = 0;
                        }
                    } else if($price_rule->reduction_type == "p"){
                        $get_pr = $price_rule->reduction_amount / 100;
                        $conv = $prc * $get_pr;
                        $prc = $prc - $conv;
                    }
                }
                
                $sub_total    = $prc * $qty;

                $checkout_weight += ($weight * 1000) * $qty;

                $data_product       = [
                    'id'           => $product->id,
                    'images'       => $product->image,
                    'attribute'    => $temp_attribute,
                    'name'         => $product->name,
                    'name_alias'   => $product->name_alias,
                    'price'        => (int)$prc,
                    'sub_total'    => $sub_total,
                    'category'     => $cart_categorys,
                ];

                $checkout_sub_total += $sub_total;

                $request->request->add(['data_product' => $data_product]);
                Cart::set_product_data($request);
            }

            $get_coupon                 = Cart::get_coupon();
            $total                      = $checkout_sub_total;
            $checkout_data              = Cart::get_checkout_data();

            if($get_coupon) {
                if($get_coupon['type'] == "p") {
                    $coupon = $total * $get_coupon['value'];
                    $total = $total - $coupon;
                } else {
                    $coupon = $get_coupon['value'];

                    if($total > $coupon){
                        $total = $total - $coupon;
                    } else if($coupon > $total){
                        $total = 0;
                    }

                }
            }

            $checkout_data = [
                                'sub_total' => $checkout_sub_total,
                                'total_weight' => $checkout_weight,
                                'coupon_id' => $get_coupon['id'] ?? '', 
                                'coupon_name' => $get_coupon['coupon_name'] ?? '',   
                                'coupon' => $coupon,
                                'coupon_amount' => $get_coupon['amount'] ?? '',
                                'coupon_type' => $get_coupon['type'] ?? '',
                                'coupon_qty' => $get_coupon['qty'] ?? '',
                                'total' => $total
                            ];
            
            Cart::set_checkout_data($checkout_data);

            $cart           = Cart::get_cart();  
            $checkout_data  = Cart::get_checkout_data();  
            return ['cart' => $cart,'checkout_data' => $checkout_data];
        }else{
            return 'no_data';
        }
    }

    public function sync_dbase_cart($request){
        $customer_cart  = Customer_cart::firstOrCreate(['customer_id' => auth()->guard($this->guard)->user()->id]);
        $temp           = [];
        $cart           = $this->generateCart($request);
        if($cart != 'no_data'){
            foreach($cart['cart'] as $q => $c){
                $temp[] = [
                    'id'            => $q,
                    'product_id'    => $c['product_id'],
                    'attr'          => $c['attr'],
                    'qty'           => $c['qty'],
                    'weight'        => $c['weight'],
                ];
            }
        }
        if($customer_cart->content){
            $dbase_cart     = json_decode($customer_cart->content);
        }else{
            $dbase_cart     = [];  
        }
        foreach($temp as $t){
            $match      = false;
            foreach($dbase_cart as $q => $dc){
                if($dc->id == $t['id']){
                    $dif    = array_diff($t['attr'],$dc->attr);
                    if(count($dif) == 0){
                        $dbase_cart[$q]->qty = $t['qty'];
                        $match  = true;
                        break;
                    }
                }
            }
            if($match == false){
                $dbase_cart[]   = (object)$t;
            }
        }
        foreach($dbase_cart as $q => $dc){
            $data       = [
                'id'        => $dc->product_id,
                'product_data_attribute_master_id'  => $dc->id,
                'attr'      => $dc->attr,
                'qty'       => $dc->qty,
                'weight'    => $dc->weight,
                'action'    => 'update',
            ];
            $request->request->add($data);
            $check  = $this->check_stock($request);
            if($check['status'] == 'out_of_stock'){
                array_forget($dbase_cart,$q);
            }
            Cart::update($data);
        }
        $customer_cart->content  = json_encode(array_values($dbase_cart));
        $customer_cart->save();
    }

    public function remove_dbase_cart($request){
        $customer_cart  = Customer_cart::where('customer_id', auth()->guard($this->guard)->user()->id)->first();
        if($customer_cart->content){
           $dbase_cart     = json_decode($customer_cart->content); 
       } else{
            $dbase_cart     = [];
       }

       if(count($dbase_cart > 0)){
         foreach($dbase_cart as $q => $rc){
            if($rc->id == $request){
                array_forget($dbase_cart, $q);
            }
         }
         $customer_cart->content = json_encode(array_values($dbase_cart));
         $customer_cart->save();
       }
    }

    public function get_attribute($request){
        $product_data_attribute     = Product_data_attribute_master::where('id',$request['id'])->with('product_data_attribute')->first();
        return $product_data_attribute;
    }

    public function get_total_cart(){
        $total  = Cart::get_total_item();
        return $total;
    }

    public function add_cart($request){
        Cart::add($request);
    }

    public function update_cart($request){
        Cart::update($request);
    }

    public function c_update_cart($request){
        $c_stock = $this->c_check_stock($request);
        $get_cart   = Cart::get_cart();

        if($c_stock['status'] == "available") {
            $id             = $get_cart[$c_stock['cr_id']]['product_id']; 
            $attr           = $get_cart[$c_stock['cr_id']]['attr'];
            $qty            = $c_stock['qty'];
            $weight         = $get_cart[$c_stock['cr_id']]['weight'];
            $data       = ['product_id' => $id,'attr' => $attr,'qty' => $qty, 'weight' => $weight];
            session(['cart.'.$c_stock['cr_id'] => $data]);
            $result = ['status' => 'success'];
        } else{
            $product = Product::where('id', $get_cart[$c_stock['cr_id']]['product_id'])->first();
            $name = $product->name;
            $result = ['status' => 'failed', 'name' => $name, 'qty' => $c_stock['qty']];
        }
        return $result;
    }

    public function remove_cart($id){
        Cart::remove($id);
    }

    public function removed_coupon($id){
        Cart::remove_coupon($id);
    }

    public function update_checkout_notes($notes){
        $checkout_data          = Cart::get_checkout_data();
        $checkout_data['notes'] = $notes;
        Cart::set_checkout_data($checkout_data);
    }

    public function delivery_address_type($type){
        $checkout_data                          = Cart::get_checkout_data();
        $checkout_data['delivery_address_type'] = $type;
        Cart::set_checkout_data($checkout_data);
    }

    public function update_delivery_billing_data($data){
        $checkout_data                          = Cart::get_checkout_data();
        $checkout_data['delivery_address_type'] = $data['delivery_type'];
        $checkout_data['delivery_address']      = $data['delivery'];
        $checkout_data['billing_address_type']  = $data['billing_type'];
        $checkout_data['billing_address']       = $data['billing'];
        Cart::set_checkout_data($checkout_data);
    }

    /* Custom function */
    public function reseller_func($id, $price, $qty){
        $query = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule');
        $rss = 0;
        $r_data = []; 
        $rtemp = [];

        if($qty == 0){
            $reseller = $query->where([['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $id]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule.reduction_type', 'price_rule.reduction_amount', 'price_rule.from_qty', 'price_rule_product.*')->orderBy('updated_at', 'asc')->orderBy('price_rule.from_qty', 'desc')->get();

            if(count($reseller) > 0){
                foreach($reseller as $res){
                    if($rss == 0 || $rss != $res->from_qty){
                        $result_discount = $this->disc_calc($res->reduction_type, $res->reduction_amount, $price);
                        $rtemp = [
                                'from'   => $res->from_qty,
                                'amount' => $result_discount,
                             ];
                        $r_data[$res->from_qty] = (object)$rtemp;
                        $rss = $res->from_qty;
                    } else{
                        $result_discount = $this->disc_calc($res->reduction_type, $res->reduction_amount, $price);
                        $r_data[$res->from_qty]->amount = $result_discount;
                    }
                }
            }
        } else if($qty > 0){

            $reseller = $query->where([['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $id],['price_rule.from_qty', '<=', $qty]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule.from_qty', '<=', $qty]])->select('price_rule.reduction_type', 'price_rule.reduction_amount', 'price_rule.from_qty', 'price_rule_product.*')->orderBy('from_qty', 'desc')->orderBy('updated_at', 'desc')->first();

            if(count($reseller) > 0){
                 $result_discount = $this->disc_calc($reseller->reduction_type, $reseller->reduction_amount, $price);

                $rtemp = [
                            'from'   => $reseller->from_qty,
                            'amount' => $result_discount,
                         ];
                $r_data = (object)$rtemp;
            }
        }

        return $r_data;
    }

    public function disc_calc($type,$amount, $price){
        if($type == 'p'){
            $rs_discount = $amount / 100;
            $cal_discount = $price * $rs_discount;
            if($price >= $cal_discount){
                $result = $price - $cal_discount;
            } else{
                $result = 0;
            }
        } else if($type == 'v'){
            if($price >= $amount){
                $result = $price - $amount;
            } else{
                $result = 0;
            }
        }
        return $result;
    }
    /* End Custom Function */


    /* Custom Controller */
    public function getprovince($request){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $province = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $province;
        }
    }

    public function getcity($request){
        $curl = curl_init();
        $prov_id = 'province='.$request['province'] ?? '';
        $city_id = 'id='.$request['city'] ?? '';

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/city?".$city_id."&".$prov_id."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $city = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $city;
        }
    }

    public function getsubdistrict($request){
        $curl = curl_init();
        $city_id = 'city='.$request['city'] ?? '';
        $subdis_id = 'id='.$request['sub_district'] ?? '';

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/subdistrict?".$city_id."&".$subdis_id."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $subdistrict = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $subdistrict;
        }
    }

    public function getcourier($request){
       $curl = curl_init();
       $ids = $request['id'];
       $weight = $request['weight'];
       $from = $request['from'];
       $c_courier = '';

        $courier = Delivery_service::where('status', 'y')->get();
        $check = count($courier);

       if($check > 0) {
            foreach($courier as $ct) {
               $get_courier = $ct->name_alias; 
               $c_courier = strtolower($get_courier.':'.$c_courier);
            }
       }

        $get_length = strlen($c_courier) - 1;
        $result = substr($c_courier, 0, $get_length);

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "origin=".$from."&originType=city&destination=".$ids."&destinationType=city&weight=".$weight."&courier=".$result."",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $cost = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $cost;
        }
    }

    public function getwaybill($request){
       $curl = curl_init();
       $waybill = $request['waybill'];
       $courier = $request['courier'];
    
       $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://pro.rajaongkir.com/api/waybill",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "waybill=".$waybill."&courier=".$courier."",
            CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $waybill = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $waybill;
        }
    }
    /* End Custom Controller */

    public function email($request){
        $url                         = $request->e_url;
        $dt_cust['subject']          = $request->e_subject;
        $dt_cust['to']               = $request->e_email;
        $this->data['title']         = $request->e_title;
        $this->data['status']        = $request->e_status;
        if($request->e_status == "admin"){
            $this->data['adm_content']  = $request->e_content;
        } else{
            $this->data['cust_content']  = $request->e_content;
        }

        Mail::send($url, $this->data, function ($message) use($dt_cust){
            $message->subject($dt_cust['subject'])
                ->to($dt_cust['to']);
            });
    }

    /*function Nicepay payment gateway*/
    function generateReference()
    {
        $micro_date = microtime();
        $date_array = explode(" ",$micro_date);
        $date = date("YmdHis",$date_array[1]);
        $date_array[0] = preg_replace('/[^\p{L}\p{N}\s]/u', '', $date_array[0]);
        return "Ref".$date.$date_array[0].rand(100,999);
    }
    /*end function Nicepay payment gateway*/
}
