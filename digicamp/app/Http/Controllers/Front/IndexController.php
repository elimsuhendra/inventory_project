<?php namespace digipos\Http\Controllers\Front;

// use Cache;
use DB;

use digipos\models\Slideshow;
use digipos\models\Content;
use digipos\models\Category;
use digipos\models\Banner;
use digipos\models\Product;
use digipos\models\Product_category;
use digipos\models\product_data_attribute_master;
use digipos\models\Featured_product_dt;
use digipos\models\Price_rule;
use digipos\models\Price_rule_product;
use digipos\models\Post;

use Illuminate\Http\request;

class IndexController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}

	public function index(request $request){
		$limit = 8;
		$get_cat = Product_category::select('id', 'category_name', 'category_name_alias', 'description')->where([['status', 'y'],['sticky_category', 'y']])->first();

		if($get_cat != null){
			$this->data['sub_category'] = Product_category::select('thumbnail', 'category_name', 'description', 'category_name_alias')->where([['status', 'y'],['category_id', $get_cat->id]])->inRandomOrder()->limit(3)->get();
			$this->data['category'] = $get_cat;
		}else{
			$this->data['category'] = "";
		}

		$this->data['slideshow'] = Slideshow::where([["status", "y"], ['pages_id', 1]])->get();
		$this->data['alasan'] = Category::select('id')->with('post')->where([['status', 'y'], ['id', 2]])->limit(3)->first();
		$this->data['banner_2'] = Banner::select('image')->where([['pages_id', 1], ['status', 'y']])->first();

		$promo_terbaru = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->where('price_rule_product.id_product', 0)->get();
		$promo_terbaru_ac = "promo";

		$product_terbaru = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y']])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
		$product_terbaru_ac = "pro_default";

		// $get_rekomendasi = Featured_product_dt::where('featured_product_hd_id', 1)->distinct()->pluck('product_id', 'id')->toArray();
		// $product_rekomendasi = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y']])->whereIn('product.id', $get_rekomendasi)->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();

		$product_rekomendasi = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('featured_product_dt', 'featured_product_dt.product_id', 'product.id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y'], ['featured_product_dt.featured_product_hd_id', 1]])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
		$product_rekomendasi_ac = "pro_default";

		$this->data['promo_terbaru'] = $this->checkPriceruleProduct($promo_terbaru, $promo_terbaru_ac, $limit);
		$this->data['product_terbaru'] = $this->checkPriceruleProduct($product_terbaru, $product_terbaru_ac, $limit);
		$this->data['product_rekomendasi'] = $this->checkPriceruleProduct($product_rekomendasi, $product_rekomendasi_ac, $limit);
		$this->data['lmt'] = 4;

		return $this->render_view('pages.index');
	}
}
