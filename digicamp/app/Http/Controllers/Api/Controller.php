<?php namespace digipos\Http\Controllers\Api;

use digipos\models\Pages;
use digipos\models\Socialmedia;
use digipos\models\Config;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Request;
use View;
use Cache;
use Cookie;
use App;

class Controller extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $data         = [];
    public $root_path    = '/';
    public $view_path    = 'api';
    public $guard        = 'web';
    public $auth_guard   = 'auth:api';
    public $guest_guard  = 'guest:api';

    public function __construct() {
        $this->data['guard']        = $this->guard;
        $this->data['root_path']    = url($this->root_path);
        $this->data['view_path']    = $this->view_path;

        //Global Number
        $no = 1;
        if (Request::has('page')){
            $no = Request::input('page') * 10 - 9;
        }
        $this->data['no']       = $no;
        $this->data['my_ip']    = $_SERVER['REMOTE_ADDR'];
        
        //Set Global
        $query_config = Config::where('config_type','front')->orwhere('config_type','both');
        $config = $this->cache_query('config',$query_config,'get');
        foreach($config as $c){
            $this->data[$c->name] = $c->value;
            $this->data['email_config'][$c->name] = $c->value;
        }
        //Set Meta
        $this->data['meta_full_image']  = asset('components/both/images/web/'.$this->data['web_logo']);                    
        $this->set_meta();

        // $this->data['social_media']     = $this->cache_query('social_media',Socialmedia::where('status', 'y')->orderBy('order','asc'),'get');
        // $this->data['helpful_links']    = $this->cache_query('helpful_links',Pages::where('status', 'y')->orderBy('order','asc')->take(8),'get');
        
        // $popup_cookie                   = Cookie::get('alskdj');
        // if(!$popup_cookie){
        //     $popup                      = Popup::where('valid_from', '<=', date('Y-m-d H:i:s'))
        //                                     ->where('valid_to', '>=', date('Y-m-d H:i:s'))
        //                                     ->orderBy('id', 'desc')
        //                                     ->first();
        //     if($popup){
        //         $data_pop               =  [
        //                                     'name'  => $popup->popup_name,
        //                                     'url'   => $popup->url,
        //                                     'image' => $popup->image
        //         ];
        //         Cookie::queue(Cookie::make('alskdj',$data_pop,60*24));
        //         $this->data['popup_cookie'] = $data_pop;
        //     }
        // }

    }

    public function render_view($view = ''){
        $data = $this->data;
        return view($this->view_path.'.'.$view,$data);       
    }

    public function cache_query($name,$query,$type='',$time = 60){
        $c = Cache::remember($name, $time, function() use($query,$type){
            if (!empty($type)){
                if ($type == 'first'){
                    $q = $query->first();
                }else{
                    $q = $query->get();
                }
                return $q;
            }else{
                return $query;
            }
        });
        Cache::flush();
        return $c;
    }
   
    public function cache_query_content($name,$table,$data,$with=[],$time = 60){
        $c  = Cache::remember($name, $time,function() use($table,$data,$with){
                $res    = [];
                $data_with  = $with;
                foreach($data as $fc){
                    $q      = $table->where('id',$fc);
                    if(count($with) > 0){
                        $q  = $q->with($data_with);
                    }
                    $res[]  = $q->first();
                }
                return $res;
            });
        //Cache::flush();
        return $c;
    }

    public function set_meta(){
        $this->data['facebook_meta']['og:title']        = $this->data['web_title'].' | '.$this->data['web_name'];
        $this->data['facebook_meta']['og:site_name']    = $this->data['web_name'];
        $this->data['facebook_meta']['og:url']          = Request::url();
        $this->data['facebook_meta']['og:type']         = "article";
        $this->data['facebook_meta']['og:locale']       = "id_ID";
        $this->data['facebook_meta']['og:image']        = $this->data['meta_full_image'];
        $this->data['facebook_meta']['og:description']  = $this->data['web_description'];

        $this->data['twitter_meta']['twitter:card']          = "summary_large_image";
        $this->data['twitter_meta']['twitter:site']          = "@".$this->data['web_name'];
        $this->data['twitter_meta']['twitter:creator']       = "@".$this->data['web_name'];
        $this->data['twitter_meta']['twitter:url']           = Request::url();
        $this->data['twitter_meta']['twitter:title']         = $this->data['web_name'];
        $this->data['twitter_meta']['twitter:image']         = $this->data['meta_full_image'];
        $this->data['twitter_meta']['twitter:description']   = $this->data['web_description'];
    }

    public function set_dif_time($sum,$dif,$unit,$time){
        $q  =   strftime('%H:%M',strtotime($sum.$dif." ".$unit, strtotime($time)));
        return $q;
    }

    public function get_dif_time($dtime,$atime){
        $nextDay    =$dtime>$atime?1:0;
        $dep        =EXPLODE(':',$dtime);
        $arr        =EXPLODE(':',$atime);
        $diff       =ABS(MKTIME($dep[0],$dep[1],0,DATE('n'),DATE('j'),DATE('y'))-MKTIME($arr[0],$arr[1],0,DATE('n'),DATE('j')+$nextDay,DATE('y')));
        $hours      =FLOOR($diff/(60*60));
        $mins       =FLOOR(($diff-($hours*60*60))/(60));
        $secs       =FLOOR(($diff-(($hours*60*60)+($mins*60))));
        IF(STRLEN($hours)<2){$hours="0".$hours;}
        IF(STRLEN($mins)<2){$mins="0".$mins;}
        IF(STRLEN($secs)<2){$secs="0".$secs;}
        return $hours.':'.$mins;
    }
}
