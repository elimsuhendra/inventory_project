<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Delivery_service;
use digipos\models\Delivery_service_sub;
use digipos\models\Delivery_price;
use digipos\models\Sub_district;

use digipos\Libraries\Alert;
use Request;
use File;
use Cache;

class ConfigController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->model 		 	= new Config;
		$this->data['title']	= trans('general.general-settings');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$config = $this->model->get();
		foreach($config as $c){
			$configs[$c->name] = $c->value;
		}
		$configs = (object)$configs;
		$this->data['configs'] = $configs;

		$this->data['city'] = json_decode($this->getcity());
		return $this->render_view('pages.config.index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(){
		$all 	= Request::except(['clean_cache']);
		foreach($all as $r => $qr){
			$q = $this->model->where('name',$r)->first();
			if ($q){
				if($q->value != $qr){
					if (Request::file($r)){
						$data = [
								'name' => $r,
								'file_opt' => ['path' => 'components/both/images/web/'],
								'old_image'	=> $this->model->value
							];
						$image = $this->build_image($data);
						$qr = $image;
					}
					$q->value = $qr;
					$q->save();
				}
			}
		}
		if(Request::input('clean_cache') == 'y'){
			Cache::flush();
		}
		Alert::success('Successfully change '.$this->data['title']);
		return redirect()->back();
	}

	public function sync(){
		ini_set('max_execution_time', 0);
		$delivery_service 	= Delivery_service::with('delivery_service_sub')->get();
		Delivery_price::truncate();
		foreach($delivery_service as $ds){
			$origin 	= 151;
			$courier 	= $ds->name_alias;
			$subdistrict 	= Sub_district::get();
			foreach($subdistrict as $sd){
				$destination 	= $sd->sub_district_id;
				$curl 		= curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => "origin=".$origin."&originType=city&destination=".$destination."&destinationType=subdistrict&weight=1000&courier=".$courier,
					CURLOPT_HTTPHEADER => array(
					    "content-type: application/x-www-form-urlencoded",
					    "key: ".env('RAJAONGKIR_KEY')
					),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
				  echo "cURL Error #:" . $err;
				} else {
					$result = json_decode($response)->rajaongkir->results[0];
					$temp 	= [];
					foreach($result->costs as $cs){
						$sub 	= Delivery_service_sub::where('delivery_service_id',$ds->id)->where('name_alias',$cs->service)->first();
						if($sub){
							$temp[] = [
									'delivery_service_sub_id' 	=> $sub->id,
									'sub_district_id'			=> $destination,
									'price'						=> $cs->cost[0]->value,
									'etd'						=> $cs->cost[0]->etd,
									'notes'						=> $cs->cost[0]->note,
									'created_at'				=> date('Y-m-d H:i:s'),
									'updated_at'				=> date('Y-m-d H:i:s'),
								];
						}
					}
					Delivery_price::insert($temp);
				}
			}
		}
		Alert::success('Successfully sync data ');
		return redirect()->back();
	}
}
