<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Content;
use digipos\models\Product_category;

use Illuminate\Http\Request;
use digipos\Libraries\Alert;
use File;
use Cache;
use DB;

class ContentController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->model 		 = new Content;
		$this->data['title'] = 'Manage '.trans('general.content');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$contents = $this->model->get();
		foreach($contents as $c){
			if(substr($c->value,0,1) == '['){
				$c->value 	= json_decode($c->value);
			}
			$content[$c->name] 	= $c->value;
		}
		$this->data['content'] 			= (object)$content;
		$this->data['product_category']	= Product_category::where('status','y')->orderBy('order','asc')->get();
		$data 					= [
									'home_banner' 				=> 'home_banner',
									'footer_product_category'	=> 'product_category'
								];
		foreach($data as $d => $q){
			$this->data[$d]	= [];
			$this->data['content']->$d = $this->data['content']->$d ? $this->data['content']->$d : []; 
			foreach($this->data['content']->$d as $hp){
				if(is_object($hp)){
					$this->data[$d][]	= $hp;
				}else{
					$this->data[$d][]	= DB::table($q)->where('id',$hp)->first();
				}
			}
		}
		return $this->render_view('pages.content.index');
	}

	public function ext(request $request,$action){
		$this->data['request']	= $request;
		return $this->$action();
	}

	//Function
	public function sorting_content_array(){
		$request 	= $this->data['request'];
		$name 	 	= $request->content_name;
		$data 		= $request->data;
		$check 		= Content::where('name',$name)->first();
		if($check){
			$value 	= json_decode($check->value);
			$temp 	= [];
			foreach($data as $d){
				foreach($value as $v){
					if((is_object($v))){
						$id 	= $v->id;
					}else{
						$id 	= $v;
					}
					if($id == $d){
						$temp[]	= $v;
					}
				}
			}
			$check->value  	= json_encode($temp);
			$check->save();
		}
		return response()->json(['status' => 'continue']);
	}

	public function add_new_content_array(){
		$request 	= $this->data['request'];
		$name 	 	= $request->content_name;
		$data 		= $request->data;
		$type 		= $request->type;
		$check 		= Content::where('name',$name)->first();
		if($check){
			$value 	= json_decode($check->value);
			if($type == 'single'){
				if($name == 'home_banner' && count($value) == 9){
					return response()->json(['status' => 'fail','text' => 'Maximum data is 9']);
				}
				if($request->hasFile('image')){
					$datas = [
							'name' => 'image',
							'file_opt' => ['path' => 'components/front/images/content/'],
						];
					$image = $this->build_image($datas);
					$data['image']	= $image;
				}
				$data['id']		= str_random(3);
			}
			$value[] 		= $data;
			$check->value 	= json_encode($value);
		}else{
			$check 			= new Content;
			$check->name 	= $name;
			$check->value  	= json_encode($data);
		}
		$check->save();
		return response()->json(['status' => 'continue']);
	}

	public function delete_content_array(){
		$request 	= $this->data['request'];
		$name 	 	= $request->content_name;
		$data 		= $request->data;
		$check 		= Content::where('name',$name)->first();
		if($check){
			$value 	 		= json_decode($check->value);
			if($value){
				foreach($value as $v => $q){
					if((is_object($q))){
						$id 	= $q->id;
					}else{
						$id 	= $q;
					}
					if($id == $data){
						unset($value[$v]);
						break;
					}
				}
				array_multisort($value);
				$check->value 	= json_encode($value);
				$check->save();
			}else{
				return response()->json(['status' => 'fail']);
			}
		}
		return response()->json(['status' => 'continue']);
	}

	public function save_field(){
		$request 	= $this->data['request'];
		foreach($request->data as $rd){
			Content::where('name',$rd['name'])->update(['value' => $rd['val']]);
		}
		return response()->json(['status' => 'continue']);
	}
}
