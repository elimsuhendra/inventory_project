<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Customer;
use digipos\models\Customer_group;
use digipos\models\Customer_group_data;

class CustomerController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Customer";
		$this->primary_field 	= "email";
		$this->root_link 		= "customer";
		$this->model 			= new Customer;
		$this->bulk_action 		= true;
		$this->bulk_action_data = [4];
		$this->image_path 		= 'components/front/images/customer/';
		$this->tab_data 		= [
									'general'	=> 'General',
									'group'		=> 'Group'
								];
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'images',
				'label' => 'Photo',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],[
				'name' => 'email',
				'label' => 'Email',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'name',
				'label' => 'Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'gender',
				'label' => 'Gender',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['m' => 'Male','f' => 'Female'],
			],[
				'name' => 'status',
				'label' => 'Status',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'email',
						'label' => 'Email',
						'type' => 'email',
						'attribute' => 'required autofocus',
						'validation' => 'email|required',
						'not_same' => 'y',
						'tab' => 'general'
					],[
						'name' => 'password',
						'label' => 'Password',
						'type' => 'password',
						'attribute' => 'required',
						'validation' => 'required',
						'hash' => 'y',
						'tab' => 'general'
					],[
						'name' => 'name',
						'label' => 'User Name',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					],[
						'name' => 'gender',
						'label' => 'Gender',
						'type' => 'select',
						'data' => ['m' => 'Male','f' => 'Female'],
						'class'	=> 'select2',
						'tab' => 'general'
					],[
						'name' => 'images',
						'label' => 'Your Picture',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb',
						'tab' => 'general'
					],[
						'name' => 'status',
						'label' => 'Status User',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					],[
						'name' => 'customer_group_id',
						'label' => 'Group',
						'type' => 'checkbox',
						'hasmany' => ['method' => 'customer_group_data','primary'=>'','field' => 'customer_group_id','table' => new Customer_group_data], 
						'data' => $this->build_array(Customer_group::orderBy('id','desc')->get(),'id','customer_group_name'),
						'tab' => 'group'
					]
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'email',
						'label' => 'Email',
						'type' => 'email',
						'attribute' => 'required autofocus',
						'validation' => 'email|required',
						'not_same' => 'y',
						'tab' => 'general'
					],[
						'name' => 'password',
						'label' => 'Password',
						'type' => 'password',
						'attribute' => 'required',
						'validation' => 'required',
						'hash' => 'y',
						'tab' => 'general'
					],[
						'name' => 'name',
						'label' => 'User Name',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					],[
						'name' => 'gender',
						'label' => 'Gender',
						'type' => 'select',
						'data' => ['m' => 'Male','f' => 'Female'],
						'class'	=> 'select2',
						'tab' => 'general'
					],[
						'name' => 'images',
						'label' => 'Your Picture',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb',
						'tab' => 'general'
					],[
						'name' => 'status',
						'label' => 'Status User',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					],[
						'name' => 'customer_group_id',
						'label' => 'Group',
						'type' => 'checkbox',
						'hasmany' => ['method' => 'customer_group_data','primary'=>'','field' => 'customer_group_id','table' => new Customer_group_data], 
						'data' => $this->build_array(Customer_group::orderBy('id','desc')->get(),'id','customer_group_name'),
						'tab' => 'group'
					]
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export();
	}
}
