<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Delivery_service;
use digipos\models\Delivery_service_sub;

class DeliveryservicesubController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.delivery-service-sub');
		$this->root_link 		= "delivery-service-sub";
		$this->primary_field 	= "name";
		$this->model 			= new Delivery_service_sub;
		$this->bulk_action 		= true;
		$this->bulk_action_data = [2];
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'name',
				'label' => 'Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'delivery_service_id',
				'label' => trans('general.parent'),
				'search' => 'select',
				'search_data' => $this->get_delivery_service(),
				'sorting' => 'y',
				'belongto' => ['method' => 'delivery_service','field' => 'name']
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'name',
						'label' => 'Name',
						'type' => 'text',
						'attribute' => 'required autofocus readonly',
						'validation' => 'required',
					],[
						'name' => 'name_alias',
						'label' => 'Name Alias',
						'type' => 'text',
						'attribute' => 'required readonly',
						'validation' => 'required',
					],[
						'name' => 'status',
						'label' => 'Status',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					]
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'name',
						'label' => 'Name',
						'type' => 'text',
						'attribute' => 'required autofocus readonly',
						'validation' => 'required',
					],[
						'name' => 'name_alias',
						'label' => 'Name Alias',
						'type' => 'text',
						'attribute' => 'required readonly',
						'validation' => 'required',
					],[
						'name' => 'status',
						'label' => 'Status',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					]
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'name',
								'type' => 'text'
							]
						];
		$this->order_method = "multiple";
		$this->order_filter = [
								'label' => 'Delivery Service',
								'name' => 'delivery_service_id',
								'data' => $this->get_delivery_service(),
								];
	}
	public function sorting(){
		$this->model = $this->model->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		$this->sorting_config();
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		$a = $this->buildbulkedit();
		return $a;
	}

	public function get_delivery_service(){
		$query = Delivery_service::where('status','y')->orderBy('order','asc')->pluck('name','id')->toArray();
		return $query;
	}
}
