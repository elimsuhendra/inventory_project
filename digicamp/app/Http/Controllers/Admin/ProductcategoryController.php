<?php namespace digipos\Http\Controllers\Admin;


use Illuminate\Http\Request;
use digipos\Libraries\Alert;
use Illuminate\Validation\Rule;

use File;
use digipos\models\Product_category;

class ProductcategoryController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.category');
		$this->check_relation 	= ['subcategory','productcategory'];
		$this->root_link 		= "product-category";
		$this->primary_field 	= "category_name";
		$this->model 			= new Product_category;
		$this->bulk_action 		= true;
		$this->bulk_action_data = [2]; 
		$this->image_path 		= 'components/front/images/product_category/';
		$this->tab_data 		= [
									'general'	=> 'General',
									'seo'		=> 'SEO'
								];
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'image',
				'label' => trans('general.image'),
				'sorting' => 'y',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],[
				'name' => 'category_name',
				'label' => trans('general.category-name'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
			// ],[
			// 	'name' => 'sticky_category',
			// 	'label' => 'Sticky Category',
			// 	'sorting' => 'y',
			// 	'search' => 'select',
			// 	'search_data' => ['y' => 'Sticky', 'n' => 'Not-sticky'],
			// 	'type' => 'check'
			// ]
		];
		$this->model = $this->model->where('category_id',0);
		$this->data['title'] = 'List '.$this->title;
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	// public function field_create(){
	// 	$field = [
	// 				[
	// 					'name' => 'category_name',
	// 					'label' => trans('general.category-name'),
	// 					'type' => 'text',
	// 					'attribute' => 'required autofocus',
	// 					'validation' => 'required',
	// 					'not_same' => 'y',
	// 					'alias' => 'category_name_alias',
	// 					'tab'	=> 'general'
	// 				],
	// 				// ],[
	// 				// 	'name' => 'description',
	// 				// 	'label' => trans('general.description'),
	// 				// 	'type' => 'textarea',
	// 				// 	'validation' => 'required',
	// 				// 	'class' => 'editor',
	// 				// 	'tab'	=> 'general',
	// 				// ],[
	// 				[
	// 					'name' => 'meta_title',
	// 					'label' => trans('general.meta-title'),
	// 					'type' => 'text',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'meta_description',
	// 					'label' => trans('general.meta-description'),
	// 					'type' => 'textarea',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'meta_keywords',
	// 					'label' => trans('general.meta-keywords'),
	// 					'type' => 'textarea',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'image',
	// 					'label' => trans('general.image'),
	// 					'type' => 'file',
	// 					'file_opt' => ['path' => $this->image_path],
	// 					'upload_type' => 'single-image',
	// 					'form_class' => 'col-md-6 pad-left',
	// 					'validation' => 'mimes:jpeg,png,jpg,gif',
	// 					'note' => 'Note: File Must jpeg,png,jpg,gif',
	// 					'tab'	=> 'general'
	// 				],[
	// 					'name' => 'status',
	// 					'label' => trans('general.status'),
	// 					'type' => 'radio',
	// 					'data' => ['y' => 'Active','n' => 'Not Active'],
	// 					'attribute' => 'required',
	// 					'validation' => 'required',
	// 					'tab'	=> 'general'
	// 				]
	// 			];
	// 	return $field;
	// }

	// public function field_edit(){
	// 	$field = [
	// 				[
	// 					'name' => 'category_name',
	// 					'label' => trans('general.category-name'),
	// 					'type' => 'text',
	// 					'attribute' => 'required autofocus',
	// 					'validation' => 'required',
	// 					'not_same' => 'y',
	// 					'alias' => 'category_name_alias',
	// 					'tab'	=> 'general'
	// 				]
	// 				// ],[
	// 				// 	'name' => 'description',
	// 				// 	'label' => trans('general.description'),
	// 				// 	'type' => 'textarea',
	// 				// 	'validation' => 'required',
	// 				// 	'class' => 'editor',
	// 				// 	'tab'	=> 'general',
	// 				// ],[
	// 				,[
	// 					'name' => 'meta_title',
	// 					'label' => trans('general.meta-title'),
	// 					'type' => 'text',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'meta_description',
	// 					'label' => trans('general.meta-description'),
	// 					'type' => 'textarea',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'meta_keywords',
	// 					'label' => trans('general.meta-keywords'),
	// 					'type' => 'textarea',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'image',
	// 					'label' => trans('general.image'),
	// 					'type' => 'file',
	// 					'file_opt' => ['path' => $this->image_path],
	// 					'upload_type' => 'single-image',
	// 					'form_class' => 'col-md-6 pad-left',
	// 					'validation' => 'mimes:jpeg,png,jpg,gif',
	// 					'note' => 'Note: File Must jpeg,png,jpg,gif',
	// 					'tab'	=> 'general'
	// 				],[
	// 					'name' => 'status',
	// 					'label' => trans('general.status'),
	// 					'type' => 'radio',
	// 					'data' => ['y' => 'Active','n' => 'Not Active'],
	// 					'attribute' => 'required',
	// 					'validation' => 'required',
	// 					'tab'	=> 'general'
	// 				]
	// 			];
	// 	return $field;
	// }

	public function create(){
		// $this->field = $this->field_create();
		// return $this->build('create');

		$this->data['title']	= trans('general.create').' '.$this->title;
		return $this->render_view('pages.product_category.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		// $this->field = $this->field_create();
		// $this->model->category_id = 0;
		// return $this->build('store');

		$this->validate($request,[
			'category_name'	=> 'required|unique:product_category',
		]);

		// $check_status = 1;

		// $check = Product_category::where('sticky_category','y')->count();

		// if($request->sticky_category == 'y'){
		// 	if($check < 4){
		// 		$this->model->sticky_category 	= 'y';
		// 	} else{
		// 		$check_status = 0;
		// 	}	
		// } else{
		// 	$this->model->sticky_category 	= 'n';
		// }

		// if($check_status == 0){
		// 	Alert::fail('Limit Sticky Category 4');
		// 	return back();
		// } else{
		$this->model->category_name 		= $request->category_name;
		$this->model->category_name_alias 	= $request->category_name_alias;
		$this->model->category_id 			= 0;
		
		

		$this->model->status 				= $request->status;
		$this->model->meta_title 			= $request->meta_title;
		$this->model->meta_description 		= $request->meta_description;
		$this->model->meta_keywords 		= $request->meta_keywords;

		$file 	= [
					'name' 		=> 'image',
					'file_opt' 	=> ['path' => 'components/front/images/product_category/'],
				];

		if($request->hasFile('image')){
			$image = $this->build_image($file);
			$this->model->image = $image;
			$this->model->save();
		}else{
			if($request->input('remove-single-image-image') == 'y'){
				File::delete($file['file_opt']['path'].$this->model->image);
				$this->model->image = '';
				$this->model->save();
			}
		}

		Alert::success('Successfully add Category');
		return redirect()->to($this->data['path']);
		// }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request, $id){
		// $this->model = $this->model->where('id',$id);
		// $this->field = $this->field_edit();
		// return $this->build('view');

		$this->data['title']	= trans('general.edit').' '.$this->title;
		$this->data['get_category'] = $this->model->where('id',$id)->first();
		return $this->render_view('pages.product_category.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		// $this->model = $this->model->where('id',$id);
		// $this->field = $this->field_edit();
		// return $this->build('edit');

		$this->data['title']	= trans('general.edit').' '.$this->title;
		$this->data['get_category'] = $this->model->where('id',$id)->first();
		return $this->render_view('pages.product_category.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		// $this->field = $this->field_edit();
		// $this->model = $this->model->where('id',$id);
		// return $this->build('update');

		$this->validate($request,[
			'category_name'	=> [
				'required',
				Rule::unique('product_category')->ignore($id),
			],
		]);

		$check_status = 1;

		$check = Product_category::where([['sticky_category','y'],['id', '!=' ,$id]])->count();
		$this->model 						= $this->model->find($id);

		if($request->sticky_category == 'y'){
			if($check < 4){
				$this->model->sticky_category 	= 'y';
			} else{
				$check_status = 0;
			}	
		} else{
			$this->model->sticky_category 	= 'n';
		}

		if($check_status == 0){
			Alert::fail('Limit Sticky Category 4');
			return back();
		} else{
			$this->model->category_name 		= $request->category_name;
			$this->model->category_name_alias 	= $request->category_name_alias;
			$this->model->category_id 			= 0;
			$this->model->status 				= $request->status;
			$this->model->meta_title 			= $request->meta_title;
			$this->model->meta_description 		= $request->meta_description;
			$this->model->meta_keywords 		= $request->meta_keywords;

			$file 	= [
						'name' 		=> 'image',
						'file_opt' 	=> ['path' => $this->image_path],
					];

			$file2 	= [
						'name' 		=> 'image_active',
						'file_opt' 	=> ['path' => $this->image_path],
					];

			if($request->hasFile('image')){
				$image = $this->build_image($file);
				$this->model->image = $image;
			}else{
				if($request->input('remove-single-image-image') == 'y'){
					File::delete($file['file_opt']['path'].$this->model->image);
					$this->model->image = '';
				}
			}

			if($request->hasFile('image_active')){
				$image = $this->build_image($file2);
				$this->model->image_active = $image;
			}else{
				if($request->input('remove-single-image-image_active') == 'y'){
					File::delete($file['file_opt']['path'].$this->model->image_active);
					$this->model->image_active = '';
				}
			}

			$this->model->save();

			Alert::success('Successfully update Category');
			return redirect()->to($this->data['path']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		// $this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'image',
								'type' => 'image',
								'file_opt' => ['path' => $this->image_path]

							],[
								'name' => 'category_name',
								'type' => 'text'
							]
						];
	}

	public function sorting(){
		$this->model = $this->model->where('category_id',0)->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}
}
