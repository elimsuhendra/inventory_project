<?php namespace digipos\Http\Controllers\Admin;

use digipos\Libraries\Alert;
use digipos\Libraries\Email;

use digipos\models\Orderhd;
use digipos\models\Orderdt;
use digipos\models\Orderaddress;
use digipos\models\Orderbilling;
use digipos\models\Orderconfirm;
use digipos\models\Orderlog;
use digipos\models\Orderlogstock;
use digipos\models\Orderstatus;
use digipos\models\Ordervoucher;
use digipos\models\Payment_method;
use digipos\models\Product_category;
use digipos\models\Product_data_attribute_master;
use digipos\models\Socialmedia;

use App\Mail\MailOrder;
use Illuminate\Support\Facades\Mail;

use DB;
use Illuminate\Http\Request;
use PDF;

class OrdersController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title = "Orders";
		$this->root_link = "orders";
		$this->primary_field = "order_id";
		$this->model = new Orderhd;
	}

	public function index(){
		$this->field = [
			[
				'name' 	=> 'order_id',
				'label' => 'Order ID',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'first_name',
				'label' => 'First Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'total_amount',
				'label' => 'Total Amount',
				'sorting' => 'y',
				'search' => 'text',
				'before_show' => [
									'number_format' => ['decimal' => 0]
								]
			],[
				'name' => 'payment_method_id',
				'label' => 'Payment Method',
				'search' => 'select',
				'search_data' => $this->get_payment_method(),
				'belongto' => ['method' => 'payment_method','field' => 'payment_method_name'],
				'sorting' => 'y',
			],[
				'name' => 'order_status_id',
				'label' => 'Order Status',
				'search' => 'select',
				'search_data' => $this->get_order_status(),
				'belongto' => ['method' => 'orderstatus','field' => 'order_status_name'],
				'sorting' => 'y',
				'after_show' => [
									'label' => [
										'New Order' => 'warning',
										'Payment Confirmation' => 'info',
										'Payment Accepted' => 'primary',
										'Order Shipped' => 'default',
										'Completed' => 'success',
										'Order Cancelled' => 'danger',
									]
								]
			],[
				'name' => 'order_date',
				'label' => 'Order Date',
				'search' => 'text',
				'class' => 'readonly datepicker',
				'sorting' => 'y',
			],
		];
		$this->model 		 = Orderhd::join('orderbilling','orderhd.id','orderbilling.orderhd_id')->select('orderhd.*','orderbilling.first_name','orderbilling.last_name');
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model 					= $this->model->where('id',$id)->with('orderdt','orderbilling','orderaddress','orderlog.orderstatus','customer','orderconfirm','orderstatus')->first();
		$this->data['orderhd']			= $this->model;
		$this->data['orderdt']			= $this->model->orderdt;
		$this->data['orderbilling']		= $this->model->orderbilling;
		$this->data['orderaddress']		= $this->model->orderaddress;
		$this->data['orderlog']			= $this->model->orderlog;
		$this->data['customer']			= $this->model->customer;
		$this->data['orderconfirm']		= $this->model->orderconfirm;
		
		if($this->model->order_status_id == 1 || $this->model->order_status_id == 20){
			$this->data['orderstatus']		= Orderstatus::whereIn('id',[2,11])->pluck('order_status_name','id')->toArray();
		}else {
			$statusorder  					= [$this->model->order_status_id+1,12];
			if($this->model->order_status_id == 3){
				$statusorder  				= [$this->model->order_status_id+1,10];
			}
			$this->data['orderstatus']		= Orderstatus::wherebetween('id',$statusorder)->pluck('order_status_name','id')->toArray();
		}
		$this->data['title'] 			= 'View '.$this->title.' #'.$this->model->order_id;
		return $this->render_view('pages.orders.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model 					= $this->model->where('id',$id)->with('orderdt','orderbilling','orderaddress','orderlog.orderstatus','customer','orderconfirm','orderstatus')->first();
		$this->data['orderhd']			= $this->model;
		$this->data['orderdt']			= $this->model->orderdt;
		// dd($this->data['orderdt']);
		$this->data['orderbilling']		= $this->model->orderbilling;
		$this->data['orderaddress']		= $this->model->orderaddress;
		$this->data['orderlog']			= $this->model->orderlog;
		$this->data['customer']			= $this->model->customer;
		$this->data['orderconfirm']		= $this->model->orderconfirm;
		
		if($this->model->order_status_id == 1  || $this->model->order_status_id == 20){
			$this->data['orderstatus']		= Orderstatus::whereIn('id',[2,11])->pluck('order_status_name','id')->toArray();
		}else{
			$statusorder  					= [$this->model->order_status_id+1,12];
			if($this->model->order_status_id == 3){
				$statusorder  				= [$this->model->order_status_id+1,10];
			}
			$this->data['orderstatus']		= Orderstatus::wherebetween('id',$statusorder)->pluck('order_status_name','id')->toArray();
		}
		$this->data['title'] 			= 'Edit '.$this->title.' #'.$this->model->order_id;
		return $this->render_view('pages.orders.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(request $request,$id){
		$this->validate($request,[
			'order_status_id' => 'required'
		]);
		$query = DB::transaction(function() use($request,$id){
			$orderhd 					= $this->model->where('id',$id)->first();
			$order_status_id 			= $request->order_status_id;
			$orderhd->order_status_id 	= $order_status_id;
			$orderdt 					= $orderhd->orderdt;
			if($order_status_id == 3){
				$orderhd->tracking_code = $request->tracking_code;
				//Remove hold
				foreach($orderdt as $o){
					//Reduce from product
					Product_data_attribute_master::where('id',$o->product_data_attribute_master_id)->decrement('hold',$o->qty);
					Product_data_attribute_master::where('id',$o->product_data_attribute_master_id)->increment('sold',$o->qty);
					//Reduce from hold log
					$stocklog				= new Orderlogstock;
					$stocklog->orderdt_id	= $o->id;
					$stocklog->action       = 'sold';
					$stocklog->value 		= $o->qty;
					$o->orderlogstock()->save($stocklog);
				}

			}else if($order_status_id == 11){
				foreach($orderdt as $o){
					//Reduce from product
					Product_data_attribute_master::where('id',$o->product_data_attribute_master_id)->decrement('hold',$o->qty);
					Product_data_attribute_master::where('id',$o->product_data_attribute_master_id)->increment('stock',$o->qty);

					//Reduce from hold log
					$stocklog				= new Orderlogstock;
					$stocklog->orderdt_id	= $o->id;
					$stocklog->action       = 'return';
					$stocklog->value 		= $o->qty;
					$o->orderlogstock()->save($stocklog);
				}
			}

			//Log
			$orderlog = new Orderlog;
			$orderlog->orderhd_id	  	= $orderhd->id;
			$orderlog->order_status_id 	= $order_status_id;
			$orderlog->notes 		  	= $request->notes;
			$orderlog->upd_owner 		= 'a';
			$orderlog->upd_by		  	= auth()->guard($this->guard)->user()->id;
			
			$orderhd->save();
			$orderhd->orderlog()->save($orderlog);


			$orderstatus    = Orderstatus::find($orderhd->order_status_id);
			$orderhd  		= Orderhd::where('id', $id)->with('orderaddress','customer')->first();	

			if($this->data['send_email'] == 'y'/* && $this->data['send_sms'] == 'y'*/){
				//Send Email
				if($orderstatus->status_email_cust == 'y'){
					$subject = str_replace('#id',$orderhd->order_id,$orderstatus->subject);
					$title = str_replace('#id',$orderhd->order_id,$orderstatus->title);

					$dt_cust['subject'] = $subject;
					$dt_cust['to'] 		= $orderhd->customer->email;

					$this->data['title']        = $title;
					$this->data['status']		= 'cust';

					$change_name = ucfirst(str_replace('#cust_name',$orderhd->orderaddress->first_name,$orderstatus->email_cust_content));

					if($orderstatus->id == 3) {
						$change_name = str_replace('#no_resi',$request->tracking_code,$change_name);
						$change_name = str_replace('#kurir',$orderhd->orderaddress->delivery_service_name,$change_name);
					}

					$this->data['cust_content'] = $change_name;

					Mail::send('front.mail.content', $this->data, function ($message) use($dt_cust){
			        	$message->subject($dt_cust['subject'])
			              ->to($dt_cust['to']);
			         });
				}

				if($orderstatus->status_email_admin == 'y'){
					$subject = str_replace('#id',$orderhd->order_id,$orderstatus->subject);
					$title = str_replace('#id',$orderhd->order_id,$orderstatus->title);

					$dt_cust['subject'] = $subject;
					$dt_cust['to'] 		= $orderstatus->email_admin;

					$this->data['title']        = $title;
					$this->data['status']		= 'admin';

					$this->data['adm_content'] = $orderstatus->email_admin_content;

					Mail::send('front.mail.content', $this->data, function ($message) use($dt_cust){
			        	$message->subject($dt_cust['subject'])
			              ->to($dt_cust['to']);
			         });
				}
			}

			// 	$orderstatus 					= Orderstatus::find($orderhd->order_status_id);
			// 	$orderhd 						= $orderhd->with('orderdt','orderbilling','orderaddress','orderlog.orderstatus','customer','orderconfirm','orderstatus')->first();
			// 	$this->data['orderdt']			= $orderhd->orderdt;
			// 	$this->data['orderbilling']		= $orderhd->orderbilling;
			// 	$this->data['orderaddress']		= $orderhd->orderaddress;
			// 	$this->data['customer']			= $orderhd->customer;
			// 	$this->data['orderhd']		 	= $orderhd;
			// 	$this->data['sociallink']   	= Socialmedia::where('status','y')->get();
		 //        $this->data['category']     	= Product_category::where('category_id',0)->where('status','y')->orderBy('order','asc')->get();

			// 	if($this->data['send_email'] == 'y'){
			// 		//Replace Specific data
			// 		$orderstatus->title 			 = str_replace('#id',$orderhd->order_id,$orderstatus->title);
			// 		$orderstatus->subject 			 = str_replace('#id',$orderhd->order_id,$orderstatus->subject);
			// 		$orderstatus->email_cust_content = str_replace(
			// 			['#id'],
			// 			[$orderhd->id],
			// 			$orderstatus->email_cust_content
			// 		);

			// 		$orderstatus->email_admin_content = str_replace(
			// 			['#id'],
			// 			[$orderhd->id],
			// 			$orderstatus->email_admin_content
			// 		);

			// 		//Set Data
			//        	if($orderstatus->status_email_cust == 'y'){
			// 			$this->data['email_status'] = 'cust';
			// 			Email::to($orderhd->customer->email);
			// 			Email::subject($orderstatus->subject);
			// 			Email::view('emails.order');
			// 			Email::email_data($this->data);
			// 			Email::send();
			// 		}

			// 		if($orderstatus->status_email_admin == 'y'){
			// 			$admin 	= explode(',',$orderstatus->email_admin);
			// 			foreach($admin as $a){
			// 				$this->data['email_status'] = 'admin';
			// 				Email::to($a);
			// 				Email::subject($orderstatus->subject);
			// 				Email::view('emails.order');
			// 				Email::email_data($this->data);
			// 				Email::send();
			// 			}
			// 		}
			// 	}

			// 	if($this->data['send_sms'] == 'y'){
			// 		//Send SMS
			// 		if($this->data['fgsms'] == 'y'){
			// 			$userkey			="bookmedev"; // userkey di SMS Notifikasi //
			// 			$passkey			="d1amond"; // passkey di SMS Notifikasi //
			// 			$url  				= "http://reguler.sms-notifikasi.com/apps/smsapi.php";
			// 			if($orderstatus->fgsms_cust == 'y'){
			// 				$isi_email_cust = $orderstatus->isi_sms_cust;
			// 				$array = ['#id' => $orderhd->order_id,'#total' => $orderhd->total,'#tracking_code' => $orderhd->tracking_code];
			// 				foreach($array as $a => $q){
			// 					$isi_email_cust = str_replace($a,$q,$isi_email_cust);
			// 				}
			// 				$telepon 			= $orderhd->orderbilling->phone;
			// 				$message 			= $isi_email_cust;

			// 				$curlHandle 		= curl_init();
			// 				curl_setopt($curlHandle, CURLOPT_URL, $url);
			// 				curl_setopt($curlHandle, CURLOPT_POSTFIELDS, "userkey=".$userkey."&passkey=".$passkey."&nohp=".$telepon.
			// 				"&pesan=".urlencode($message));
			// 				curl_setopt($curlHandle, CURLOPT_HEADER, 0);
			// 				curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			// 				curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
			// 				curl_setopt($curlHandle, CURLOPT_POST, 1);
			// 				$results = curl_exec($curlHandle);
			// 				curl_close($curlHandle);
			// 			}

			// 			if($orderstatus->fgsms_admin == 'y'){

			// 				$isi_email_admin = $orderstatus->isi_sms_admin;
			// 				$array = ['#id' => $orderhd->order_id,'#total' => $orderhd->total,'#tracking_code' => $orderhd->tracking_code];
			// 				foreach($array as $a => $q){
			// 					$isi_email_admin = str_replace($a,$q,$isi_email_admin);
			// 				}
			// 				$telepon 			= explode(',',$orderstatus->phone_admin);
			// 				$message 			= $isi_email_admin;

			// 				foreach($telepon as $t){
			// 					$curlHandle 		= curl_init();
			// 					curl_setopt($curlHandle, CURLOPT_URL, $url);
			// 					curl_setopt($curlHandle, CURLOPT_POSTFIELDS, "userkey=".$userkey."&passkey=".$passkey."&nohp=".$t.
			// 					"&pesan=".urlencode($message));
			// 					curl_setopt($curlHandle, CURLOPT_HEADER, 0);
			// 					curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			// 					curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
			// 					curl_setopt($curlHandle, CURLOPT_POST, 1);
			// 					$results = curl_exec($curlHandle);
			// 					curl_close($curlHandle);
			// 				}
			// 			}
			// 		}
			// 	}
			// }
			return 'success';
		});
		Alert::success('Successfuly update order status');
		return redirect()->back();
	}

	public function ext(request $request,$action){
		$this->data['request'] 	= $request;
		return $this->$action();
	}

	public function export(){
		return $this->build_export();
	}

	public function invoice(){
		$request 			= $this->data['request'];
		$data 				= $this->data;
		$data['order']   	= $this->model->where('id',$request->id)->with('orderdt','orderbilling','orderaddress','orderlog.orderstatus','customer','orderconfirm','orderstatus')->first();
		$view 				= $this->view_path.'.pdf.invoice';
		$title				= 'Invoice order #'.$data['order']->order_id.'.pdf';
		//PDF
		$pdf 				= PDF::loadView($view, $data);
		return $pdf->setPaper('a4')->download($title);
	}

	public function delivery_label(){
		$request 			= $this->data['request'];
		$data 				= $this->data;
		$data['order']   	= $this->model->where('id',$request->id)->with('orderdt','orderbilling','orderaddress','orderlog.orderstatus','customer','orderconfirm','orderstatus')->first();
		$view 				= $this->view_path.'.pdf.delivery';
		$title				= 'Invoice order #'.$data['order']->order_id.'.pdf';
		//PDF
		$pdf 				= PDF::loadView($view, $data);
		return $pdf->setPaper('a4')->download($title);
	}

	public function get_payment_method(){
		$query = Payment_method::orderBy('id','asc')->pluck('payment_method_name','id')->toArray();
		return $query;
	}

	public function get_order_status(){
		$query = Orderstatus::where('id','<',21)->orderBy('id','asc')->pluck('order_status_name','id')->toArray();
		return $query;
	}

	public function getChangestatusorder(){
 		$cekorder = Tborderhd::whereIn('fkorderstatusid',[1,3])->get();
 		$i = 0;
 		$half_selisih = $this->data['deadline_payment'];
 		// $half_selisih = -1;
 		foreach($cekorder as $orderhd){
 			$date1 = date_format(date_create($orderhd->updated_at),'Y-m-d');
			$date2 = date('Y-m-d');
	 		$selisih = ((abs(strtotime ($date1) - strtotime ($date2)))/(60*60*24));
 			if($selisih > $half_selisih){
				$fkorderstatusid = $orderhd->fkorderstatusid == 1 ? 11 : 4;
				$orderdt = $orderhd->orderdt;
				if($fkorderstatusid == 11){
					foreach($orderdt as $o){
						if($o->fgstock == 'y'){
							//Reduce from product
							Tbproductstock::where('id',$o->fkproductstockid)->decrement('hold',$o->qty);
							Tbproductstock::where('id',$o->fkproductstockid)->increment('stock',$o->qty);

							//Reduce from hold log
							$stocklog				= new Tborderlogstock;
							$stocklog->fkorderdtid	= $o->id;
							$stocklog->action       = 'return';
							$stocklog->value 		= $o->qty;
							$o->orderlogstock()->save($stocklog);

							//Return Point
							$point  = $orderhd->total_point/$orderhd->point_value;
							$point 	= (float)$point;

							$cust 					= $orderhd->cust;
							$cust->total_point 		+= $point;
							$cust->save();

							$pointlog 				= new Tbpointlog;
							$pointlog->fkcustid 	= $cust->id;
							$pointlog->fkorderhdid 	= $orderhd->id;
							$pointlog->action 		= 'return';
							$pointlog->value 		= $orderhd->total_point_order;
							$pointlog->description  = 'Return point to customer because order cancelled';
							$pointlog->upd_by		= Auth::user()->id;
							$pointlog->save();
						}
					}
					$notes = 'Order Canceled';
				}else{
					$notes = 'Order Completed';
				}
				$orderhd->fkorderstatusid 	= $fkorderstatusid;
				//Log
				$log = new Tborderlog;
				$log->fkorderhdid	  = $orderhd->id;
				$log->fkorderstatusid = $fkorderstatusid;
				$log->notes 		  = $notes;
				$log->upd_by		  = Auth::user()->id;
				$orderhd->save();
				$orderhd->orderlog()->save($log);
				
				//Send Email
				$orderstatus = Tborderstatus::find($orderhd->fkorderstatusid);

				//Format data for email
				$primary_currency = Tbcurrency::find($orderhd->fkcurrencyid);
				$orderhd = $orderhd->where('id',$orderhd->id)->with('orderstatus','ordervoucher','paymentmethod','orderaddress.shippingvendor')->first();
				if($orderhd->discount_type == 'voucher'){
					$orderhd->total_voucher = Currency::convert($orderhd->total_voucher,$primary_currency);
				}
				$orderhd->total_point 		= Currency::convert($orderhd->total_point,$primary_currency);
				$orderhd->total_gift_wrap 	= Currency::convert($orderhd->total_gift_wrap,$primary_currency);
				$orderhd->sub_total 		= Currency::convert($orderhd->sub_total,$primary_currency);
				$orderhd->total_ongkir		= Currency::convert($orderhd->total_ongkir,$primary_currency);
				$orderhd->total 			= Currency::convert($orderhd->total,$primary_currency);
				if($this->data['fgemail'] == 'y'){
					//Replace Specific data
					$orderstatus->title 			= str_replace('#id',$orderhd->order_id,$orderstatus->title);
					$orderstatus->subject 			= str_replace('#id',$orderhd->order_id,$orderstatus->subject);
					$orderstatus->isi_email_cust 	= str_replace(
						['#id'],
						[$orderhd->id],
						$orderstatus->isi_email_cust
					);

					$orderstatus->isi_email_admin 	= str_replace(
						['#id'],
						[$orderhd->id],
						$orderstatus->isi_email_cust
					);

					//Set Data
					$this->data['orderhd'] 		= $orderhd;
					$this->data['bonusitem']   	= json_decode($this->data['orderhd']->bonus_item);

					foreach($orderhd->orderdt as $dt){
						$dt->price = Currency::convert($dt->price,$primary_currency);
						$dt->total = Currency::convert($dt->total,$primary_currency);	
						$datadt[] = $dt;
					}
					$this->data['orderdt'] 		= $datadt;
					$this->data['orderbilling'] = $orderhd->orderbilling;
					$this->data['orderaddress'] = $orderhd->orderaddress;
					$this->data['orderstatus'] 	= $orderstatus;
					$this->data['sociallink']   = Tbsociallink::where('fgstatus','y')->get();
			        $this->data['category']     = Tbcategory::where('fkparentid',0)->where('fgstatus','y')->orderBy('order','asc')->get();

					if($orderstatus->fgemail_cust == 'y'){
						$this->data['email_status'] = 'cust';
						Email::to($orderhd->cust->email);
						Email::subject($orderstatus->subject);
						Email::view('emails.order');
						Email::email_data($this->data);
						Email::send();
					}


					if($orderstatus->fgemail_admin == 'y'){
						$admin 	= explode(',',$orderstatus->email_admin);
						foreach($admin as $a){
							$this->data['email_status'] = 'admin';
							Email::to($a);
							Email::subject($orderstatus->subject);
							Email::view('emails.order');
							Email::email_data($this->data);
							Email::send();
						}
					}
				}

				//Send SMS
				if($this->data['fgsms'] == 'y'){
					$userkey			="bookmedev"; // userkey di SMS Notifikasi //
					$passkey			="d1amond"; // passkey di SMS Notifikasi //
					$url  				= "http://reguler.sms-notifikasi.com/apps/smsapi.php";
					if($orderstatus->fgsms_cust == 'y'){
						$isi_email_cust = $orderstatus->isi_sms_cust;
						$array = ['#id' => $orderhd->order_id,'#total' => $orderhd->total,'#tracking_code' => $orderhd->tracking_code];
						foreach($array as $a => $q){
							$isi_email_cust = str_replace($a,$q,$isi_email_cust);
						}
						$telepon 			= $orderhd->orderbilling->phone;
						$message 			= $isi_email_cust;

						$curlHandle 		= curl_init();
						curl_setopt($curlHandle, CURLOPT_URL, $url);
						curl_setopt($curlHandle, CURLOPT_POSTFIELDS, "userkey=".$userkey."&passkey=".$passkey."&nohp=".$telepon.
						"&pesan=".urlencode($message));
						curl_setopt($curlHandle, CURLOPT_HEADER, 0);
						curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
						curl_setopt($curlHandle, CURLOPT_POST, 1);
						$results = curl_exec($curlHandle);
						curl_close($curlHandle);
					}

					if($orderstatus->fgsms_admin == 'y'){

						$isi_email_admin = $orderstatus->isi_sms_admin;
						$array = ['#id' => $orderhd->order_id,'#total' => $orderhd->total,'#tracking_code' => $orderhd->tracking_code];
						foreach($array as $a => $q){
							$isi_email_admin = str_replace($a,$q,$isi_email_admin);
						}
						$telepon 			= explode(',',$orderstatus->phone_admin);
						$message 			= $isi_email_admin;

						foreach($telepon as $t){
							$curlHandle 		= curl_init();
							curl_setopt($curlHandle, CURLOPT_URL, $url);
							curl_setopt($curlHandle, CURLOPT_POSTFIELDS, "userkey=".$userkey."&passkey=".$passkey."&nohp=".$t.
							"&pesan=".urlencode($message));
							curl_setopt($curlHandle, CURLOPT_HEADER, 0);
							curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
							curl_setopt($curlHandle, CURLOPT_POST, 1);
							$results = curl_exec($curlHandle);
							curl_close($curlHandle);
						}
					}
				}
	 			$i++;
 			}
 		}
 		echo $i;
	}
}
