<?php namespace digipos\Http\Controllers\Admin;

use digipos\Libraries\Alert;
use digipos\Libraries\Email;

use digipos\models\Orderhd;
use digipos\models\Orderdt;
use digipos\models\Orderaddress;
use digipos\models\Orderbilling;
use digipos\models\Orderconfirm;
use digipos\models\Orderlog;
use digipos\models\Orderlogstock;
use digipos\models\Orderstatus;
use digipos\models\Ordervoucher;
use digipos\models\Payment_method;
use digipos\models\Product_category;
use digipos\models\Product_data_attribute_master;
use digipos\models\Socialmedia;

use App\Mail\MailOrder;
use Illuminate\Support\Facades\Mail;

use DB;
use Illuminate\Http\Request;
use PDF;

class IncomeReportController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title = "Orders";
		$this->root_link = "orders";
		$this->primary_field = "order_id";
		$this->model = new Orderhd;
	}

	public function index(){
		
		// $this->model 		 = Orderhd::join('orderbilling','orderhd.id','orderbilling.orderhd_id')->select('orderhd.*','orderbilling.first_name','orderbilling.last_name');
		$this->data['title'] = 'Income Report';
		$this->data['order_status'] 		= Orderstatus::where('id', 4)->get();
		return $this->render_view('pages.report.income_report');
	}

	public function ext(request $request, $action){
		return $this->$action($request);
	}


	public function export(){
		return $this->build_export();
	}

	public function invoice(){
		$request 			= $this->data['request'];
		$data 				= $this->data;
		$data['order']   	= $this->model->where('id',$request->id)->with('orderdt','orderbilling','orderaddress','orderlog.orderstatus','customer','orderconfirm','orderstatus')->first();
		$view 				= $this->view_path.'.pdf.invoice';
		$title				= 'Invoice order #'.$data['order']->order_id.'.pdf';
		//PDF
		$pdf 				= PDF::loadView($view, $data);
		return $pdf->setPaper('a4')->download($title);
	}

	public function delivery_label(){
		$request 			= $this->data['request'];
		$data 				= $this->data;
		$data['order']   	= $this->model->where('id',$request->id)->with('orderdt','orderbilling','orderaddress','orderlog.orderstatus','customer','orderconfirm','orderstatus')->first();
		$view 				= $this->view_path.'.pdf.delivery';
		$title				= 'Invoice order #'.$data['order']->order_id.'.pdf';
		//PDF
		$pdf 				= PDF::loadView($view, $data);
		return $pdf->setPaper('a4')->download($title);
	}

	public function get_payment_method(){
		$query = Payment_method::orderBy('id','asc')->pluck('payment_method_name','id')->toArray();
		return $query;
	}

	public function get_order_status(){
		$query = Orderstatus::where('id','<',21)->orderBy('id','asc')->pluck('order_status_name','id')->toArray();
		return $query;
	}

	public function getChangestatusorder(){
 		$cekorder = Tborderhd::whereIn('fkorderstatusid',[1,3])->get();
 		$i = 0;
 		$half_selisih = $this->data['deadline_payment'];
 		// $half_selisih = -1;
 		foreach($cekorder as $orderhd){
 			$date1 = date_format(date_create($orderhd->updated_at),'Y-m-d');
			$date2 = date('Y-m-d');
	 		$selisih = ((abs(strtotime ($date1) - strtotime ($date2)))/(60*60*24));
 			if($selisih > $half_selisih){
				$fkorderstatusid = $orderhd->fkorderstatusid == 1 ? 11 : 4;
				$orderdt = $orderhd->orderdt;
				if($fkorderstatusid == 11){
					foreach($orderdt as $o){
						if($o->fgstock == 'y'){
							//Reduce from product
							Tbproductstock::where('id',$o->fkproductstockid)->decrement('hold',$o->qty);
							Tbproductstock::where('id',$o->fkproductstockid)->increment('stock',$o->qty);

							//Reduce from hold log
							$stocklog				= new Tborderlogstock;
							$stocklog->fkorderdtid	= $o->id;
							$stocklog->action       = 'return';
							$stocklog->value 		= $o->qty;
							$o->orderlogstock()->save($stocklog);

							//Return Point
							$point  = $orderhd->total_point/$orderhd->point_value;
							$point 	= (float)$point;

							$cust 					= $orderhd->cust;
							$cust->total_point 		+= $point;
							$cust->save();

							$pointlog 				= new Tbpointlog;
							$pointlog->fkcustid 	= $cust->id;
							$pointlog->fkorderhdid 	= $orderhd->id;
							$pointlog->action 		= 'return';
							$pointlog->value 		= $orderhd->total_point_order;
							$pointlog->description  = 'Return point to customer because order cancelled';
							$pointlog->upd_by		= Auth::user()->id;
							$pointlog->save();
						}
					}
					$notes = 'Order Canceled';
				}else{
					$notes = 'Order Completed';
				}
				$orderhd->fkorderstatusid 	= $fkorderstatusid;
				//Log
				$log = new Tborderlog;
				$log->fkorderhdid	  = $orderhd->id;
				$log->fkorderstatusid = $fkorderstatusid;
				$log->notes 		  = $notes;
				$log->upd_by		  = Auth::user()->id;
				$orderhd->save();
				$orderhd->orderlog()->save($log);
				
				//Send Email
				$orderstatus = Tborderstatus::find($orderhd->fkorderstatusid);

				//Format data for email
				$primary_currency = Tbcurrency::find($orderhd->fkcurrencyid);
				$orderhd = $orderhd->where('id',$orderhd->id)->with('orderstatus','ordervoucher','paymentmethod','orderaddress.shippingvendor')->first();
				if($orderhd->discount_type == 'voucher'){
					$orderhd->total_voucher = Currency::convert($orderhd->total_voucher,$primary_currency);
				}
				$orderhd->total_point 		= Currency::convert($orderhd->total_point,$primary_currency);
				$orderhd->total_gift_wrap 	= Currency::convert($orderhd->total_gift_wrap,$primary_currency);
				$orderhd->sub_total 		= Currency::convert($orderhd->sub_total,$primary_currency);
				$orderhd->total_ongkir		= Currency::convert($orderhd->total_ongkir,$primary_currency);
				$orderhd->total 			= Currency::convert($orderhd->total,$primary_currency);
				if($this->data['fgemail'] == 'y'){
					//Replace Specific data
					$orderstatus->title 			= str_replace('#id',$orderhd->order_id,$orderstatus->title);
					$orderstatus->subject 			= str_replace('#id',$orderhd->order_id,$orderstatus->subject);
					$orderstatus->isi_email_cust 	= str_replace(
						['#id'],
						[$orderhd->id],
						$orderstatus->isi_email_cust
					);

					$orderstatus->isi_email_admin 	= str_replace(
						['#id'],
						[$orderhd->id],
						$orderstatus->isi_email_cust
					);

					//Set Data
					$this->data['orderhd'] 		= $orderhd;
					$this->data['bonusitem']   	= json_decode($this->data['orderhd']->bonus_item);

					foreach($orderhd->orderdt as $dt){
						$dt->price = Currency::convert($dt->price,$primary_currency);
						$dt->total = Currency::convert($dt->total,$primary_currency);	
						$datadt[] = $dt;
					}
					$this->data['orderdt'] 		= $datadt;
					$this->data['orderbilling'] = $orderhd->orderbilling;
					$this->data['orderaddress'] = $orderhd->orderaddress;
					$this->data['orderstatus'] 	= $orderstatus;
					$this->data['sociallink']   = Tbsociallink::where('fgstatus','y')->get();
			        $this->data['category']     = Tbcategory::where('fkparentid',0)->where('fgstatus','y')->orderBy('order','asc')->get();

					if($orderstatus->fgemail_cust == 'y'){
						$this->data['email_status'] = 'cust';
						Email::to($orderhd->cust->email);
						Email::subject($orderstatus->subject);
						Email::view('emails.order');
						Email::email_data($this->data);
						Email::send();
					}


					if($orderstatus->fgemail_admin == 'y'){
						$admin 	= explode(',',$orderstatus->email_admin);
						foreach($admin as $a){
							$this->data['email_status'] = 'admin';
							Email::to($a);
							Email::subject($orderstatus->subject);
							Email::view('emails.order');
							Email::email_data($this->data);
							Email::send();
						}
					}
				}

				//Send SMS
				if($this->data['fgsms'] == 'y'){
					$userkey			="bookmedev"; // userkey di SMS Notifikasi //
					$passkey			="d1amond"; // passkey di SMS Notifikasi //
					$url  				= "http://reguler.sms-notifikasi.com/apps/smsapi.php";
					if($orderstatus->fgsms_cust == 'y'){
						$isi_email_cust = $orderstatus->isi_sms_cust;
						$array = ['#id' => $orderhd->order_id,'#total' => $orderhd->total,'#tracking_code' => $orderhd->tracking_code];
						foreach($array as $a => $q){
							$isi_email_cust = str_replace($a,$q,$isi_email_cust);
						}
						$telepon 			= $orderhd->orderbilling->phone;
						$message 			= $isi_email_cust;

						$curlHandle 		= curl_init();
						curl_setopt($curlHandle, CURLOPT_URL, $url);
						curl_setopt($curlHandle, CURLOPT_POSTFIELDS, "userkey=".$userkey."&passkey=".$passkey."&nohp=".$telepon.
						"&pesan=".urlencode($message));
						curl_setopt($curlHandle, CURLOPT_HEADER, 0);
						curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
						curl_setopt($curlHandle, CURLOPT_POST, 1);
						$results = curl_exec($curlHandle);
						curl_close($curlHandle);
					}

					if($orderstatus->fgsms_admin == 'y'){

						$isi_email_admin = $orderstatus->isi_sms_admin;
						$array = ['#id' => $orderhd->order_id,'#total' => $orderhd->total,'#tracking_code' => $orderhd->tracking_code];
						foreach($array as $a => $q){
							$isi_email_admin = str_replace($a,$q,$isi_email_admin);
						}
						$telepon 			= explode(',',$orderstatus->phone_admin);
						$message 			= $isi_email_admin;

						foreach($telepon as $t){
							$curlHandle 		= curl_init();
							curl_setopt($curlHandle, CURLOPT_URL, $url);
							curl_setopt($curlHandle, CURLOPT_POSTFIELDS, "userkey=".$userkey."&passkey=".$passkey."&nohp=".$t.
							"&pesan=".urlencode($message));
							curl_setopt($curlHandle, CURLOPT_HEADER, 0);
							curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
							curl_setopt($curlHandle, CURLOPT_POST, 1);
							$results = curl_exec($curlHandle);
							curl_close($curlHandle);
						}
					}
				}
	 			$i++;
 			}
 		}
 		echo $i;
	}

	public function filter($request){
		// dd( $this->model->where('id',$request->id)->with('orderdt','orderbilling','orderaddress','orderlog.orderstatus','customer','orderconfirm','orderstatus')->first());
		// $outlet_access = $this->myStore2();

		$query = $this->model->leftJoin('customer', 'customer.id', 'orderhd.customer_id')->leftJoin('orderstatus', 'orderstatus.id', 'orderhd.order_status_id');
		
		// return $request->input('outlet');
		if($request->input('order_status') != ''){
			if(in_array('0', $request->input('order_status'))){
				
			}else{
				$query->whereIn('order_status_id', $request->input('order_status'));
			}
		}else{
			return '<div style="text-align:center;color:red;">Please select Order Status !</div>';
		}
		
		$query2 = Orderdt::OrderBy('orderhd_id')->select('orderdt.*');
		// $product 	= $this->product->where('status', 'y');
		// $outlet 	= $this->outlet->where('status', 'y')->get();

		if($request->input('search_date_from') != ""){
			// dd($request->input('search_date_from'));
			$search_date_from = date('d-m-Y', strtotime($request->input('search_date_from') . ' -1 day'));
			$search_from = $this->displayToSql($search_date_from);
			// dd($search_from);
			$query->whereDate('orderhd.created_at', '>=', $search_from);
		}

		if($request->input('search_date_to') != ""){
			$search_to = $this->displayToSql($request->input('search_date_to'));
			$query->whereDate('orderhd.created_at', '<=', $search_to);
		}

		$count_product = 0;

		$display = "";
		$get_last = "";
		

		// $mode = 2; //mode header
		$mode = 3;
		if($mode == 2){
			$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Header</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Product Name</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Barcode</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Total Item</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Type</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Price (Rp)</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Subtotal (Rp)</th>
		                </tr>
		              </thead>
		              <tbody>';
		}elseif($mode ==3){
			$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Date</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Order Id</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Product Code</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Product Name</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Attribute</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Price</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Qty</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Weight</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Total Weight</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Discount</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Discount Type</th>
		                  <th class="bg-blue-madison font-white" style="text-align:center;">Total Price (Rp)</th>
		                </tr>
		              </thead>
		              <tbody>';
		}
		$query->select('orderhd.*')->orderBy('orderhd.id'); 
		foreach($query->get() as $odr){
			// $display .= '<tr>
			// 				<td colspan="6" class="bg-dark font-white order_header">Office: '.$odr->outlet_name.' - Order Id: '.$odr->order_id.' - sales: '.$odr->user_name.'<br>'.date('d - F - Y', strtotime($odr->order_date)).' - cust: '.$odr->name.' - Payment Type: '.$odr->payment_type.'</td>
			// 			</tr>';
			$curr_orderhd_id = $odr->id;
			$curr_subtotal = 0;
			$flagfind = 0;
			$flagFirst = 0;
			foreach($query2->get() as $key => $odc){
				if($curr_orderhd_id == $odc->orderhd_id){
					$flagfind 		= 1;
					$subtotal 		= $odc->price;
					$curr_subtotal += $subtotal;
					$total_item   	= 1;
					if($odc->type == 'dompul'){
						$total_item 	= $odc->total_item;
						$subtotal 		= $odc->price * $odc->total_item;
					}

					$display .= '<tr>';
					if($mode == 2){
						if($flagFirst == 0){
							$display .= '<td>Office: '.$odr->outlet_name.' - Order Id: '.$odr->order_id.' - sales: '.$odr->user_name.'<br>'.date('d - F - Y', strtotime($odr->order_date)).' - cust: '.$odr->name.' - Payment Type: '.$odr->payment_type.'</td>';
							$flagFirst = 1;
						}else{
							$display .= '<td></td>';
						}
						$display .=	'<td>'.$odc->name.'</td>
									<td>'.$odc->barcode.'</td>
									<td>'.$total_item.'</td>
									<td>'.$odc->type.'</td>	
									<td>'. number_format($odc->price).'</td>
									<td>'. number_format($subtotal).'</td>
								</tr>';
					}elseif($mode == 3){
						$date_order = date('d/m/Y', strtotime($odc->created_at));
						$attribute = $odr->attribute_description;
						$display .=	'<td>'.$date_order.'</td>
										<td>'.$odr->order_id.'</td>
										<td>'.$odc->product_code.'</td>
										<td>'.$odc->product_name.'</td>	
										<td>'.$attribute.'</td>
										<td>'.number_format($odr->price).'</td>
										<td>'.$odc->qty.'</td>
										<td>'.$odc->weight.'</td>
										<td>'.$odc->total_weight.'</td>	
										<td>'.$odc->discount.'</td>
										<td>'.$odc->discount_type.'</td>
										<td>'. number_format($odc->total_price).'</td>
									</tr>';
					}
					
				}else{
					if($flagfind == 1){
						break;
					}
				}

			}
			if($flagfind == 1){
				$date_order = date('d/m/Y', strtotime($odr->created_at));
				$display .= '<tr><td>'.$date_order.'</td><td colspan="10" style="text-align: right;">Subtotal Amount (Rp.)</td><td>'.number_format($odr->sub_total_amount).'</td></tr>
						<tr><td colspan="11" style="text-align: right;">Voucher Amount (Rp.)</td><td>'.number_format($odr->voucher_amount).'</td></tr>
						<tr><td colspan="11" style="text-align: right;">Total Shipping Amount (Rp.)</td><td>'.number_format($odr->total_shipping_amount).'</td></tr>
						<tr><td colspan="11" style="text-align: right;">Total Amount (Rp.)</td><td>'.number_format($odr->total_amount)	.'</td></tr>';
			}	
			
		}

		$display .='</tbody>
					</table>
					</div>';

		return $display;
	}
}
