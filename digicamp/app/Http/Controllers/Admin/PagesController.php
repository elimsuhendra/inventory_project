<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Pages;
use Request;


class PagesController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title = trans('general.pages');
		$this->root_link = "pages";
		$this->primary_field = "pages_name";
		$this->model = new Pages;
		$this->bulk_action = true;
		$this->bulk_action_data   = [2];
		$this->image_path 	= 'components/front/images/pages/';
		$this->tab_data 		= [
									'general'	=> 'General',
									'seo'		=> 'SEO'
								];
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'image',
				'label' => trans('general.image'),
				'sorting' => 'y',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],[
				'name' => 'pages_name',
				'label' => trans('general.pages_name'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'pages_name',
						'label' => trans('general.pages_name'),
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
						'alias' => 'pages_name_alias',
						'tab'	=> 'general'
					],[
						'name' => 'content',
						'label' => trans('general.content'),
						'type' => 'textarea',
						'class'	=> 'editor',
						'tab'	=> 'general'
					],[
						'name' => 'meta_title',
						'label' => trans('general.meta-title'),
						'type' => 'text',
						'tab'	=> 'seo'
					],[
						'name' => 'meta_description',
						'label' => trans('general.meta-description'),
						'type' => 'textarea',
						'tab'	=> 'seo'
					],[
						'name' => 'meta_keywords',
						'label' => trans('general.meta-keywords'),
						'type' => 'textarea',
						'tab'	=> 'seo'
					],[
						'name' => 'image',
						'label' => trans('general.image'),
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1024 x 600',
						'tab'	=> 'general'
					],[
						'name' => 'status',
						'label' => trans('general.status'),
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required',
						'tab'	=> 'general'
					]
				];
		return $field;
	}

	public function field_edit(){
		$path = explode('/',Request::path());
		$flag = 0;
		if(isset($path[4])){
			if($path[4] == 6){
				$flag = 1;
			}
			
		}
		if($flag == 1){
			$field = [
					[
						'name' => 'pages_name',
						'label' => trans('general.pages_name'),
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
						'alias' => 'pages_name_alias',
						'tab'	=> 'general'
					],[
						'name' => 'content',
						'label' => trans('general.content'),
						'type' => 'textarea',
						'class'	=> 'editor',
						'tab'	=> 'general'
					],[
						'name' => 'meta_title',
						'label' => trans('general.meta-title'),
						'type' => 'text',
						'tab'	=> 'seo'
					],[
						'name' => 'meta_description',
						'label' => trans('general.meta-description'),
						'type' => 'textarea',
						'tab'	=> 'seo'
					],[
						'name' => 'meta_keywords',
						'label' => trans('general.meta-keywords'),
						'type' => 'textarea',
						'tab'	=> 'seo'
					],[
						'name' => 'image',
						'label' => trans('general.image'),
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1024 x 600',
						'tab'	=> 'general'
					],[
						'name' => 'image_2',
						'label' => trans('general.image_2'),
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1024 x 600',
						'tab'	=> 'general'
					],[
						'name' => 'status',
						'label' => trans('general.status'),
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required',
						'tab'	=> 'general'
					]
				];
		}else{			
			$field = [
				[
					'name' => 'pages_name',
					'label' => trans('general.pages_name'),
					'type' => 'text',
					'attribute' => 'required autofocus',
					'validation' => 'required',
					'not_same' => 'y',
					'alias' => 'pages_name_alias',
					'tab'	=> 'general'
				],[
					'name' => 'content',
					'label' => trans('general.content'),
					'type' => 'textarea',
					'class'	=> 'editor',
					'tab'	=> 'general'
				],[
					'name' => 'meta_title',
					'label' => trans('general.meta-title'),
					'type' => 'text',
					'tab'	=> 'seo'
				],[
					'name' => 'meta_description',
					'label' => trans('general.meta-description'),
					'type' => 'textarea',
					'tab'	=> 'seo'
				],[
					'name' => 'meta_keywords',
					'label' => trans('general.meta-keywords'),
					'type' => 'textarea',
					'tab'	=> 'seo'
				],[
					'name' => 'image',
					'label' => trans('general.image'),
					'type' => 'file',
					'file_opt' => ['path' => $this->image_path],
					'upload_type' => 'single-image',
					'form_class' => 'col-md-6 pad-left',
					'validation' => 'mimes:jpeg,png,jpg,gif',
					'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1024 x 600',
					'tab'	=> 'general'
				],[
					'name' => 'status',
					'label' => trans('general.status'),
					'type' => 'radio',
					'data' => ['y' => 'Active','n' => 'Not Active'],
					'attribute' => 'required',
					'validation' => 'required',
					'tab'	=> 'general'
				]
			];
		}
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'image',
								'type' => 'image',
								'file_opt' => ['path' => $this->image_path]

							],[
								'name' => 'pages_name',
								'type' => 'text'
							]
						];
	}

	public function sorting(){
		$this->model = $this->model->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}
}
