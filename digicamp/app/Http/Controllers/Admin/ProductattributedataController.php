<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Product_attribute;
use digipos\models\Product_attribute_data;

use Illuminate\Http\Request;
use digipos\Libraries\Alert;
use File;

class ProductattributedataController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.product-attribute-data');
		$this->root_link 		= "product-attribute-data";
		$this->primary_field 	= "product_attribute_data_name";
		$this->model 			= new Product_attribute_data;
		$this->bulk_action 		= true;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'name',
				'label' => 'Attribute Data Name',
				'sorting' => 'y',
				'type' => 'text',
			],[
				'name' => 'value',
				'label' => 'Value',
				'sorting' => 'y',
				'type' => 'text',
			],[
				'name' => 'attribute_id',
				'label' => 'Attribute',
				'search' => 'select',
				'search_data' => $this->get_product_attribute(),
				'sorting' => 'y',
				'belongto' => ['method' => 'attribute','field' => 'attribute_name']
			]
		];
		return $this->build('index');
	}


	public function create(){
		$this->data['title']			= trans('general.create').' '.$this->title;
		$this->data['product_attribute'] = Product_attribute::OrderBy('order','asc')->get();
		return $this->render_view('pages.product_attribute.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(request $request){
		$this->validate($request,[
			'attribute_id'		=> 'required',
			'name'				=> 'required|unique:product_attribute_data'
		]);
		if($request->value_color == '' && $request->value_select == ''){
			Alert::fail("Value Can't empty");
			return redirect()->back();
		}

		$this->model->attribute_id 		= $request->attribute_id;
		$this->model->name 				= $request->name;
		$attr 							= $this->model->attribute->attribute_type;
		if($attr == 'color'){
			$value 						= $request->value_color;
		}else{
			$value 						= $request->value_select;
		}
		$check 							= Product_attribute_data::where('attribute_id',$request->attribute_id)->where('value',$value)->first();
		if($check){
			Alert::fail("Value already exist");
			return redirect()->back();
		}
		$this->model->value 			= $value;
		$this->model->save();
		
		Alert::success('Successfully add new '.$this->title);
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['product_attribute_data']  = $this->model->find($id);
		$this->data['product_attribute'] 	= Product_attribute::OrderBy('order','asc')->get();
		$this->data['title']				= trans('general.view').' '.$this->title.' '.$this->data['product_attribute_data']->product_attribute_data_name;
		return $this->render_view('pages.product_attribute.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['product_attribute_data']  = $this->model->find($id);
		$this->data['product_attribute'] 	= Product_attribute::OrderBy('order','asc')->get();
		$this->data['title']				= trans('general.edit').' '.$this->title.' '.$this->data['product_attribute_data']->attribute->attribute_name.' '.$this->data['product_attribute_data']->value;
		return $this->render_view('pages.product_attribute.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(request $request,$id){
		$this->validate($request,[
			'attribute_id'		=> 'required',
			'name'				=> 'required|unique:product_attribute_data,id,'.$id
		]);
		if($request->value_color == '' && $request->value_select == ''){
			Alert::fail("Value Can't empty");
			return redirect()->back();
		}
		$this->model 					= $this->model->find($id);
		$this->model->attribute_id 		= $request->attribute_id;
		$this->model->name 				= $request->name;
		$attr 							= $this->model->attribute->attribute_type;
		if($attr == 'color'){
			$value 						= $request->value_color;
		}else{
			$value 						= $request->value_select;
		}
		$check 							= Product_attribute_data::where('attribute_id',$request->attribute_id)->where('value',$value)->first();
		if($value != $this->model->value){
			if($check){
				Alert::fail("Value already exist");
				return redirect()->back();
			}
		}
		$this->model->value 			= $value;
		$this->model->save();
		
		Alert::success('Successfully edit '.$this->title.' '.$this->model->name);
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(request $request){
		$product_attribute_data 	= $this->model->find($request->id);
		$product_attribute_data->delete();
		Alert::success('Product attribute data has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'name',
								'type' => 'text'
							]
						];
		$this->order_method = "multiple";
		$this->order_filter = [
								'label' => 'Attribute',
								'name' => 'attribute_id',
								'data' => $this->get_product_attribute(),
								];
	}
	public function sorting(){
		$this->model = $this->model->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_product_attribute(){
		$a 	= Product_attribute::OrderBy('order','asc')->pluck('attribute_name','id')->toArray();
		return $a;
	}
}
