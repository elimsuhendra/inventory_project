<?php namespace digipos\Http\Controllers\Admin;

use Illuminate\Http\Request;
// use Request;
use Illuminate\Validation\Rule;
use digipos\Libraries\Alert;
use File;

use digipos\models\Product_category;

class ProductsubcategoryController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.sub-category');
		$this->root_link 		= "product-sub-category";
		$this->primary_field 	= "category_name";
		$this->model 			= new Product_category;
		$this->check_relation 	= ['productcategory'];
		$this->bulk_action 		= true;
		$this->bulk_action_data = [3];
		$this->image_path 		= 'components/front/images/product_category/';
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'image',
				'label' => trans('general.image'),
				'sorting' => 'y',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],[
				'name' => 'category_name',
				'label' => trans('general.sub-category-name'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'category_id',
				'label' => trans('general.parent'),
				'search' => 'select',
				'search_data' => $this->get_category(),
				'sorting' => 'y',
				'belongto' => ['method' => 'category','field' => 'category_name']
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			],[
				'name' => 'sticky_category',
				'label' => 'sticky Category',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Sticky', 'n' => 'Not-sticky'],
				'type' => 'check'
			]
		];
		$this->model = $this->model->where('category_id','!=',0);
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	// public function field_create(){
	// 	$field = [
	// 				[
	// 					'name' => 'category_name',
	// 					'label' => 'Sub Category Name',
	// 					'type' => 'text',
	// 					'attribute' => 'required autofocus',
	// 					'validation' => 'required',
	// 					'not_same' => 'y',
	// 					'alias' => 'category_name_alias',
	// 					'tab'	=> 'general'
	// 				],[
	// 					'name' => 'category_id',
	// 					'label' => 'Parent Category',
	// 					'type' => 'select',
	// 					'data' => $this->get_category(),
	// 					'class'	=> 'select2',
	// 					'attribute' => 'required',
	// 					'validation' => 'required',
	// 					'tab'	=> 'general'
	// 				],[
	// 					'name' => 'meta_title',
	// 					'label' => 'Meta Title',
	// 					'type' => 'text',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'meta_description',
	// 					'label' => 'Meta Description',
	// 					'type' => 'textarea',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'meta_keywords',
	// 					'label' => 'Meta Keywords',
	// 					'type' => 'textarea',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'image',
	// 					'label' => 'Sub Category Image',
	// 					'type' => 'file',
	// 					'file_opt' => ['path' => $this->image_path],
	// 					'upload_type' => 'single-image',
	// 					'form_class' => 'col-md-6 pad-left',
	// 					'validation' => 'mimes:jpeg,png,jpg,gif',
	// 					'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 2000 x 600',
	// 					'tab'	=> 'general'
	// 				],[
	// 					'name' => 'status',
	// 					'label' => 'Status Category',
	// 					'type' => 'radio',
	// 					'data' => ['y' => 'Active','n' => 'Not Active'],
	// 					'attribute' => 'required',
	// 					'validation' => 'required',
	// 					'tab'	=> 'general'
	// 				]
	// 			];
	// 	return $field;
	// }

	// public function field_edit(){
	// 	$field = [
	// 				[
	// 					'name' => 'category_name',
	// 					'label' => 'Sub Category Name',
	// 					'type' => 'text',
	// 					'attribute' => 'required autofocus',
	// 					'validation' => 'required',
	// 					'not_same' => 'y',
	// 					'alias' => 'category_name_alias',
	// 					'tab'	=> 'general'
	// 				],[
	// 					'name' => 'category_id',
	// 					'label' => 'Parent Category',
	// 					'type' => 'select',
	// 					'data' => $this->get_category(),
	// 					'class'	=> 'select2',
	// 					'attribute' => 'required',
	// 					'validation' => 'required',
	// 					'tab'	=> 'general'
	// 				],[
	// 					'name' => 'meta_title',
	// 					'label' => 'Meta Title',
	// 					'type' => 'text',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'meta_description',
	// 					'label' => 'Meta Description',
	// 					'type' => 'textarea',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'meta_keywords',
	// 					'label' => 'Meta Keywords',
	// 					'type' => 'textarea',
	// 					'tab'	=> 'seo'
	// 				],[
	// 					'name' => 'image',
	// 					'label' => 'Sub Category Image',
	// 					'type' => 'file',
	// 					'file_opt' => ['path' => $this->image_path],
	// 					'upload_type' => 'single-image',
	// 					'form_class' => 'col-md-6 pad-left',
	// 					'validation' => 'mimes:jpeg,png,jpg,gif',
	// 					'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 2000 x 600',
	// 					'tab'	=> 'general'
	// 				],[
	// 					'name' => 'status',
	// 					'label' => 'Status Category',
	// 					'type' => 'radio',
	// 					'data' => ['y' => 'Active','n' => 'Not Active'],
	// 					'attribute' => 'required',
	// 					'validation' => 'required',
	// 					'tab'	=> 'general'
	// 				]
	// 			];
	// 	return $field;
	// }

	public function create(){
		// $this->field = $this->field_create();
		// return $this->build('create');
		$this->data['get_category'] = $this->get_category();
		$this->data['title']	= trans('general.create').' '.$this->title;
		return $this->render_view('pages.product_subcategory.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(request $request){
		// $this->field = $this->field_create();
		// return $this->build('store');

		$this->validate($request,[
			'category_name'	=> 'required|unique:product_category',
		]);

		$check_status = 1;

		$check = Product_category::where('sticky_category','y')->count();

		if($request->sticky_category == 'y'){
			if($check < 4){
				$this->model->sticky_category 	= 'y';
			} else{
				$check_status = 0;
			}	
		} else{
			$this->model->sticky_category 	= 'n';
		}

		if($check_status == 0){
			Alert::fail('Limit Sticky Category 4');
			return back();
		} else{
			$sub_category = NULL;
			$check_sub = Product_category::select('category_id')->where('id', $request->category_id)->first();

			if(count($check_sub) > 0){
				if($check_sub->category_id != 0){
					$sub_category = "false";
				}
			}

			$this->model->category_name 		= $request->category_name;
			$this->model->category_name_alias 	= $request->category_name_alias;
			$this->model->category_id 			= $request->category_id;
			$this->model->sub_category 			= $sub_category;
			$this->model->status 				= $request->status;
			$this->model->meta_title 			= $request->meta_title;
			$this->model->meta_description 		= $request->meta_description;
			$this->model->meta_keywords 		= $request->meta_keywords;

			$file 	= [
						'name' 		=> 'image',
						'file_opt' 	=> ['path' => 'components/front/images/product_category/'],
					];

			if($request->hasFile('image')){
				$image = $this->build_image($file);
				$this->model->image = $image;
				$this->model->save();
			}else{
				if($request->has('remove-single-image-image') == 'y'){
					File::delete($file['file_opt']['path'].$this->model->image);
					$this->model->image = '';
					$this->model->save();
				}
			}

			Alert::success('Successfully add Sub Category');
			return redirect()->to($this->data['path']);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		// $this->model = $this->model->where('id',$id);
		// $this->field = $this->field_edit();
		// return $this->build('view');

		$this->data['title']	= trans('general.edit').' '.$this->title;
		$this->data['get_category'] = $this->get_category();
		$this->data['sub_category'] = $this->model->where('id',$id)->first();
		return $this->render_view('pages.product_subcategory.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		// $this->model = $this->model->where('id',$id);
		// $this->field = $this->field_edit();
		// return $this->build('edit');

		$this->data['title']	= trans('general.edit').' '.$this->title;
		$this->data['get_category'] = $this->get_category();
		$this->data['sub_category'] = $this->model->where('id',$id)->first();
		return $this->render_view('pages.product_subcategory.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(request $request,$id){
		$this->validate($request,[
			'category_name'	=> [
				'required',
				Rule::unique('product_category')->ignore($id),
			],
		]);

		$check_status = 1;

		$check = Product_category::where([['sticky_category','y'],['id', $id]])->count();
		$this->model 						= $this->model->find($id);

		if($request->sticky_category == 'y'){
			if($check < 4){
				$this->model->sticky_category 	= 'y';
			} else{
				$check_status = 0;
			}	
		} else{
			$this->model->sticky_category 	= 'n';
		}

		if($check_status == 0){
			Alert::fail('Limit Sticky Category 4');
			return back();
		} else{
			$sub_category = NULL;
			$check_sub = Product_category::select('category_id')->where('id', $request->category_id)->first();

			if(count($check_sub) > 0){
				if($check_sub->category_id != 0){
					$sub_category = "false";
				}
			}

			$this->model->category_name 		= $request->category_name;
			$this->model->category_name_alias 	= $request->category_name_alias;
			$this->model->category_id 			= $request->category_id;
			$this->model->sub_category 			= $sub_category;
			$this->model->status 				= $request->status;
			$this->model->meta_title 			= $request->meta_title;
			$this->model->meta_description 		= $request->meta_description;
			$this->model->meta_keywords 		= $request->meta_keywords;

			$file 	= [
						'name' 		=> 'image',
						'file_opt' 	=> ['path' => 'components/front/images/product_category/'],
					];

			if($request->hasFile('image')){
				$image = $this->build_image($file);
				$this->model->image = $image;
			}else{
				if($request->input('remove-single-image-image') == 'y'){
					File::delete($file['file_opt']['path'].$this->model->image);
					$this->model->image = '';
				}
			}

			$this->model->save();

			Alert::success('Successfully Update new Sub Category');
			return redirect()->to($this->data['path']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		// $this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'image',
								'type' => 'image',
								'file_opt' => ['path' => $this->image_path]
							],[
								'name' => 'category_name',
								'type' => 'text'
							]
						];
		$this->order_method = "multiple";
		$this->order_filter = [
								'label' => 'Category',
								'name' => 'category_id',
								'data' => $this->get_category(),
								];
	}
	public function sorting(){
		$this->model = $this->model->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		$this->sorting_config();
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		$a = $this->buildbulkedit();
		return $a;
	}

	public function get_category(){
		// $ids = Request::segment(4);
		$query = Product_category::where('sub_category', NULL)->where('status','y')->orderBy('order','asc')->pluck('category_name','id')->toArray();
		return $query;
	}
}
