<?php namespace digipos\Http\Controllers\Admin;

use digipos\Libraries\Alert;
use digipos\Libraries\Currency;
use digipos\Libraries\Report;

use digipos\models\Mscurrency;

use Session;
use Request;
use DB;
use App;
use Validator;
use Image;
use File;
use Auth;
use Hash;
use Schema;

class KyubiController extends Controller {
	public function __construct(){
		parent::__construct();
		$this->data['head_button'] = '';
	}

	public $field;
	public $datareport;
	public $head_button 	= '';
	public $scripts			= '';
	public $title;
	public $root_link;
	public $model;
	public $primary_field 	= 'id';
	public $query_method 	= 'model';
	public $tab_data 		= [];
	public $restrict_id 	= [];
	public $restrict_delete = [];
	public $bulk_action 	= false;
	public $bulk_action_data = [];
	
	//sorting
	public $order_method 	= 'single';
	public $order_field 	= 'order';
	public $order_field_by 	= 'asc';

	public function hidden($data){
		$gen = view($this->view_path.'.builder.hidden',$data)->render();
		return $gen;
	}

	public function generate_token(){
		$token = csrf_token();
		return $token;
	}

	public function build_input($f){
		if (in_array($f['type'],['text','email','password','number'])){
			$content = view($this->view_path.'.builder.text',$f);
		}else if($f['type'] == 'hidden'){
			$content = view($this->view_path.'.builder.hidden',$f);
		}else if($f['type'] == 'select'){
			$content = view($this->view_path.'.builder.select',$f);
		}else if($f['type'] == 'radio'){
			$content = view($this->view_path.'.builder.radio',$f);
		}else if($f['type'] == 'textarea'){
			$content = view($this->view_path.'.builder.textarea',$f);
		}else if($f['type'] == 'file'){
			$content = view($this->view_path.'.builder.file',$f);
		}else if($f['type'] == 'checkbox'){
			if(isset($f['hasmany'])){
				$f['value'] = $this->build_array($this->model->{$f['hasmany']['method']},$f['hasmany']['primary'],$f['hasmany']['field']);
			}
			$content = view($this->view_path.'.builder.checkbox',$f);
		}
		return $content;
	}

	public function build_array($method,$primary,$field){
		$data 	= [];
		if($primary == ''){
			foreach($method as $u){
				$data[] = $u->$field;
			}
		}else{
			foreach($method as $u){
				$data[$u->$primary] = $u->$field;
			}
		}
		return $data;
	}

	public function build_validation(){
		$validate = [];
		foreach($this->field as $f){
			if (isset($f['validation'])){
				var_dump($validate);
				var_dump($f['name']);
				var_dump($f['validation']);
				$validate = array_add($validate,$f['name'],$f['validation']);
			}
		}
		$v = Validator::make(Request::all(),$validate);
		return $v;
	}

	public function build_image($f){
		if (!file_exists($f['file_opt']['path'])) {
		    mkdir($f['file_opt']['path'], 0777, true);
		}
		$extension 	= Request::file($f['name'])->getClientOriginalExtension();
		if(in_array($extension,['pdf','word','zip','xlsx','xls','rar','ppt'])){
			$filename 	= $f['name'].'.'.$extension;
			Request::file($f['name'])->move($f['file_opt']['path'], $filename);
		}else{
			if(isset($f['crop']) && $f['crop'] == 'y'){
				$img = Request::input($f['name'].'_crop');
				$img = str_replace('data:image/png;base64,', '', $img);
				$img = str_replace(' ', '+', $img);
				$thumbnail = base64_decode($img);
				$filename 	= str_random(7).'.png';
				file_put_contents($f['file_opt']['path'].$filename, $thumbnail);
			}else{
				$filename = str_random(6) . '_' . str_replace(' ','_',Request::file($f['name'])->getClientOriginalName());   
				$image = Image::make(Request::file($f['name'])->getRealPath());
				if (isset($f['file_opt']['width']) && isset($f['file_opt']['height'])){
			        $image = $image->resize($f['file_opt']['width'],$f['file_opt']['height']);
				}
				$image = $image->save($f['file_opt']['path'].$filename);				
			}
			if(isset($f['old_image'])){
				File::delete($f['file_opt']['path'].$f['old_image']);
			}
		}
		return $filename;
	}

	public function buildupdateflag(){
		$field 	= Request::input('field');
		$id 	= Request::input('id');
		$value 	= Request::input('value');
		if(App('role')->edit == 'y'){
			if(Request::input('field') == "sticky_category"){
				$check = DB::table('product_category')->where('sticky_category','y')->count();

				if ($value == 'y'){
					if($check < 4){
						$this->model->where('id',$id)->update([$field => $value]);
						return "<td><a href='".url($this->data['path'])."/ext/updateflag?field=".$field."&id=".$id."&value=n' class='ajax-update'><i class='fa fa-check text-success'></i></a></td>";
					} else{
						return 'limit';
					}
					
				}else{
					$this->model->where('id',$id)->update([$field => $value]);

					return "<td><a href='".url($this->data['path'])."/ext/updateflag?field=".$field."&id=".$id."&value=y' class='ajax-update'><i class='fa fa-close text-danger'></i></a></td>";
				}
				
			}else{
				$this->model->where('id',$id)->update([$field => $value]);

				if ($value == 'y'){
					return "<td><a href='".url($this->data['path'])."/ext/updateflag?field=".$field."&id=".$id."&value=n' class='ajax-update'><i class='fa fa-check text-success'></i></a></td>";
				}else{
					return "<td><a href='".url($this->data['path'])."/ext/updateflag?field=".$field."&id=".$id."&value=y' class='ajax-update'><i class='fa fa-close text-danger'></i></a></td>";
				}
			}
		}else{
			return 'no_access';
		}
	}

	public function buildbulkedit(){
		$action = Request::input('action');
		$data 	= Request::input('data');
		$role 	= App('role');
		if($role->edit == 'y'){
			if($action == 'delete' && $role->delete == 'y'){
				$cnt 		= 0;
				$cnt_del 	= 0;
				foreach($data as $d){
					$query = $this->model->where('id',$d)->first();

					$ad 	= true;
					//Check Relation for delete
					if(isset($this->check_relation)){
						foreach($this->check_relation as $d){
							$q = count($query->$d);
							if ($q > 0){
								$ad = false;
							}
						}
					}
					if($ad == true){
						//Delete Relation with raw
						if(isset($this->delete_relation)){
							foreach($this->delete_relation as $d){
								$query->$d()->delete();
							}
						}
						$cnt++;
						$query->delete();
					}else{
						$cnt_del++;
					}
				}
				if($cnt_del > 0){
					Alert::fail($cnt_del." data deleted, ".$cnt." data can't deleted");
				}else{ 
					Alert::success("Success delete all selected data");
				}
			}else if($action == 'edit'){
				$name 	= Request::input('name');
				$value 	= Request::input('value');
				$this->model->whereIn('id',$data)->update([$name => $value]);
				Alert::success(count($data)." data updated");
			}
		}else{
			Alert::fail("You don't have access");
		}
		return response()->json(['status' => 'continue']);
	}

	public function build_export(){
		$type 		= Request::input('type');
		$schema 	= Schema::getColumnListing($this->model->getTable());
		if($type == 'excel'){
			$data 	= ['header'	=> $schema,'data' => $this->model->get()];
			Report::setdata($data);
			Report::settitle($this->title);
			Report::setview('admin.builder.excel');
			Report::settype($type);
			Report::setformat('xlsx');
			Report::setcreator(auth()->guard($this->guard)->user()->email);
			Report::generate();
		}
	}

	public function build_alias($data){
		$data = preg_replace('/[^a-zA-Z0-9]/', '-', $data);
		return $data;
	}

	public function build_index(){
		//Set Paging Data
		$pag_data = [];
		$query = $this->model->whereNotIn('id',$this->restrict_id);
		foreach(Request::all() as $r => $q){
			$pag_data = array_add($pag_data,$r,$q);
		}

		$orderby = Request::input('orderby') ? Request::input('orderby') : 'id';
		if ($this->data['role']->create == 'y'){
			$this->head_button .= view($this->view_path.'.builder.link',['url' => $this->data['path'].'/create','label' => '<i class="fa fa-plus"></i> '.trans('general.add')]);
        }
        if ($this->data['role']->sorting == 'y'){
			$this->head_button .= view($this->view_path.'.builder.link',['url' => $this->data['path'].'/ext/sorting','class' => 'purple-medium','label' => '<i class="fa fa-align-justify"></i> '.trans('general.sorting')]);
        }
        if ($this->data['role']->export == 'y'){
        	$this->head_button .= '<div class="btn-group">
					                <a class="btn green-jungle" href="javascript:;" data-toggle="dropdown">
					                    <i class="fa fa-newspaper-o"></i> '.trans('general.export').'
					                    <i class="fa fa-angle-down"></i>
					                </a>
					                <ul class="dropdown-menu bulk-actions">
					                    <li>
					                        <a href="'.url($this->data['path']).'/ext/export?type=excel">
					                        <i class="fa fa-file-excel-o"></i> '.trans('general.excel').' </a>
					                    </li>
					                </ul>
					               </div>&nbsp;';
					               // <li>
					               //          <a href="'.url($this->data['path']).'/export/pdf">
					               //          <i class="fa fa-file-pdf-o"></i> '.trans('general.pdf').' </a>
					               //      </li>
        }
        $this->head_button .= view($this->view_path.'.builder.button',['class' => 'yellow-gold search-head','label' => '<i class="fa fa-search"></i> '.trans('general.search')]);
		$content = '<div class="table-responsive"><table class="table table-bordered">';
		$content .= '<form method="get" id="builder_form" action="'.url($this->data['path']).'" data-ask="n"></form>';
		$content .= view($this->view_path.'.includes.errors');
		//SearchBox
		if(Request::has('q')){
			$content .= '<tr class="search-head-content">';
		}else{
			$content .= '<tr class="search-head-content" style="display:none;">';
		}
		$content .= '<th></th>';
		if($this->bulk_action == true && $this->data['role']->edit == 'y'){
	    	$content .= '<th></th>';
	    }
	    foreach($this->field as $f){
    		if (isset($f['search'])){
	        	if ($f['search'] == 'text'){
		          	$content .= '<th><div class="form-group form-md-line-input form-md-floating-label">';
		          	$content .= '<input type="text" id="form_floating_1" class="form-control search-data '.(isset($f['class']) ? $f['class'] : "").'" name="'.$f['name'].'" value="'.(Request::has($f['name']) ? Request::input($f['name']) : "").'" '.(isset($f['attribute']) ? $f['attribute'] : "").'>';
		          	$content .= '<label for="form_floating_1">'.$f['label'].'</label></div></th>';
	        	}else if ($f['search'] == 'select'){
	        		$content .= '<th><div class="form-group form-md-line-input">';
	        		$content .= '<select class="form-control search-data '.(isset($f['class']) ? $f['class'] : '').'" name="'.$f["name"].'"><option value="" '.(Request::input($f['name']) == '' ? 'selected' : '').'>--'.trans("general.select_one").'--</option>';
	        		if (isset($f['search_data'])){
	        			foreach($f['search_data'] as $fs => $fq){
	        				$content .= "<option value='".$fs."' ".(Request::input($f['name']) == $fs ? 'selected' : '').">".$fq."</option>";
	        			}
	        		}
	        		$content .= '</select></div></th>';
	        	}
	        	if(isset($f['query_type']) && Request::has($f['name'])){
	        		if($f['query_type']['range'] == 'from'){
	        			$query = $query->where($f['name'],'>=',Request::input($f['name']));
	        		}else{
	        			$query = $query->where($f['name'],'<=',Request::input($f['name']));
	        		}
	        	}else{
	        		if((int)Request::input($f['name'])){
		        		$query = $query->where($f['name'],'=',Request::input($f['name']));
		        	}else{
		        		if(!isset($this->cus_field)){
		        			$query = $query->where($f['name'],'like','%'.Request::input($f['name']).'%');
		        		}
		        	}
	        	}
	        }else{
	        	$content .= "<th></th>";
	        }
	    }

	    $content .= '<th colspan="3">'.view($this->view_path.'.builder.button',['type' => 'submit','label' => '<i class="fa fa-search"></i> '.trans('general.filter'),'class' => 'submit-search']).view($this->view_path.'.builder.link',['url' => $this->data['path'],'label' =>'<i class="fa fa-refresh"></i> Reset','class' => 'red-mint']).'</th></tr>';
	   
	    //Header for datatable
	    $content .= '<tr>';
	    if($this->bulk_action == true && $this->data['role']->edit == 'y'){
	    	$content .= '<th></th>';
	    }
	    $content .= '<th>No</th>';
	    foreach($this->field as $f){
	        if (isset($f['sorting']) && $f['sorting'] == 'y'){
	          	$content .= '<th><a href="#" class="sorting-data" data-orderby="'.$f['name'].'">
		            	'.$f['label'];
		        if(Request::has('orderby') && request::input('orderby') == $f['name']){
		        	if(request::input('orderdata') == 'asc'){
		        		$content .= '<i class="fa fa-arrow-down"></i>';
		        	}else{
		        		$content .= '<i class="fa fa-arrow-up"></i>';
		        	}
		        }else{
		        	$content .= '<i class="fa fa-arrows-v"></i>';
		        }
		        $content .= '</a></th>';
	        }else{
	          $content .= "<th>".$f['label']."</th>";
	        }
	    }
	    $content .= '<th colspan="3"></th></tr>';

	    //Data
	    $query = $query->orderBy($orderby,Request::input('orderdata'))->paginate(10);
	    foreach($query as $q){
	    	$content .= '<tr>';
	    	if($this->bulk_action == true && $this->data['role']->edit == 'y'){
	    		$content .= '<td><center>'.$this->build_input(['type' => 'checkbox','name' => 'bulkaction_id','data' => [$q->id => ''],'class' => 'bulk_checkbox']).'</center></td>';
	    	}
	    	$content .= "<td>".$this->data['no']++."</td>";
	    	foreach($this->field as $f){
	    		if(isset($f['before_show'])){
	    			if(array_key_exists('format_currency',$f['before_show'])){
	    				$currency = Mscurrency::find($q->fkcurrencyid);
	    				$q->{$f['name']} = Currency::convert($q->{$f['name']},$currency);
	    			}

	    			if(array_key_exists('number_format',$f['before_show'])){
	    				$q->{$f['name']} = number_format($q->{$f['name']},$f['before_show']['number_format']['decimal'],',','.');
	    			}
	    		}
	    		if(isset($f['belongtoraw'])){
	    			$q->$f['name'] = $q->{$f['belongtoraw']['field']};
	    		}
	    		if (isset($f['type']) && $f['type'] == 'check'){
		    		if ($q->{$f['name']} == 'y'){
		    			$content .= "<td><a href='".url($this->data['path'])."/ext/updateflag?field=".$f['name']."&id=".$q->id."&value=n' class='ajax-update'><i class='fa fa-check text-success'></i></a></td>";
		    		}else{
		    			$content .= "<td><a href='".url($this->data['path'])."/ext/updateflag?field=".$f['name']."&id=".$q->id."&value=y' class='ajax-update'><i class='fa fa-close text-danger'></i></a></td>";
		    		}
				}else if (isset($f['type']) && $f['type'] == 'image'){
					if(isset($f['belongto'])){
						$q->{$f['name']} = $q->{$f['belongto']['method']}()->first();
						if($q->{$f['name']}){
							$q->{$f['name']} = $q->{$f['name']}->{$f['belongto']['field']};
						}
					}
					if(isset($f['file_opt']['custom_path'])){
						$f['file_opt']['path'] 	= $f['file_opt']['path'].$q->{$f['file_opt']['custom_path']}.'/';
					}
					if($q->{$f['name']}){
						$q->{$f['name']} = asset($f['file_opt']['path'].$q->{$f['name']});
					}else{
						$q->{$f['name']} = asset('components/both/images/web/none.png');
					}
					$content .= "<td><img src='".$q->{$f['name']}."' width='144px'></td>";
				}else{
					if(isset($f['belongto'])){
						$q->{$f['name']} = $q->{$f['belongto']['method']}()->first()->{$f['belongto']['field']};
					}
					if(isset($f['value_key'])){
						$q->{$f['name']} = $f['value_key'][$q->{$f['name']}];
					}
					if(isset($f['after_show'])){
		    			if(array_key_exists('label',$f['after_show'])){
		    				$q->{$f['name']} = '<span class="label label-'.$f['after_show']['label'][$q->{$f['name']}].'">'.$q->{$f['name']}.'</span>';
		    			}
		    		}
					$content .= "<td>".$q->{$f['name']}."</td>";
				}
	    	}
	    	if ($this->data['role']->view == 'y'){
	    		$content .= "<td>".view($this->view_path.'.builder.link',['url' => $this->data['path'].'/'.$q->id,'label' => '<i class="fa fa-eye"></i> '.trans('general.view')])."</td>";
	    	}
	    	if ($this->data['role']->edit == 'y'){
	    		$content .= "<td>".view($this->view_path.'.builder.link',['url' => $this->data['path'].'/'.$q->id.'/edit','label' => '<i class="fa fa-edit"></i> '.trans('general.edit')])."</td>";
	    	}
	    	//Delete
	    	if ($this->data['role']->delete == 'y' && !in_array($q->id,$this->restrict_delete)){
	    		$content .= "<td><form method='post' action='".url($this->data['path'].'/destroy/')."'>";
		    	$content .= $this->hidden(['name' => 'id','value' => $q->id]);
		    	$content .= $this->hidden(['name' => '_method','value' => 'delete']);
		    	$content .= $this->hidden(['name' => '_token','value' => $this->generate_token()]);
		    	$content .= view($this->view_path.'.builder.button',['type' => 'submit','class' => 'red-mint','label' => '<i class="fa fa-trash"></i> '.trans('general.delete'),'ask' => 'y']);
	    	}
	    	$content .= "</form>";
	    	$content .= '</td></tr>';
	    }

	    $content .= '</table></div>';
	    $query->setPath($this->root_link);
	    $content .= '<center>'.$query->appends($pag_data)->render().'</center>';

	    //Dropdown Bulk
	    if($this->bulk_action == true && $this->data['role']->edit == 'y' && count($query) > 0){
	    	$content .= '
				    	<div class="btn-group">
			                <a class="btn purple" href="javascript:;" data-toggle="dropdown">
			                    <i class="fa fa-bars"></i> '.trans('general.bulk-actions').'
			                    <i class="fa fa-angle-up"></i>
			                </a>
			                <ul class="dropdown-menu bottom-up bulk-actions">
			                    <li>
			                        <a class="checkall" data-target="bulk_checkbox">
			                        <i class="fa fa-check-square-o true"></i> '.trans('general.select-all').' </a>
			                    </li>
			                    <li>
			                        <a class="uncheckall" data-target="bulk_checkbox">
			                        <i class="fa fa-square-o false"></i> '.trans('general.deselect-all').' </a>
			                    </li>';

			foreach($this->bulk_action_data as $bad){
				$content .= '<li class="divider"> </li>
				                    <li>
				                        <a class="ajax-bulk-update" data-href="'.url($this->data['path'].'/ext/bulkupdate/').'" class="ajax-bulk-update" data-action="edit" data-alert="'.trans('general.are-you-sure').'" data-name="'.$this->field[$bad]['name'].'" data-value="y">
				                        	<i class="fa fa-power-off true"></i> '.trans('general.enable-all-selected').' '.trans('general.'.strtolower($this->field[$bad]['label'])).' 
				                      	</a>
				                    </li>
				                    <li>
				                        <a class="ajax-bulk-update" data-href="'.url($this->data['path'].'/ext/bulkupdate/').'" class="ajax-bulk-update" data-action="edit" data-alert="'.trans('general.are-you-sure').'" data-name="'.$this->field[$bad]['name'].'" data-value="n">
				                        	<i class="fa fa-power-off false"></i> '.trans('general.disable-all-selected').' '.trans('general.'.strtolower($this->field[$bad]['label'])).'
				                        </a>
				                    </li>
				            ';
			}

			if($this->data['role']->delete == 'y'){
				$content 	.=  '<li class="divider"> </li>
					                    <li>
					                        <a data-href="'.url($this->data['path'].'/ext/bulkupdate/').'" class="ajax-bulk-update" data-action="delete" data-alert="'.trans('general.are-you-sure').'">
					                        <i class="fa fa-trash false"></i> '.trans('general.delete-selected').' </a>
					                    </li>
					                </ul>
					            </div>';
	    	}
	    }
	     // <li>
			   //                      <a href="javascript:;">
			   //                      <i class="fa fa-edit true"></i> '.trans('general.edit-manual-selected').' </a>
			   //                  </li>

	    //Build view
		$this->data['title'] = 'List '.$this->title;
	    $this->data['head_button'] = $this->head_button;
	    $this->data['content'] = $content;
	}

	public function build_create(){
		$this->head_button .= view($this->view_path.'.builder.link',['url' => $this->data['path'],'label' => '<i class="fa fa-arrow-left"></i> '.trans('general.back'),'class' => 'red-mint']);
		$content = '<form role="form" method="post" action="'.url($this->data['path']).'" enctype="multipart/form-data">';
		$content .= view($this->view_path.'.includes.errors');
		if(count($this->tab_data) == 0){
			foreach($this->field as $f){
			if (!isset($f['value'])){
				$f= array_add($f,'value',old($f['name']) ? old($f['name']) : '');
			}
			$content .= $this->build_input($f);
		}
		}else{
			$tab_data = $this->tab_data;
			$content .= '<div class="tabbable-line"><ul class="nav nav-tabs">';
			$tab_loop = 0;
			foreach($tab_data as $td => $tdq){
				if($tab_loop++ == 0){
					$content .= '<li class="active">';
				}else{
					$content .= '<li>';
				}
				$content .= '<a href="#'.$td.'" data-toggle="tab" aria-expanded="true">'.$tdq.'</a>';
				$content .= '</li>';

				$content_tab[$td] = '';
			}
			$content_tab['all'] 	= '';
			$content .= '</ul><div class="tab-content">';

			foreach($this->field as $f){
				if (!isset($f['value'])){
					$f= array_add($f,'value',old($f['name']) ? old($f['name']) : '');
				}
				if(isset($f['tab'])){
					$content_tab[$f['tab']] .= $this->build_input($f);
				}else{
					$content_tab['all'] 	.= $this->build_input($f);
				}
			}

			$tab_loop = 0;
			foreach($tab_data as $td => $tdq){
				if($tab_loop++ == 0){
					$content .= '<div class="tab-pane active" id="'.$td.'">';
				}else{
					$content .= '<div class="tab-pane" id="'.$td.'">';
				}
				$content .= $content_tab[$td];
				$content .= '</div>';
			}
			$content .= '</div></div>';
			$content .= $content_tab['all'];
		}
		$content .= '<div class="clearfix"></div>';
		$content .= '<div class="box-footer">'.view($this->view_path.'.builder.button',['type' => 'submit','label' => '<i class="fa fa-floppy-o"></i> '.trans('general.submit'),'ask' => 'y']).'</div></form>';
		$this->data['title'] = 'Add New '.$this->title;

		$this->data['head_button'] = $this->head_button;
		$this->data['content'] = $content;
	}

	public function build_store(){
		$validation = $this->build_validation();
		if (!$validation->fails()){
			foreach($this->field as $f){
				if (isset($f['not_same']) && $f['not_same'] == 'y'){
					$a = $this->model->where($f['name'],Request::input($f['name']))->first();
					if($a){
						Alert::fail($f['label'].' already exist');
						return "exit";
					}
				}

				if ($f['type'] == 'file'){
					if(Request::hasFile($f['name'])){
						$image = $this->build_image($f);
						$this->model->{$f['name']} = $image;
					}else{
						if(Request::input('remove-single-image-'.$f['name']) == 'y'){
							File::delete($f['file_opt']['path'].$this->model->{$f['name']});
							$this->model->{$f['name']} = '';
						}
					}
				}else if($f['type'] == 'checkbox'){
					if(count(Request::input($f['name'])) > 0){
						foreach(Request::input($f['name']) as $d){
							$temp[$f['name']][]	= new $f['hasmany']['table']([
								$f['name'] 	=> $d
							]);
						}
					}
				}else{
					$this->model->{$f['name']} = Request::input($f['name']);

					//If Hash for encrypt or password
					if (isset($f['hash']) && $f['hash'] == 'y'){
						$this->model->{$f['name']} = Hash::make(Request::input($f['name']));
					}

					//For Make Alias
					if(isset($f['alias'])){
						$this->model->{$f['alias']} = $this->build_alias($this->model->{$f['name']});
					}
				}
			}

			$this->model->upd_by = auth()->guard($this->guard)->user()->id;
			$this->model->save();

			//Checkbox in another table
			foreach($this->field as $f){
				if($f['type'] == 'checkbox'){
					if(count(Request::input($f['name'])) > 0){
						$this->model->{$f['hasmany']['method']}()->saveMany($temp[$f['name']]);
					}
				}
			}
			Alert::success('Successfully add new '.$this->title);
			return "next";
		}
	}

	public function build_edit(){
		$this->model = $this->model->first();
		if(in_array($this->model->id,$this->restrict_id)){
			Alert::fail("You don't have access");
			return "exit";
		}
		$this->head_button .= view($this->view_path.'.builder.link',['url' => $this->data['path'],'label' => '<i class="fa fa-arrow-left"></i> '.trans('general.back'),'class' => 'red-mint']);
		$content = '<form role="form" method="post" action="'.url($this->data['path']).'/'.$this->model->id.'" enctype="multipart/form-data">';
		$content .= $this->hidden(['name' => '_method','value' => 'put']);
		$content .= '<div class="form-body">';
		$content .= view($this->view_path.'.includes.errors');
		if(count($this->tab_data) == 0){
			foreach($this->field as $f){
				if (!isset($f['value'])){
					$f= array_add($f,'value',old($f['name']) ? old($f['name']) : $this->model->{$f['name']});
				}
				if(isset($f['format'])){
					if($f['format']['datetime']){
						$f['value'] = date_format(date_create($f['value']),$f['format']['datetime']);
					}
				}
				$content .= $this->build_input($f);
			}
		}else{
			$tab_data = $this->tab_data;
			$content .= '<div class="tabbable-line"><ul class="nav nav-tabs">';
			$tab_loop = 0;
			foreach($tab_data as $td => $tdq){
				if($tab_loop++ == 0){
					$content .= '<li class="active">';
				}else{
					$content .= '<li>';
				}
				$content .= '<a href="#'.$td.'" data-toggle="tab" aria-expanded="true">'.$tdq.'</a>';
				$content .= '</li>';

				$content_tab[$td] = '';
			}
			$content_tab['all'] 	= '';
			$content .= '</ul><div class="tab-content">';

			foreach($this->field as $f){
				if (!isset($f['value'])){
					$f= array_add($f,'value',old($f['name']) ? old($f['name']) : $this->model->{$f['name']});
				}
				if(isset($f['format'])){
					if($f['format']['datetime']){
						$f['value'] = date_format(date_create($f['value']),$f['format']['datetime']);
					}
				}
				if(isset($f['tab'])){
					$content_tab[$f['tab']] .= $this->build_input($f);
				}else{
					$content_tab['all'] 	.= $this->build_input($f);
				}
			}

			$tab_loop = 0;
			foreach($tab_data as $td => $tdq){
				if($tab_loop++ == 0){
					$content .= '<div class="tab-pane active" id="'.$td.'">';
				}else{
					$content .= '<div class="tab-pane" id="'.$td.'">';
				}
				$content .= $content_tab[$td];
				$content .= '</div>';
			}
			$content .= '</div></div>';
			$content .= $content_tab['all'];
		}
		$content .= '<div class="clearfix"></div>';
		$content .= '<div class="box-footer">'.view($this->view_path.'.builder.button',['type' => 'submit','label' => '<i class="fa fa-floppy-o"></i> '.trans('general.submit'),'ask' => 'y']).'</div></div></form>';
		
		$pk_field = $this->primary_field;
		$this->data['title'] = 'Edit '.$this->title.' '.$this->model->$pk_field;

		$this->data['head_button'] = $this->head_button;
		$this->data['content'] = $content;
		return "next";
	}

	public function build_update(){
		$validation = $this->build_validation();
		$this->model = $this->model->first();
		if (!$validation->fails()){
			foreach($this->field as $f){
				if(isset($f['not_save']) && $f['not_save'] == 'y'){
					continue;
				}
				if (isset($f['not_same']) && $f['not_same'] == 'y'){
					if ($this->model->{$f['name']} != Request::input($f['name'])){
						$a = $this->model->where($f['name'],Request::input($f['name']))->first();
						if($a){
							Alert::fail($f['label'].' already exist');
							return "exit";
						}
					}
				}

				if ($f['type'] == 'file'){
					if (Request::hasFile($f['name'])){
						File::delete($f['file_opt']['path'].$this->model->{$f['name']});
						$image = $this->build_image($f);
						$this->model->{$f['name']} = $image;
					}else{
						if(Request::input('remove-single-image-'.$f['name']) == 'y'){
							File::delete($f['file_opt']['path'].$this->model->{$f['name']});
							$this->model->{$f['name']} = '';
						}
					}
				}else if($f['type'] == 'checkbox'){
					if(count(Request::input($f['name'])) > 0){
						foreach(Request::input($f['name']) as $d){
							$temp[]	= new $f['hasmany']['table']([
								$f['name'] 	=> $d
							]);
						}
					}
					$this->model->{$f['hasmany']['method']}()->delete();
					if(isset($temp)){
						$this->model->{$f['hasmany']['method']}()->saveMany($temp);
					}
				}else{
					if($this->model->{$f['name']} != Request::input($f['name'])){
						$this->model->{$f['name']} = Request::input($f['name']);

						//If Hash for encrypt or password
						if (isset($f['hash']) && $f['hash'] == 'y'){
							$this->model->{$f['name']} = Hash::make(Request::input($f['name']));
						}

						//For Make Alias
						if(isset($f['alias'])){
							$this->model->{$f['alias']} = $this->build_alias($this->model->{$f['name']});
						}
					}
				}
			}

			$this->model->upd_by = auth()->guard($this->guard)->user()->id;
			$this->model->save();
			$primary = $this->primary_field;
			Alert::success('Successfully edit '.$this->title.' '.$this->model->$primary);
			return "next";
		}
	}

	public function build_view(){
		$this->model = $this->model->first();
		if(in_array($this->model->id,$this->restrict_id)){
			Alert::fail("You don't have access");
			return "exit";
		}
		$this->head_button .= view($this->view_path.'.builder.link',['url' => $this->data['path'],'label' => '<i class="fa fa-arrow-left"></i> '.trans('general.back'),'class' => 'red-mint']);
		$content	= '';
		if(count($this->tab_data) == 0){
			foreach($this->field as $f){
				if (!isset($f['value'])){
					$f= array_add($f,'value',old($f['name']) ? old($f['name']) : $this->model->{$f['name']});
				}
				if(isset($f['format'])){
					if($f['format']['datetime']){
						$f['value'] = date_format(date_create($f['value']),$f['format']['datetime']);
					}
				}
				$content .= $this->build_input($f);
			}
		}else{
			$tab_data = $this->tab_data;
			$content .= '<div class="tabbable-line"><ul class="nav nav-tabs">';
			$tab_loop = 0;
			foreach($tab_data as $td => $tdq){
				if($tab_loop++ == 0){
					$content .= '<li class="active">';
				}else{
					$content .= '<li>';
				}
				$content .= '<a href="#'.$td.'" data-toggle="tab" aria-expanded="true">'.$tdq.'</a>';
				$content .= '</li>';

				$content_tab[$td] = '';
			}
			$content_tab['all'] 	= '';
			$content .= '</ul><div class="tab-content">';

			foreach($this->field as $f){
				if (!isset($f['value'])){
					$f= array_add($f,'value',old($f['name']) ? old($f['name']) : $this->model->{$f['name']});
				}
				if(isset($f['format'])){
					if($f['format']['datetime']){
						$f['value'] = date_format(date_create($f['value']),$f['format']['datetime']);
					}
				}
				if(isset($f['tab'])){
					$content_tab[$f['tab']] .= $this->build_input($f);
				}else{
					$content_tab['all'] 	.= $this->build_input($f);
				}
			}

			$tab_loop = 0;
			foreach($tab_data as $td => $tdq){
				if($tab_loop++ == 0){
					$content .= '<div class="tab-pane active" id="'.$td.'">';
				}else{
					$content .= '<div class="tab-pane" id="'.$td.'">';
				}
				$content .= $content_tab[$td];
				$content .= '</div>';
			}
			$content .= '</div></div>';
			$content .= $content_tab['all'];
		}
		$content .= '<div class="clearfix"></div>';
		$pk_field = $this->primary_field;
		$this->data['title'] = 'Edit '.$this->title.' '.$this->model->$pk_field;

		$this->data['head_button'] 	= $this->head_button;
		$this->data['content'] 		= $content;
		$this->scripts				.= "$('input,select,textarea,.single-image,.remove-single-image').prop('disabled',true);tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });";
		$this->data['scripts'] 		= $this->scripts;
		return 'next';
	}

	public function build_delete(){
		$secure = DB::transaction(function(){
			$query = $this->model->where('id',Request::input('id'))->first();
			//Check Relation for delete
			if(isset($this->check_relation)){
				$temp = '';
				foreach($this->check_relation as $d){
					$q = count($query->$d);
					if ($q > 0){
						$temp .= ucwords(str_replace('_',' ',$d)).', '; 
					}
				}
				if($temp != ''){
					Alert::fail('You must delete related data first in: '.trim($temp,", "));
					return;
				}
			}
			//Delete Relation with raw
			if(isset($this->delete_relation)){
				foreach($this->delete_relation as $d){
					$query->$d()->delete();
				}
			}
			//Delete File/image
			if (isset($this->field)){
				foreach($this->field as $f){
					if ($f['type'] == 'file'){
						File::delete($f['file_opt']['path'].$query->{$f['name']});
					}
				}
			}
			$query->delete();
			Alert::success('Successfully delete '.$this->title);
			return 'success';
		});
	}

	public function build_single_sorting(){
		$this->head_button .= view($this->view_path.'.builder.link',['url' => $this->data['path'],'label' => '<i class="fa fa-arrow-left"></i> '.trans('general.back'),'class' => 'red-mint']);
		$query = $this->model->get();
		$content  = view($this->view_path.'.includes.errors');
		$content .= "<form method='post' action='".url($this->data['path'])."/ext/dosorting'><ul class='sortable'>";
		$content .= csrf_field();
		foreach($query as $q){
			$content .= '<li class="ui-state-default" '.(count($query) < 5 ? "style=width:100%" : "").'> <input type="hidden" name="'.$this->order_field.'[]" value="'.$q->id.'">';
			foreach($this->field as $f){
				if($f['type'] == 'image'){
					if($q->{$f['name']}){
						$q->{$f['name']} = asset($f['file_opt']['path'].$q->{$f['name']});
					}else{
						$q->{$f['name']} = asset('components/both/images/web/none.png');
					}
					$content .= "<img src='".$q->{$f['name']}."' width='80px' height='50px'><div class='clearfix'></div>";
				}else{
					$content .= $q->{$f['name']};
				}
			}
			$content .= '</li>';
		}
		$content .= "</ul><div class='clearfix'></div><br/>";
		$content .= view($this->view_path.'.builder.button',['type' => 'submit','label' => '<i class="fa fa-floppy-o"></i> Submit','ask' => 'y','class' => 'btn-block']);
		$content .= "</form>";
		$this->data['title'] = trans('general.sorting').' '.$this->title;
		$this->data['head_button'] = $this->head_button;
		$this->data['content'] = $content;
	}

	public function build_multiple_sorting(){
		$this->head_button .= view($this->view_path.'.builder.link',['url' => $this->data['path'],'label' => '<i class="fa fa-arrow-left"></i> '.trans('general.back'),'class' => 'red-mint']);
		$content = view($this->view_path.'.includes.errors');;
		$content .= '<form method="get" action="'.url(Request::url()).'">';
		$value = Request::has($this->order_filter['name']) ? Request::input($this->order_filter['name']) : '0';
		$content .= view($this->view_path.'.builder.select',['label' => $this->order_filter['label'],'data' => $this->order_filter['data'],'value' => $value,'class' => 'submit_form','name' => $this->order_filter['name']]);
		$content .= '</form>';	
		if(Request::has($this->order_filter['name'])){
			if($this->query_method == 'raw'){
				$query = $this->model->get();
			}else{
				$query = $this->model->where($this->order_filter['name'],Request::input($this->order_filter['name']))->get();
			}
			$content .= "<form method='post' action='".url($this->data['path'])."/ext/dosorting?".$this->order_filter['name']."=".Request::input($this->order_filter['name'])."'><ul class='sortable'>";
			$content .= $this->hidden(['name' => '_token','value' => $this->generate_token()]);
			foreach($query as $q){
				$content .= '<li class="ui-state-default" '.(count($query) < 5 ? "style=width:100%" : "").'> <input type="hidden" name="'.$this->order_field.'[]" value="'.$q->id.'">';
				foreach($this->field as $f){
					if($f['type'] == 'image'){
						if($q->{$f['name']}){
							if(isset($f['file_opt']['custom_path'])){
								$q->{$f['name']} = asset($f['file_opt']['path'].$q->{$f['file_opt']['custom_path']}.'/'.$q->{$f['name']});
							}else{
								$q->{$f['name']} = asset($f['file_opt']['path'].$q->{$f['name']});
							}
						}else{
							$q->{$f['name']} = asset('components/both/images/web/none.png');
						}
						$content .= "<img src='".$q->{$f['name']}."' width='60px' height=''><div class='clearfix'></div>";
					}else{
						$content .= $q->{$f['name']};
					}
				}
				$content .= '</li>';
			}
			$content .= "</ul><div class='clearfix'></div><br/>";
			if($query->count() > 0){
				$content .= view($this->view_path.'.builder.button',['type' => 'submit','label' => '<i class="fa fa-floppy-o"></i> Submit','ask' => 'y']);
			}
			$content .= "</form>";
		}
		$this->data['title'] = 'Sorting '.$this->title;
		$this->data['head_button'] = $this->head_button;
		$this->data['content'] = $content;
	}

	public function build_do_sorting(){
		$data = Request::input($this->order_field);
		$query = $this->model;
		for($i = 0; $i<count($data);$i++){
			if($this->query_method == 'raw'){
				$query->where('id',$data[$i])->update(array($this->order_field => $i+1));
			}else{
				if ($this->order_method == 'multiple'){
					$query->where($this->order_filter['name'],Request::input($this->order_filter['name']));
				}
				$query->where('id',$data[$i])->update(array($this->order_field => $i+1));
			}
		}
		Alert::success('Successfully update '.$this->title.' order');
	}

	public function build_report(){
		$data = $this->datareport;
		Report::setdata($data['data']);
		Report::settitle($data['title']);
		Report::setview($data['view']);
		Report::settype($data['type']);
		Report::setformat($data['format']);
		Report::setcreator(auth()->guard($this->guard)->user()->email);
		Report::generate();
		return true;
	}

	public function build($url = ''){
		$this->data['role'] = App('role');
		switch($url){
			case 'index':
				$this->build_index();
				return $this->render_view('builder');
				break;
			case 'create':
				$this->build_create();
				return $this->render_view('builder');
				break;
			case 'store':
				$result = $this->build_store();
				if($result == 'next'){
					return $this->redirect_to($this->data['path']);
				}else{
					return redirect()->back()
						->withInput(Request::except('password'))
						->withErrors($this->build_validation());
				}
				break;
			case 'edit':
				$result 	= $this->build_edit();
				if($result == 'next'){
					return $this->render_view('builder');
				}else{
					return redirect()->back();
				}
				break;
			case 'update':
				$result = $this->build_update();
				if($result == 'next'){
					return redirect()->back();
				}else{
					return redirect()->back()
						->withInput()
						->withErrors($this->build_validation());
				}
				break;
			case 'view':
				$result 	= $this->build_view();
				if($result == 'next'){
					return $this->render_view('builder');
				}else{
					return redirect()->back();
				}
				break;
			case 'delete':
				$this->build_delete();
				return redirect()->back();
				break;
			case 'sorting':
				if ($this->order_method == 'single'){
					$this->build_single_sorting();
				}else{
					$this->build_multiple_sorting();
				}
				return $this->render_view('builder');
				break;
			case 'dosorting':
				$this->build_do_sorting();
				return redirect()->back();
				break;
			case 'report':
				$this->build_report();
				return redirect()->back();
				break;
			default:
				echo "Method Failed";
				break;
		}
	}

	/* Custom Controller */
	public function getcity(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/city",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: e7d6a9287b3e4f6ae339a20cbbb0de5a"
          ),
        ));

        $city = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $city;
        }
    }
	/* End Custom Controller */
}