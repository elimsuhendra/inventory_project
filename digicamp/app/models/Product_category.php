<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product_category extends Model{
	protected $table 				 = 'product_category';
	protected $category 			 = 'digipos\models\Product_category';
	protected $product_data_category = 'digipos\models\Product_data_category';

	public function category(){
		return $this->belongsTo($this->category,'category_id');
	}

	public function subcategory(){
		return $this->hasMany($this->category,'category_id');
	}

	public function product_data_category(){
		return $this->hasMany($this->product_data_category,'product_category_id');
	}
}
