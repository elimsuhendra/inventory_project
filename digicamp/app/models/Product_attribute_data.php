<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product_attribute_data extends Model{
	protected $table = 'product_attribute_data';
	protected $attribute 	= 'digipos\models\Product_attribute';

	public function attribute(){
		return $this->belongsTo($this->attribute,'attribute_id');
	}
}
