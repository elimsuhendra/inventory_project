<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Customer_cart extends Model{
	protected $table 	= 'customer_cart';
    protected $fillable = ['customer_id'];

}
