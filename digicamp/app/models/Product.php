<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
	protected $table 					= 'product';
	protected $product_category 		= 'digipos\models\Product_category';
	protected $product_data_category 	= 'digipos\models\Product_data_category';
	protected $product_data_images 		= 'digipos\models\Product_data_images';
	protected $product_data_attribute_master 	= 'digipos\models\Product_data_attribute_master';
	protected $product_data_discount 	= 'digipos\models\Discount';

	public function product_category(){
		return $this->belongsTo($this->product_category,'product_category_id');
	}

	public function primary_images(){
		return $this->hasOne($this->product_data_images,'product_id')->where('status_primary','y');
	}

	public function product_data_discount(){
		return $this->hasOne($this->product_data_discount,'product_id')->where('status','y');
	}

	public function product_data_category(){
		return $this->hasMany($this->product_data_category,'product_id');
	}

	public function product_data_images(){
		return $this->hasMany($this->product_data_images,'product_id')->orderBy('order','asc');
	}

	public function product_data_attribute_master(){
		return $this->hasMany($this->product_data_attribute_master,'product_id');
	}

	public function thumbnail(){
		return $this->hasOne($this->product_data_images,'product_id')->where('status_primary','y')->where('status','y');
	}
}
