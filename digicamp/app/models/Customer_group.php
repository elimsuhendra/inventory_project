<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Customer_group extends Model{
	protected $table 				= 'customer_group';
	protected $customer_group_data 	= 'digipos\models\Customer_group_data';
	protected $voucher_customer_group_data 	= 'digipos\models\Voucher_user_group_data';

	public function customer_group_data(){
		return $this->hasMany($this->customer_group_data,'customer_group_id');
	}

	public function voucher_customer_group_data(){
		return $this->hasMany($this->voucher_customer_group_data,'customer_group_id');
	}	
}
