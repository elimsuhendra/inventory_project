<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Customer_wishlist extends Model{
	protected $table 	= 'customer_wishlist';
    protected $fillable = ['customer_id'];
}
