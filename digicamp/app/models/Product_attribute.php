<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product_attribute extends Model{
	protected $table 			= 'product_attribute';
	protected $attribute_data 	= 'digipos\models\Product_attribute_data';

	public function attribute_data(){
		return $this->hasMany($this->attribute_data,'attribute_id');
	}
}
