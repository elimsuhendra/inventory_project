<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Orderlog extends Model{
	protected $table 			= 'orderlog';
	protected $orderstatus 		= 'digipos\models\Orderstatus';
	protected $user 			= 'digipos\models\User';
	
	public function orderstatus(){
		return $this->belongsTo($this->orderstatus,'order_status_id');
	}

	public function user(){
		return $this->belongsTo($this->user,'upd_by');
	}
}
