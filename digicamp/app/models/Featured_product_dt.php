<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Featured_product_dt extends Model{
	protected $table 	= 'featured_product_dt';
	protected $product 	= 'digipos\models\Product';

	public function product(){
		return $this->belongsTo($this->product,'product_id');
	}
}
