<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model{
	protected $table = 'contact';
}
