<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Customer_address extends Model{
	protected $table 	= 'customer_address';
	protected $province = 'digipos\models\Province';
	protected $city 	= 'digipos\models\City';
	protected $sub_district = 'digipos\models\Sub_district';

	public function province(){
		return $this->belongsTo($this->province,'province_id');
	}

	public function city(){
		return $this->belongsTo($this->city,'city_id');
	}

	public function sub_district(){
		return $this->belongsTo($this->sub_district,'sub_district_id');
	}
}
