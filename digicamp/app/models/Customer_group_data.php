<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Customer_group_data extends Model{
	protected $table 	= 'customer_group_data';
	protected $fillable = ['id','customer_group_id'];

}
