<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model{
	protected $table 		= 'post';
	protected $post_related = 'digipos\models\Post_related';
	protected $post_tag 	= 'digipos\models\Post_tag';
	protected $writer 		= 'digipos\models\Writer';
	protected $user 		= 'digipos\models\User';
	protected $category 	= 'digipos\models\Category';

	public function post_tag(){
		return $this->hasMany($this->post_tag,'post_id');
	}

	public function writer(){
		return $this->belongsTo($this->writer,'writer_id');
	}

	public function user(){
		return $this->belongsTo($this->user,'writer_id');
	}

	public function category(){
		return $this->belongsTo($this->category,'category_id');
	}

	public function post_related(){
		return $this->hasMany($this->post_related,'post_id');
	}
}
