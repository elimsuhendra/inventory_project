<?php
	Route::get('login','LoginController@index');
	Route::post('cek_login','LoginController@cek_login');
	Route::get('logout','LoginController@logout');
	
	Route::get('profile', 'LoginController@profile');
	Route::post('update_profile', 'LoginController@update_profile');
	Route::get('change-language/{id}','LoginController@change_language');

	//Main route
	Route::get('/', 'IndexController@index');

	Route::group(['prefix' => 'catalog'], function(){
		Route::resource('product-category', 'ProductcategoryController');
		Route::any('product-category/ext/{action}','ProductcategoryController@ext');

		Route::resource('product-sub-category', 'ProductsubcategoryController');
		Route::any('product-sub-category/ext/{action}','ProductsubcategoryController@ext');

		Route::resource('brand', 'BrandController');
		Route::any('brand/ext/{action}','BrandController@ext');

		Route::resource('product', 'ProductController');
		Route::any('product/ext/{action}','ProductController@ext');

		Route::resource('product-attribute', 'ProductattributeController');
		Route::any('product-attribute/ext/{action}','ProductattributeController@ext');

		Route::resource('product-attribute-data', 'ProductattributedataController');
		Route::any('product-attribute-data/ext/{action}','ProductattributedataController@ext');

		Route::resource('price-rule', 'PriceruleController');
		Route::any('price-rule/ext/{action}','PriceruleController@ext');
	});

	Route::group(['prefix' => 'order'], function(){
		Route::resource('orders', 'OrdersController');
		Route::any('orders/ext/{action}','OrdersController@ext');
	});

	Route::group(['prefix' => 'members'], function(){
		Route::resource('customer', 'CustomerController');
		Route::any('customer/ext/{action}','CustomerController@ext');

		Route::resource('customer-group', 'CustomergroupController');
		Route::any('customer-group/ext/{action}','CustomergroupController@ext');
	});

	Route::group(['prefix' => 'shipping-&-payment'], function(){
		Route::resource('payment-method', 'PaymentmethodController');
		Route::any('payment-method/ext/{action}','PaymentmethodController@ext');

		Route::resource('bank-account', 'BankaccountController');
		Route::any('bank-account/ext/{action}','BankaccountController@ext');

		Route::resource('delivery-service', 'DeliveryserviceController');
		Route::any('delivery-service/ext/{action}','DeliveryserviceController@ext');

		Route::resource('delivery-service-sub', 'DeliveryservicesubController');
		Route::any('delivery-service-sub/ext/{action}','DeliveryservicesubController@ext');
	});

	Route::group(['prefix' => 'promotion'], function(){
		Route::resource('voucher', 'VoucherController');
		Route::any('voucher/ext/{action}','VoucherController@ext');
	});

	Route::group(['prefix' => 'blog'], function(){
		Route::resource('category', 'BlogcategoryController');
		Route::any('category/ext/{action}','BlogcategoryController@ext');

		Route::resource('sub-category', 'BlogsubcategoryController');
		Route::any('sub-category/ext/{action}','BlogsubcategoryController@ext');

		Route::resource('tag', 'TagController');
		Route::any('tag/ext/{action}','TagController@ext');

		Route::resource('post', 'PostController');
		Route::any('post/ext/{action}','PostController@ext');
	});

	Route::group(['prefix' => 'notification-template'], function(){
		Route::resource('email-template', 'EmailtemplateController');
		Route::any('email-template/ext/{action}','EmailtemplateController@ext');

		Route::resource('sms-template', 'SmstemplateController');
		Route::any('sms-template/ext/{action}','SmstemplateController@ext');
	});

	Route::group(['prefix' => 'front-end'], function(){
		Route::resource('pages', 'PagesController');
		Route::any('pages/ext/{action}','PagesController@ext');

		Route::resource('slide-show', 'SlideshowController');
		Route::any('slide-show/ext/{action}','SlideshowController@ext');

		Route::resource('banner', 'BannerController');
		Route::any('banner/ext/{action}','BannerController@ext');

		Route::resource('social-media', 'SocialmediaController');
		Route::any('social-media/ext/{action}','SocialmediaController@ext');

		Route::resource('popup', 'PopupController');
		Route::any('popup/ext/{action}','PopupController@ext');

		Route::resource('contact', 'ContactController');
		Route::any('contact/ext/{action}','ContactController@ext');

		Route::resource('contact-message', 'ContactmessageController');
		Route::any('contact-message/ext/{action}','ContactmessageController@ext');

		Route::resource('subscriber', 'SubscriberController');
		Route::any('subscriber/ext/{action}','SubscriberController@ext');

		Route::get('content', 'ContentController@index');
		Route::post('content/ext/{id}', 'ContentController@ext');

		Route::resource('featured-product', 'FeaturedproductController');
		Route::any('featured-product/ext/{id}', 'FeaturedproductController@ext');
	});

	Route::group(['prefix' => 'report'], function(){
		Route::resource('income-report', 'IncomeReportController');
		Route::any('income-report/ext/{action}','IncomeReportController@ext');
	});

	Route::group(['prefix' => 'administration'], function(){
		Route::resource('user-access', 'UseraccessController');
		Route::any('user-access/ext/{action}','UseraccessController@ext');

		Route::resource('user', 'UserController');
		Route::any('user/ext/{action}','UserController@ext');

		Route::resource('writer', 'WriterController');
		Route::any('writer/ext/{action}','WriterController@ext');
	});

	Route::group(['prefix' => 'flash-deal'], function(){
		Route::resource('manage-flash', 'FlashdealController');
		Route::get('manage-flash/delete_product/{id}/{product}','FlashdealController@delete_product');
		Route::any('manage-flash/ext/{action}','FlashdealController@ext');
		Route::post('manage-flash/get_pro','FlashdealController@product_get');
		Route::post('manage-flash/get_edt','FlashdealController@product_edt');
	});

	Route::group(['prefix' => 'preferences'], function(){
		Route::get('general-settings', 'ConfigController@index');
		Route::post('general-settings/update', 'ConfigController@update');
		Route::get('general-settings/sync', 'ConfigController@sync');
	});

	//Main services for ajax outside builder
	Route::any('services','ServicesController@index');