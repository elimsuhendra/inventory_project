<?php

Route::get('booting', 'IndexController@booting');
Route::get('home', 'IndexController@index');

Route::group(['prefix' => 'account'], function(){
	Route::post('login', 'LoginController@login');
	Route::post('register', 'LoginController@register');
});
Route::group(['prefix' => 'my-restaurant'], function(){
	Route::get('dashboard', 'RestaurantController@dashboard');
	Route::get('create', 'RestaurantController@create');
	Route::post('store', 'RestaurantController@store');
	Route::get('edit/{id}', 'RestaurantController@edit');
	Route::put('update', 'RestaurantController@update');
	Route::delete('delete', 'RestaurantController@destroy');

});