<?php
//Main route
Route::get('/', 'IndexController@index')->name('homes');
Route::match(['get', 'post'], 'search', 'PagesController@search');
Route::post('subscribe', 'PagesController@subscribe');

Route::get('cart', 'CartController@cart')->name('c_page');
Route::get('product/{id}', 'ProductController@index');

Route::get('aksesoris', 'PagesController@aksesoris');
Route::get('kontak-servis', 'PagesController@ctc_us');
Route::post('kirim_pesan', 'PagesController@kirim_pesan');
Route::get('tentang-kami', 'PagesController@ab_us');
// Route::get('cara-beli', 'PagesController@cr_bl');
Route::get('hubungi-kami', 'PagesController@ct_us');
// Route::get('informasi', 'PagesController@info');
Route::get('desain-interior', 'PagesController@desain_interior');
Route::get('ThankYou', 'CheckoutController@nicepay_thankyou');

Route::get('wishlist', 'ProductController@wish');

Route::group(['prefix' => 'category'], function(){
	Route::get('/{cat}', 'PagesController@category');
});

Route::group(['prefix' => 'filter'], function(){
	Route::get('/{fil}', 'PagesController@filter');
});

Route::group(['prefix' => 'desain'], function(){
	Route::post('/', 'PagesController@desain_contact');
	Route::get('/{des}', 'PagesController@desain');
});

Route::group(['prefix' => 'parts'], function(){
	Route::get('mini-cart', 'CartController@miniCart');
	Route::get('cart', 'CartController@cartParts');
	Route::get('side-cart', 'CartController@sideCartParts');
});

Route::group(['prefix' => 'checkout'], function(){
	Route::get('/', 'CheckoutController@index'); /* Custom */
	Route::post('/order', 'CheckoutController@checkout_order');
	Route::get('payment-method', 'CheckoutController@payment_method');
	Route::post('update-payment-method', 'CheckoutController@update_payment_method');
	Route::get('summary-checkout', 'CheckoutController@summary_checkout');
	Route::get('order-detail', 'CheckoutController@order_detail')->name('order-detail');
	Route::get('order-detail/{inv}', 'CheckoutController@order_detail_pdf');
	Route::post('finish-checkout', 'CheckoutController@finish_checkout');
});

Route::get('/confirm-payment', 'CheckoutController@c_payment');

Route::get('login', 'AccountController@index');
Route::get('register', 'AccountController@index_reg');
Route::get('forgot-password', 'AccountController@index_for');
Route::get('logout', 'AccountController@logout');
Route::post('do-login', 'AccountController@login');
Route::post('chk-login', 'AccountController@login2');
Route::post('do-register', 'AccountController@register');
Route::post('forgot-password', 'AccountController@forgot_password');
Route::get('profile', 'AccountController@profile');
Route::post('add-profile', 'AccountController@add_profile');
Route::get('edit-profile', 'AccountController@edit_profile');
Route::post('update-profile', 'AccountController@update_profile');
Route::get('change-password', 'AccountController@change_password');
Route::post('update-password', 'AccountController@update_password');
Route::get('order-histori', 'AccountController@order_histori');
Route::post('order-view', 'AccountController@order_view');

Route::group(['prefix' => 'forgot-password'], function(){
	route::post('/confirm-password', 'AccountController@ch_forgot_password');
});

Route::get('forget-password/{token}', 'AccountController@c_forgot_password');

Route::group(['prefix' => 'account'], function(){
	Route::get('profile', 'AccountController@profile');
	Route::post('update-profile', 'AccountController@update_profile');
});

Route::group(['prefix' => 'v1'], function(){
	Route::post('add-cart', 'ProductController@addCart');
	Route::post('update-cart', 'ProductController@updateCart');
	Route::post('remove-cart', 'ProductController@removeCart');
	Route::post('c_update-cart', 'ProductController@c_updateCart');
	Route::post('update-checkout-notes', 'ProductController@updateCheckoutnotes');
	Route::get('total-cart', 'ProductController@totalCart');
	Route::post('update-primary-address', 'AccountController@primary_address');
	Route::post('update-delivery-address', 'CheckoutController@delivery_address');
	Route::get('get-delivery-service-sub', 'CheckoutController@get_delivery_service_sub');
	Route::post('update-delivery-service-sub', 'CheckoutController@update_delivery_service_sub');
	Route::get('get-city', 'CartController@get_city');
	Route::get('get-subdistrict', 'CartController@get_subdistrict');
	Route::get('get-courier', 'CartController@get_courier');
	Route::get('get_cost', 'CartController@get_cost');
	Route::post('check_coupon', 'CartController@check_coupon');
	Route::post('remove_coupon', 'CartController@remove_coupon');
	Route::get('live_search', 'PagesController@sc_live');
	Route::get('live_inv', 'CheckoutController@inv_live');
	Route::post('add_wish', 'ProductController@sync_dbase_wishlist');
	Route::post('remove_wish', 'ProductController@remove_dbase_wishlist');
	Route::post('ch_st_pro', 'ProductController@ch_stock_pro');
	Route::post('atr_img', 'ProductController@attr_image');
});

//Pages
// Route::get('recipes/pdf/{slug}', 'PagesController@pdf');

// Route::get('{pages}', 'PagesController@pages');

// Route::get('{pages}/{slug}', 'PagesController@pages');

// Route::post('{pages}', 'PagesController@pages2');
// Route::post('{pages}/{slug}', 'PagesController@pages2');
