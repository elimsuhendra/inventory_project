@extends($view_path.'.layouts.master')
@section('content')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green">
            <i class="icon-layers font-green title-icon"></i>
            <span class="caption-subject bold uppercase"> {{$title}}</span>
        </div>
        <div class="actions">
            <div class="actions">
                <div class="btn-group">
                    <a class="btn blue" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="{{url($path)}}/ext/invoice?id={{$orderhd->id}}" target="_blank"> Print Invoice </a>
                        </li>
                        <!-- <li>
                            <a href="{{url($path)}}/ext/delivery_label?id={{$orderhd->id}}" target="_blank"> Print Delivery Label </a>
                        </li> -->
                    </ul>
                </div>
                <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
            </div>
        </div>
    </div>
    <div class="portlet-body form">
        @include('admin.includes.errors')
        <div class="tabbable-line">
            <ul class="nav nav-tabs ">
              <li class="active">
                <a href="#summary" data-toggle="tab" aria-expanded="true">Summary</a>
              </li>
              <li>
                <a href="#status" data-toggle="tab" aria-expanded="false">Order Status</a>
              </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="summary">
                    <div class="row">
                        <div class="col-md-{{$orderhd->order_status_id > 2 ? 6 : 12}} col-sm-12">
                            <div class="portlet yellow-crusta box">
                                <div class="portlet-title">
                                    <div class="caption">
                                      <i class="fa fa-cogs"></i>Order Details 
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Order #: </div>
                                        <div class="col-md-7 value"> {{$orderhd->order_id}}</div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Order Date & Time: </div>
                                        <div class="col-md-7 value"> {{date_format(date_create($orderhd->order_date),'d-m-Y H:i:s')}} </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Order Status: </div>
                                        <div class="col-md-7 value">
                                            @if($orderhd->order_status_id == 1)
                                                <span class="label label-warning"> 
                                            @elseif($orderhd->order_status_id == 20)
                                                <span class="label label-info">
                                            @elseif($orderhd->order_status_id == 2)
                                                <span class="label label-primary">
                                            @elseif($orderhd->order_status_id == 3)
                                                <span class="label label-default">
                                            @elseif($orderhd->order_status_id == 4)
                                                <span class="label label-success">
                                            @else
                                                <span class="label label-danger">
                                            @endif
                                            {{$orderhd->orderstatus->order_status_name}}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Total: </div>
                                        <div class="col-md-7 value"> IDR {{number_format($orderhd->total_amount,0,',','.')}} </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Payment Information: </div>
                                        <div class="col-md-7 value"> {{$orderhd->payment_method->payment_method_name}} </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Customer Name: </div>
                                        <div class="col-md-7 value"> {{$customer->name}} </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Email: </div>
                                        <div class="col-md-7 value"> {{$customer->email}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($orderhd->order_status_id > 2)
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Shipping Information
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Shipping Vendor: </div>
                                            <div class="col-md-7 value"> {{$orderaddress->delivery_service_name}} </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Shipping Price: </div>
                                            <div class="col-md-7 value">IDR {{number_format($orderhd->shipping_amount,0,',','.')}} </div>
                                        </div>
                                        @if($orderhd->tracking_code)
                                            <div class="row static-info">
                                                <div class="col-md-5 name"> Tracking Code: </div>
                                                <div class="col-md-7 value"> {{$orderhd->tracking_code}} </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="portlet green-meadow box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Billing Address
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row static-info">
                                        <div class="col-md-12 value"> {{$orderbilling->first_name}} {{$orderbilling->last_name}}
                                            <br> {{$orderbilling->address}}
                                            <br> {{$orderbilling->province}}
                                            <br> {{$orderbilling->city}}, {{$orderbilling->sub_district}} - {{$orderbilling->postal_code}}
                                            <br> {{$orderbilling->phone}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="portlet red-sunglo box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Shipping Address
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row static-info">
                                        <div class="col-md-12 value"> {{$orderaddress->first_name}} {{$orderaddress->last_name}}
                                            <br> {{$orderaddress->address}}
                                            <br> {{$orderaddress->province}}
                                            <br> {{$orderaddress->city}}, {{$orderaddress->sub_district}} - {{$orderaddress->postal_code}}
                                            <br> {{$orderaddress->phone}}
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet grey-cascade box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Shopping Cart
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th> Product </th>
                                                    <th> Attribute </th>
                                                    <th> Unit Price </th>
                                                    <th> Quantity </th>
                                                    <th> Weight</th>
                                                    <th> Total Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($orderdt as $dt)
                                                    <tr>
                                                        <td>
                                                            <img src="{{asset($dt->thumbnail)}}" width="80"> 
                                                        </td>
                                                        <td>
                                                            <b>{{$dt->product_code}}</b><br/>
                                                            {{$dt->product_name}}
                                                        <td>
                                                            @foreach((array)json_decode($dt->attribute_description) as $ad => $q)
                                                                {{$ad}} : {{$q}} <br/>
                                                            @endforeach
                                                        </td>
                                                        <td> IDR {{number_format($dt->price,0,',','.')}}<br/><br/>
                                                            @if($dt->discount > 0)
                                                                {{ $dt->discount_type == 'v' ? 'IDR' : '' }} 
                                                                <span>Discount :</span> 
                                                                {{ number_format($dt->discount,0,',','.') }} 
                                                                {{ $dt->discount_type == 'p' ? '% Off' : '' }}
                                                            @endif
                                                        </td>
                                                        <td> {{$dt->qty}} </td>
                                                        <td> Per Item: {{$dt->weight}} <br/> Total Weight: {{$dt->total_weight}}</td>
                                                        @php
                                                            $total_price        = $dt->total_price;
                                                            if($dt->discount > 0){
                                                                $total_price    = $total_price - ($dt->discount * $total_price / 100);
                                                            }
                                                        @endphp
                                                        <td> IDR {{number_format($total_price,0,',','.')}} </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="well">
                                <div class="row static-info align-reverse">
                                    <div class="col-md-6 name"> Kode Unik: </div>
                                    <div class="col-md-5 value">{{ $orderhd->kode_unik }}</div>
                                </div>
                                <div class="row static-info align-reverse">
                                    <div class="col-md-6 name"> Sub Total: </div>
                                    <div class="col-md-5 value"> IDR {{number_format($orderhd->sub_total_amount,0,',','.')}}</div>
                                </div>
                                <div class="row static-info align-reverse">
                                    <div class="col-md-6 name">
                                        Total Shipping Amount <br/>
                                        <small>Total Weight : {{$orderhd->total_weight}} Kg</small><br/>
                                        <small>Shipping Amount : IDR {{number_format($orderhd->shipping_amount,0,',','.')}}</small>
                                    </div>
                                    <div class="col-md-5 value"> IDR {{number_format($orderhd->total_shipping_amount,0,',','.')}} </div>
                                </div>
                                @if($orderhd->voucher_amount > 0)
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-6 name"> Voucher Amount: </div>
                                        <div class="col-md-5 value"> (IDR {{number_format($orderhd->voucher_amount,0,',','.')}}) </div>
                                    </div>
                                @endif
                                <div class="row static-info align-reverse">
                                    <div class="col-md-6 name"> Total Amount: </div>
                                    <div class="col-md-5 value"> IDR {{number_format($orderhd->total_amount,0,',','.')}} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="status">
                    <div class="row">
                        <div class="col-md-12"><h4>Order Status History</h4></div>
                        <div class="col-md-7">
                            <table class="table">
                                <th>Status</th>
                                <th>Processed By</th>
                                <th>Date</th>
                                <th>Notes</th>
                                @foreach($orderlog as $l)
                                    <tr>
                                        <td>
                                            @if($l->order_status_id == 1)
                                                <span class="label label-warning"> 
                                            @elseif($l->order_status_id == 20)
                                                <span class="label label-info">
                                            @elseif($l->order_status_id == 2)
                                                <span class="label label-primary">
                                            @elseif($l->order_status_id == 3)
                                                <span class="label label-default">
                                            @elseif($l->order_status_id == 4)
                                                <span class="label label-success">
                                            @else
                                                <span class="label label-danger">
                                            @endif
                                            {{$l->orderstatus->order_status_name}}
                                            </span>
                                        </td>
                                        <td>
                                            @if($l->upd_owner == 'a')
                                                Admin - {{$l->user->name}}
                                            @elseif($l->upd_owner == 'c')
                                                Customer
                                            @else
                                                System Administrator
                                            @endif
                                        </td>
                                        <td>{{date_format(date_create($l->updated_at),'d-m-Y H:i:s')}}</td>
                                        <td>
                                            {{$l->notes ?? '-' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>
                        <div class="col-md-12">
                            <div class="portlet yellow-crusta box">
                                <div class="portlet-title">
                                    <div class="caption">
                                      <i class="fa fa-cogs"></i>Order Confirmation History 
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table">
                                        <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Bank Destination</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Payment Date</th>
                                            <th>Notes</th>
                                            @foreach($orderconfirm as $oc)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td>
                                                        @if($oc->image_name != NULL)
                                                            <a class="image-popup-no-margins" href="{{ url('/') }}/{{ $oc->image_name }}">
                                                                <img src="{{ url('/') }}/{{ $oc->image_name }}" width="100" height="100">
                                                            </a>
                                                        @else
                                                            <a class="image-popup-no-margins" href="{{ url('/components/both/images/other/not-found.png') }}">
                                                                <img src="{{ url('/components/both/images/other/not-found.png') }}" width="100" height="100">
                                                            </a>
                                                        @endif
                                                    </td>
                                                    <td>{{$oc->destination}}</td>
                                                    <td>{{$oc->name}}</td>
                                                    <td>IDR {{number_format($oc->amount,0,',','.')}}</td>
                                                    <td>{{date_format(date_create($oc->payment_date),'d-m-Y')}}</td>
                                                    <td>{{$oc->notes ?? '-'}}</td>
                                                </tr>
                                            @endforeach
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')

@endpush
@push('custom_scripts')
<script>
   $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });
</script> 
@endpush
@endsection
