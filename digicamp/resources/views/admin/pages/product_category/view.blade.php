@extends($view_path.'.layouts.master')
@section('content')
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green">
      <i class="icon-layers font-green title-icon"></i>
      <span class="caption-subject bold uppercase"> {{$title}}</span>
    </div>
    <div class="actions">
      <div class="actions">
        {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
  </div>
  <div class="portlet-body form">
    @include('admin.includes.errors')
    <div class="tabbable-line">
      <ul class="nav nav-tabs ">
        <li class="active">
          <a href="#content" data-toggle="tab" aria-expanded="true">General</a>
        </li>
        <li>
          <a href="#seo" data-toggle="tab" aria-expanded="false">SEO</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="content">
          <div class="row">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'category_name','label' => 'Category Name','value' => (old('category_name') ? old('category_name') : $get_category->category_name),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

            <div class="col-md-12">
              <label>Category Name Alias</label>
            </div>
              
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'category_name_alias','value' => (old('category_name_alias') ? old('category_name_alias') : $get_category->category_name_alias),'attribute' => 'required autofocus','class' => 'slug','form_class' => 'col-md-12'])!!}

          </div>

          {!!view($view_path.'.builder.file',['name' => 'image','label' => 'Category Image','value' => $get_category->image,'file_opt' => ['path' => 'components/front/images/product_category/'],'type' => 'file','upload_type' => 'single-image','note' => 'Note: File Must jpeg,png,jpg,gif','form_class' => 'col-md-6 pad-left', 'validation' => 'mimes:jpeg,png,jpg,gif'])!!}

          {!!view($view_path.'.builder.file',['name' => 'image_active','label' => 'Category Image Active','value' => $get_category->image_active,'file_opt' => ['path' => 'components/front/images/product_category/'],'type' => 'file','upload_type' => 'single-image','note' => 'Note: File Must jpeg,png,jpg,gif','form_class' => 'col-md-6 pad-left', 'validation' => 'mimes:jpeg,png,jpg,gif'])!!}

          <div class="row">
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Status Category','value' => (old('status') ? old('status') : $get_category->status),'attribute' => 'required', 'form_class' => 'col-md-6 pad-right'])!!}      
          </div>

          <!-- <div class="row">
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Sticky','n' => 'Not Sticky'],'name' => 'sticky_category','label' => 'Sticky Category','value' => (old('status') ? old('status') : $get_category->sticky_category), 'form_class' => 'col-md-6 pad-right'])!!}      
          </div>   -->      
        </div>

        <div class="tab-pane" id="seo">
          <div class="row">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_title','label' => 'Meta Title','value' => (old('meta_title') ? old('meta_title') : $get_category->meta_title),'form_class' => 'col-md-12'])!!}
          </div>
          
          <div class="row">
            {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'meta_description','label' => 'Meta Description','value' => (old('meta_description') ? old('meta_description') : $get_category->meta_description),'form_class' => 'col-md-12'])!!}
          </div>

          <div class="row">
            {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'meta_keywords','label' => 'Meta Keywords','value' => (old('meta_keywords') ? old('meta_keywords') : $get_category->meta_keywords),'form_class' => 'col-md-12'])!!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('custom_scripts')
    <script>
      $(document).ready(function(){
        $('input,select,textarea,checkbox,.remove-single-image,button').prop('disabled',true);
        $('a > button').prop('disabled',false);
        tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });
      });
    </script>
@endpush
