@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$flash->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint back_btn">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
        <div class="col-md-4">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'tanggal','label' => 'Tanggal Flash','class' => 'datepicker','value' => (old('tanggal') ? old('tanggal') : $flash->tanggal),'attribute' => 'required autofocus'])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_from','label' => 'Valid From','class' => 'datetimepicker','value' => (old('valid_from') ? old('valid_from') : $flash->valid_from),'attribute' => 'required autofocus'])!!}
        </div>

        <div class="col-md-6">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_to','label' => 'Valid To','class' => 'datetimepicker','value' => (old('valid_to') ? old('valid_to') : $flash->valid_to),'attribute' => 'required autofocus'])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Publish','value' => (old('status') ? old('status') : $flash->status)])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <br><br><h4>Add Product</h4>
        </div>
      </div>

      <div class="row" id="prods_data">
        <div class="col-md-12 data_product">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>No</th>
                <th>Product Name</th>
                <th>Stock</th>
                <th>Discount</th>
                <th colspan="2"">Action</th>
              </tr>

              @php
                $prod = json_decode($flash->product_id);
              @endphp

              @foreach($product_flash as $l => $pro)
                <tr>
                    <td>{{ $l+1 }}</td>
                    <td>{{ $pro['product_name'] }}</td>
                    <td>{{ $pro['stock'] }}</td>
                    <td>{{ $pro['discount'] }}% OFF</td>
                    <td><button type="button" class="btn green ed_pro" data-id="{{ $flash->id }}" data-proid="{{ $pro['product_id'] }}">Edit</button></td>
                    <td><a href="{{ url($path)}}/delete_product/{{ $flash->id }}/{{ $pro['product_id'] }}" class="del_pro"><button type="button" class="btn red-mint">Delete</button></a></td>
              </tr>
              @endforeach
              <div
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</form>
@push('custom_scripts')
    <script>
       $('input,select,textarea,checkbox,.remove-single-image,button,.del_pro').prop('disabled',true);
       $('.back_btn').prop('disabled',false);
    </script>
@endpush
@endsection
