@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$flash->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
        <div class="col-md-4">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'tanggal','label' => 'Tanggal Flash','class' => 'datepicker','value' => (old('tanggal') ? old('tanggal') : date_format(date_create($flash->tanggal),'d-m-Y')),'attribute' => 'required'])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_from','label' => 'Valid From','class' => 'from_time','value' => (old('valid_from') ? old('valid_from') : $flash->valid_from),'attribute' => 'required'])!!}
        </div>

        <div class="col-md-6">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_to','label' => 'Valid To','class' => 'to_time','value' => (old('valid_to') ? old('valid_to') : $flash->valid_to),'attribute' => 'required'])!!}
        </div>
      </div>

       <div class="row">
        <div class="col-md-12">
          {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Publish','value' => (old('status') ? old('status') : $flash->status)])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <br><br><h4>Add Product</h4>
        </div>

        <div class="col-md-4">
          {!!view($view_path.'.builder.select',['type' => 'select','name' => 'product_id','label' => 'Product Name','class' => 'select2 pro_id','data' => $product, 'value' => ''])!!}
        </div>

        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <label>Stock: </label>
            <input type="hidden" id="arr_product_flash2" value="{{$product_dam}}"></input>
           <span id="stock_now"></span>
          </div>
        </div>
      </div>

      <div class="row">
         <div class="col-md-4">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'stock','label' => 'Stock', 'class' => 'pro_stock'])!!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'price','label' => 'Discount (%)', 'class' => 'pro_price'])!!}
        </div>

        <div class="col-md-12">
          <input type="hidden" data-id="{{ $flash->id }}" class="ids_hide" />
          {!!view($view_path.'.builder.button',['class' => 'btn green','label' => 'Add Product', 'class' => 'add_flash'])!!}
        </div>
      </div>
      <br>

      <div class="row" id="prods_data">
        <div class="col-md-12 data_product">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>No</th>
                <th>Product Name</th>
                <th>Stock</th>
                <th>Discount</th>
                <th colspan="2"">Action</th>
              </tr>

              @php
                $prod = json_decode($flash->product_id);
              @endphp

              @foreach($product_flash as $l => $pro)
                <tr>
                    <td>{{ $l+1 }}</td>
                    <td>{{ $pro['product_name'] }}</td>
                    <td>{{ $pro['stock'] }}</td>
                    <td>{{ $pro['discount'] }}% OFF </td>
                    <td><button type="button" class="btn green ed_pro" data-id="{{ $flash->id }}" data-proid="{{ $pro['product_id'] }}">Edit</button></td>
                    <td><a href="{{ url($path)}}/delete_product/{{ $flash->id }}/{{ $pro['product_id'] }}"><button type="button" class="btn red-mint">Delete</button></a></td>
              </tr>
              @endforeach
              <div
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</form>
@push('custom_scripts')
    <script>
      $(document).on('click','.add_flash',function(){
        $get_id = $('.ids_hide').data('id');
        $ids = $('.pro_id').val();
        $stc = $('.pro_stock').val();
        $prc = $('.pro_price').val();
        $fail = 0;

        if(!$ids.length > 0){
          $.growl_alert('Product Name Wajib Diisi','danger');
          $fail = 1;
        }

        if(!$stc.length > 0){
          $.growl_alert('Stock Wajib Diisi','danger');
          $fail = 1;
        }

        if(!$prc.length > 0){
          $.growl_alert('Price Wajib Diisi','danger');
          $fail = 1;
        }

        if($fail == 0){
          d = {id:$get_id,product_id:$ids,stock:$stc,price:$prc};

          $.postdata('{{url($path.'/get_pro')}}', d).success(function(data){
            if(data.status == 'success'){
              $('.pro_id').select2("val", "");
              $('.pro_stock').val('');
              $('.pro_price').val('');
              $('#prods_data').load($.cur_url()+' .data_product');
              $.growl_alert('Add Product Success','success');
            } else if(data.status == 'duplicate'){
              $('.pro_id').select2("val", "");
              $('.pro_stock').val('');
              $('.pro_price').val('');
              $.growl_alert('Product already exist','danger');
            }else{  
              $('.pro_id').select2("val", "");
              $('.pro_stock').val('');
              $('.pro_price').val('');
              $.growl_alert('Add Product Failed','danger');
            }
          });
        }
      });

      $(document).on('click','.ed_pro',function(){
        $id = $(this).data('id');
        $ids = $(this).data('proid');
        
        d = {id:$id,product_id:$ids};

        $.postdata('{{url($path.'/get_edt')}}', d).success(function(data){
          if(data.status == 'success'){
            $('.pro_id').val(data.product_id).trigger('change.select2');
            $('.pro_stock').val(data.stock);
            $('.pro_price').val(data.discount);
            $('.pro_stock').focus();
            $('.pro_price').focus();
          }
        });
      });

      $(document).ready(function(){
          $('.from_time').timepicker({
            showSeconds: true,
            showMeridian: false,
          });
          $('.to_time').timepicker({
            showSeconds: true,
            showMeridian: false,
          });

          $('.pro_id').on('change', function(){
              $('#stock_now').text('');
              var arr_product_flash2 = $('#arr_product_flash2').val();
              var arr = JSON.parse(arr_product_flash2);
              var stock =0;
              for(var i=0; i<arr.length; i++){
                if(arr[i].product_id == $(this).val()){
                  stock = arr[i].stock;
                }
              }
              $('#stock_now').text(stock);
          });

      });
    </script>
@endpush
@endsection
