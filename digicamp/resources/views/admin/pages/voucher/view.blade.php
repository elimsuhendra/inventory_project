@extends($view_path.'.layouts.master')
@section('content')
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green">
      <i class="icon-layers font-green title-icon"></i>
      <span class="caption-subject bold uppercase"> {{$title}}</span>
    </div>
    <div class="actions">
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
  </div>
  <div class="portlet-body form">
    @include('admin.includes.errors')
    <div class="tabbable-line">
      <ul class="nav nav-tabs ">
        <li class="active">
          <a href="#summary" data-toggle="tab" aria-expanded="true">Summary</a>
        </li>
        <li>
          <a href="#cs_group" data-toggle="tab" aria-expanded="false">Customer Group</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="summary">
          <div class="row">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'voucher_code','label' => 'Voucher Code','value' => (old('voucher_code') ? old('voucher_code') : $voucher->voucher_code),'attribute' => 'required disabled autofocus','form_class' => 'col-md-6'])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'voucher_name','label' => 'Voucher Name','value' => (old('voucher_name') ? old('voucher_name') : $voucher->voucher_name),'attribute' => 'required','form_class' => 'col-md-6'])!!}
          </div>
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : $voucher->description)])!!}
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_time','label' => 'Valid Time','value' => (old('valid_time') ? old('valid_time') : date_format(date_create($voucher->valid_from),'d/m/Y').' - '.date_format(date_create($voucher->valid_to),'d/m/Y')),'attribute' => 'readonly required','class' => 'datetimerange'])!!}
          {!!view($view_path.'.builder.select',['type' => 'select','name' => 'discount_type', 'data' => ['v' => 'Value', 'p' => 'Percentage'],'value' => (old('discount_type') ? old('discount_type') : $voucher->discount_type),'label' => 'Discount Type','class' => 'select2'])!!}
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'amount','label' => 'Amount','value' => (old('amount') ? old('amount') : $voucher->amount)])!!}
          {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Status','value' => (old('status') ? old('status') : $voucher->status)])!!}
        </div>
        <div class="tab-pane" id="cs_group">
          <div class="row">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'min_checkout','label' => 'Minimal Purchase To Use Voucher','value' => (old('min_checkout') ? old('min_checkout') : $voucher->min_checkout),'form_class' => 'col-md-4'])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'total','label' => 'Total Voucher','value' => (old('total') ? old('total') : $voucher->total),'form_class' => 'col-md-4'])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'total_use_per_user','label' => 'Total Use Voucher Per User','value' => (old('total_use_per_user') ? old('total_use_per_user') : $voucher->total_use_per_user),'form_class' => 'col-md-4'])!!}
          </div>
          {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Yes','n' => 'No'],'name' => 'free_shipping','label' => 'Free Shipping','value' => (old('free_shipping') ? old('free_shipping') : $voucher->free_shipping)])!!}
          <!-- {!!view($view_path.'.builder.checkbox',['type' => 'checkbox','name' => 'voucher_customer_group_data','label' => 'Apply To Customer Group','data' => $customer_group,'value' => $voucher_customer_group_data,'note' => 'If not checked, this will apply to all user'])!!} -->
        </div>
      </div>
    </div>
  </div>
</div>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('input,select,textarea,checkbox,.remove-single-image').prop('disabled',true);
      tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });
    });
  </script>
@endpush
@endsection
