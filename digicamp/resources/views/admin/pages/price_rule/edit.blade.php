@extends($view_path.'.layouts.master')
@section('content')

@if(session()->has('re_tabs'))
  <input type="hidden" value="{{ session()->get('re_tabs') }}" id="re_tabs" />
@endif

<form role="form" method="post" action="{{url($path)}}/{{$price_rule->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#info" data-toggle="tab" aria-expanded="true">Information</a>
          </li>
          <li>
            <a href="#condition" data-toggle="tab" aria-expanded="false">Condition</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="info">
            <div class="row">
              <div class="col-md-6">
                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'rule_name','label' => 'Rule Name','value' => (old('rule_name') ? old('rule_name') : $price_rule->rule_name),'attribute' => 'autofocus'])!!}
              </div>
            </div>

            <input type="hidden" value="{{ $price_rule->type }}" name="type" />

            <div class="row">
              <div class="col-md-6">
                {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Status','value' => $price_rule->status,'attribute' => 'required', 'form_class' => 'pad-right'])!!}  
              </div>
            </div>
          </div>

          <div class="tab-pane" id="condition">
            <div class="row">
              @php
                $get_valid_from   = date_create($price_rule->valid_from);
                $get_valid_to     = date_create($price_rule->valid_to);
                $conv_valid_from  = date_format($get_valid_from,"d-m-Y H:i:s");
                $conv_valid_to    = date_format($get_valid_to,"d-m-Y H:i:s");
              @endphp
              
              <div class="col-md-6">
                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_from','label' => 'Valid Date','value' => (old('valid_from') ? old('valid_from') : $conv_valid_from),'attribute' => 'required', 'class' => 'datetimepicker'])!!}
              </div>

              <div class="col-md-6">
                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_to','label' => '','value' => (old('valid_to') ? old('valid_to') : $conv_valid_to),'attribute' => 'required', 'class' => 'datetimepicker'])!!}
              </div>
            </div>

            @if($price_rule->type == "reseller")
              <div class="row">
                <div class="col-md-6">
                  {!!view($view_path.'.builder.text',['type' => 'text','name' => 'from_qty','label' => 'From Quantity','value' => (old('from_qty') ? old('from_qty') : $price_rule->from_qty),'attribute' => 'required autofocus'])!!}
                </div>
              </div>
            @elseif($price_rule->type == "discount")

            @endif

            <div class="row">
              <div class="col-md-6">
                {!!view($view_path.'.builder.select',['type' => 'select','name' => 'reduction_type','label' => 'Reduction Type','data' => $discount_type,'value' => (old('reduction_type') ? old('reduction_type') : $price_rule->reduction_type),'attribute' => 'required'])!!}
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'reduction_amount','label' => 'Reduction Amount','value' => (old('reduction_amount') ? old('reduction_amount') : $price_rule->reduction_amount)])!!}
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-12">
                <label class="control-label">Spesific Product</label>
                <select class="form-control select2-multiple" name="exc_product[]" multiple>
                  <optgroup label="Product">
                    @foreach($product as $pro)
                      @if($pro->id_price_rule == $price_rule->id)
                        <option value="{{$pro->id}}" {{ $pro->id_product != NULL ? 'selected' : '' }}>{{$pro->name}}</option>
                      @else
                        <option value="{{$pro->id}}">{{$pro->name}}</option>
                      @endif
                    @endforeach
                  </optgroup>
                </select>

                <div class="form-control-focus"> </div>
              </div>

              <div class="col-md-12">
                <small>Note : if spesific product empty, than it will affect all product.</small>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@push('custom_scripts')
<script>
  jQuery(document).ready(function() {
    $(".select2-multiple").select2({
        placeholder: "Klik di sini",
        width: null
    });

    if ($('#re_tabs').length > 0) {
      var tabs = $('#re_tabs').val();
      $('.nav-tabs > li > a[href="#' + tabs + '"]').tab('show');
    }
  });
</script>
@endpush
