@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <ul class="nav nav-tabs ">
          <li class="active">
            <a href="#info" data-toggle="tab" aria-expanded="true">Information</a>
          </li>
          <li>
            <a href="#condition" data-toggle="tab" aria-expanded="false">Condition</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="info">
            <div class="row">
              <div class="col-md-6">
                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'rule_name','label' => 'Rule Name','value' => (old('rule_name') ? old('rule_name') : ''),'attribute' => 'autofocus'])!!}
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                {!!view($view_path.'.builder.select',['type' => 'select','name' => 'type','label' => 'Type Rule','data' => $type_rule,'value' => (old('type') ? old('type') : ''),'attribute' => 'required'])!!}
              </div>
            </div>

          @if(isset($product))
            <div class="row">
              <div class="col-md-12">
                <label class="control-label">Spesific Product</label>
                <select class="form-control select2-multiple" name="exc_product[]" multiple>
                  <optgroup label="Product">
                   
                      @foreach($product as $pro)
                        @if($pro->id_price_rule == $price_rule->id)
                          <option value="{{$pro->id}}" {{ $pro->id_product != NULL ? 'selected' : '' }}>{{$pro->name}}</option>
                        @else
                          <option value="{{$pro->id}}">{{$pro->name}}</option>
                        @endif
                      @endforeach
                   
                  </optgroup>
                </select>

                <div class="form-control-focus"> </div>
              </div>

              <div class="col-md-12">
                <small>Note : if spesific product empty, than it will affect all product.</small>
              </div>
            </div>
          @endif

            <div class="row">
              <div class="col-md-6">
                {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Status','value' => 'y','attribute' => 'required', 'form_class' => 'pad-right'])!!}  
              </div>
            </div>
          </div>

          <div class="tab-pane" id="condition">
            <div class="alert alert-warning">
              <ul>
                  <li>You must save this rule before adding more data.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
