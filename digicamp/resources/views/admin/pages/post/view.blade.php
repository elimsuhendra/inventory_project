@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/update/{{$post->id}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <ul class="nav nav-tabs ">
          <li class="active">
            <a href="#post" data-toggle="tab" aria-expanded="true">{{trans('general.summary')}}</a>
          </li>
          <!-- <li>
            <a href="#additional" data-toggle="tab" aria-expanded="false">{{trans('general.additional')}}</a>
          </li>
          <li>
            <a href="#analytics" data-toggle="tab" aria-expanded="false">{{trans('general.analytics')}}</a>
          </li> -->
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="post">
            <div class="row">
              <div class="col-md-8">
                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'post_name','label' => 'Post Title','value' => (old('post_name') ? old('post_name') : $post->post_name),'attribute' => 'required autofocus'])!!}
                <!-- {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : $post->description),'attribute' => 'required'])!!} -->
                {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'content','class' => 'editor','label' => 'Content','value' => (old('content') ? old('content') : $post->content)])!!}
                <h4>Search Engine Optimization</h4><hr/>
                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_title','label' => 'Meta Title','value' => (old('meta_title') ? old('meta_title') : $post->meta_title)])!!}
                {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'meta_keywords','label' => 'Meta Keywords','value' => (old('meta_keywords') ? old('meta_keywords') : $post->meta_keywords)])!!}
                {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'meta_description','label' => 'Meta Description','value' => (old('meta_description') ? old('meta_description') : $post->meta_description)])!!}
              </div>
              <div class="col-md-4">
                <div class=" form-md-line-input form-md-floating-label">
                  {!!view($view_path.'.builder.file',['name' => 'thumbnail','label' => 'Thumbnail','value' => $post->thumbnail,'file_opt' => ['path' => 'components/front/images/post/'.$post->id.'/'],'type' => 'file','upload_type' => 'single-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 800 x 240 px','form_class' => 'col-md-6'])!!}
                  <label for="category_id">Category</label>
                  <select class="select2" name="category_id" id="category_id">
                    @foreach($category as $c)
                      @if(count($c->subcategory) > 0)
                        <optgroup label="{{$c->category_name}}">
                          @foreach($c->subcategory as $sc)
                            <option value="{{$sc->id}}" {{old('category_id') ? (old('category_id') == $sc->id ? 'selected' : '') : $post->category_id == $sc->id ? 'selected' : ''}}>{{$sc->category_name}}</option>
                          @endforeach
                        </optgroup>
                      @endif
                    @endforeach
                  </select>
                </div>
                <br>
               <!--  <div class="form-group form-md-line-input form-md-floating-label">
                  <label for="tag">Tag</label>
                  <select class="select2" name="tag[]" multiple="multiple">
                    @foreach($tag as $t)
                      <option value="{{$t->tag_name}}" {{old('tag') ? (in_array($t->tag_name,old('tag')) ? 'selected' : '') : in_array($t->tag_name,$post_tag) ? 'selected' : ''}}>{{$t->tag_name}}</option>
                    @endforeach
                  </select>
                </div> -->
               <!--  {!!view($view_path.'.builder.text',['type' => 'text','name' => 'publish_date','label' => 'Publish Date','value' => (old('publish_date') ? old('publish_date') : date_format(date_create(explode(' ',$post->publish_date)[0]),'d-m-Y')).' '.explode(' ',$post->publish_date)[1],'attribute' => 'readonly required','class' => 'datetimepicker'])!!} -->
                {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Publish','value' => (old('status') ? old('status') : $post->status)])!!}
                <hr/>
                <h4>Last Updated: <small>{{date_format(date_create($post->updated_at),'d-m-Y H:i:s')}}</small></h4>
                <h4>Updated By: 
                  <small>
                    @if($post->writer_type == 'a')
                      {{$post->user->name}}
                    @elseif($post->writer_type == 'w')
                      {{$post->writer->name}}
                    @endif
                  </small>
                </h4>
              </div>
            </div>
          </div>
          <!-- <div class="tab-pane" id="additional">
            <div class="form-group form-md-line-input form-md-floating-label">
              <label for="related_post">Related Post</label>
              <select class="select2" name="related_post[]" multiple="multiple">
                @foreach($all_post as $ap)
                  <option value="{{$ap->id}}" {{old('related_post') ? (in_array($ap->id,old('related_post')) ? 'selected' : '') : in_array($ap->id,$related_post) ? 'selected' : ''}}>{{$ap->post_name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="tab-pane" id="analytics">
            <div class="row">
              <div class="col-md-3">
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                  <h4 class="widget-thumb-heading">Total View</h4>
                  <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green fa fa-eye"></i>
                    <div class="widget-thumb-body">
                      <span class="widget-thumb-subtitle">View</span>
                      <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{$post->view}}">{{$post->view}}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('input,select,textarea,checkbox,.remove-single-image').prop('disabled',true);
      tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });
    });
  </script>
@endpush
@endsection
