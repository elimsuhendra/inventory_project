@extends($view_path.'.layouts.master')
@section('content')
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green">
      <i class="icon-layers font-green title-icon"></i>
      <span class="caption-subject bold uppercase"> {{$title}}</span>
    </div>
    <div class="actions">
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
  </div>
  <div class="portlet-body form">
    @include('admin.includes.errors')
    <div class="tabbable-line">
      <ul class="nav nav-tabs ">
        <li class="active">
          <a href="#summary" data-toggle="tab" aria-expanded="true">Information</a>
        </li>
        <li>
          <a href="#price" data-toggle="tab" aria-expanded="false">Price</a>
        </li>
        <li>
          <a href="#discount" data-toggle="tab" aria-expanded="false">Discount</a>
        </li>
        <li>
          <a href="#combination" data-toggle="tab" aria-expanded="false">Combination</a>
        </li>
        <li>
          <a href="#association" data-toggle="tab" aria-expanded="false">Category</a>
        </li>
        <li>
          <a href="#image_images_name" data-toggle="tab" aria-expanded="false">Image</a>
        </li>
        <li>
          <a href="#shipping" data-toggle="tab" aria-expanded="false">Shipping</a>
        </li>
        <li>
          <a href="#seo" data-toggle="tab" aria-expanded="false">SEO</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="summary">
          <div class="row">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Product Name','value' => (old('name') ? old('name') : $product->name),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'product_code','label' => 'Product Code','value' => (old('product_code') ? old('product_code') : $product->product_code),'attribute' => 'required','form_class' => 'col-md-6'])!!}
          </div>
          
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'description','label' => 'Description','attribute' => 'maxlength=400','value' => (old('description') ? old('description') : $product->description)])!!}

          {!!view($view_path.'.builder.file',['name' => 'image','label' => trans('general.image'),'value' => $product->image, 'file_opt' => ['path' => 'components/front/images/product/'.$product->id.'/'], 'type' => 'file','upload_type' => 'single-image','note' => 'Note: File Must jpeg,png,jpg,gif','form_class' => 'col-md-6 pad-left', 'validation' => 'mimes:jpeg,png,jpg,gif','tab' => 'general'])!!}

          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'content','class' => 'editor','label' => 'Content','value' => (old('content') ? old('content') : $product->content)])!!}

          {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Publish','value' => (old('status') ? old('status') : $product->status)])!!}
        </div>
        <div class="tab-pane" id="price">
          {!!view($view_path.'.builder.text',['type' => 'number','name' => 'price','label' => 'Price','value' => (old('price') ? old('price') : $product->price)])!!}
        </div>
        <div class="tab-pane" id="discount">
             @if(count($discount) > 0)
              {!!view($view_path.'.builder.select',['type' => 'select','name' => 'reduction_type', 'data' => $discount_type,'value' => (old('discount_type') ? old('discount_type') : $discount->reduction_type),'label' => 'Discount Type','class' => 'select2'])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'reduction_amount','label' => 'Amount','value' => (old('amount') ? old('amount') : $discount->reduction_amount)])!!}

               @php
                $get_valid_from   = date_create($discount->valid_from);
                $get_valid_to     = date_create($discount->valid_to);
                $conv_valid_from  = date_format($get_valid_from,"d-m-Y H:i:s");
                $conv_valid_to    = date_format($get_valid_to,"d-m-Y H:i:s");
              @endphp

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_from','label' => 'Valid From','value' => (old('valid_from') ? old('valid_from') : $conv_valid_from),'class' => 'datetimepicker', 'form_class' => 'col-md-6 pad-left'])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_to','label' => 'Valid To','value' => (old('valid_to') ? old('valid_to') : $conv_valid_to),'class' => 'datetimepicker','form_class' => 'col-md-6 pad-right'])!!}

              {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Discount On','n' => 'Discount Off'],'name' => 'discount_status','label' => 'Status','value' => (old('status') ? old('status') : $discount->status)])!!} 
            @else
              {!!view($view_path.'.builder.select',['type' => 'select','name' => 'reduction_type', 'data' => $discount_type,'value' => (old('discount_type') ? old('discount_type') : ''),'label' => 'Discount Type','class' => 'select2'])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'reduction_amount','label' => 'Amount','value' => (old('amount') ? old('amount') : '')])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_from','label' => 'Valid From','value' => (old('valid_from') ? old('valid_from') : ''),'class' => 'datetimepicker', 'form_class' => 'col-md-6 pad-left'])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_to','label' => 'Valid To','value' => (old('valid_to') ? old('valid_to') : ''),'class' => 'datetimepicker','form_class' => 'col-md-6 pad-right'])!!}

              {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Discount On','n' => 'Discount Off'],'name' => 'discount_status','label' => 'Status','value' => (old('status') ? old('status') : 'n')])!!} 
            @endif
        </div>
        <div class="tab-pane" id="combination">
          <div class="tab-combination-container">
            {!!view($view_path.'.builder.button',['type' => 'button', 'class' => 'btn yellow add-combination','label' => trans('general.add').' Combination'])!!}
            <div class="combination-container" style="display:none;">
              <div class="row">
                {!!view($view_path.'.builder.select',['type' => 'select','name' => 'combination_attribute', 'class' => 'combination_attribute','data' => $product_attribute,'value' => '','label' => 'Attribute','form_class' => 'col-md-6'])!!}
                {!!view($view_path.'.builder.select',['type' => 'select','name' => 'combination_value', 'class' => 'combination_value','data' => [],'value' => '','label' => 'Value','form_class' => 'col-md-6'])!!}
                {!!view($view_path.'.builder.text',['type' => 'number','name' => 'combination_stock','class' => 'combination_stock','label' => 'Stock','value' => 0,'form_class' => 'col-md-6'])!!}
                <div class="form-group form-md-line-input col-md-8">
                  <label for="combination_data">Combination Data</label>
                  <select class="form-control" name="combination_data" id="combination_data" multiple>
                  </select>
                </div>
                <div class="col-md-4">
                  {!!view($view_path.'.builder.button',['type' => 'button', 'class' => 'btn green add-attribute','label' => trans('general.add')])!!}
                  {!!view($view_path.'.builder.button',['type' => 'button', 'class' => 'btn red remove-attribute','label' => trans('general.remove')])!!}
                </div>
                <div class="clearfix"></div>           
              </div>
              <input type="hidden" class="action-tab">
              <input type="hidden" class="action-tab-value">
              <input type="hidden" class="action-change" value="n">
              {!!view($view_path.'.builder.button',['type' => 'button', 'class' => 'btn green save-combination','label' => 'Save Combination'])!!}
            </div>
            <hr/> 
            <div class="combination-data">
              <table class="table table-bordered">
                <th>Attribute</th>
                <th>Stock</th>
                @foreach($product_data_attribute_master as $pd)
                  <tr>
                    <td>
                      @foreach($pd->product_data_attribute as $pda)
                        {{$pda->product_attribute_data->attribute->attribute_name}} : {{$pda->product_attribute_data->name}}<br/>
                      @endforeach
                    </td>
                    <td>{{$pd->stock}}</td>
                  </tr>
                @endforeach
              </table>
            </div>         
          </div>
        </div>
        <div class="tab-pane" id="association">
          <div class="row">
            <div class="form-md-line-input form-md-floating-label col-md-6">
              <label for="product_category_id">Primary Category</label>
              <select class="select2" name="product_category_id" id="product_category_id">
                <option value="">--Select One--</option>
                @foreach($category as $c)
                  @if(in_array($c["id"],$product_data_category))
                    <option value="{{$c["id"]}}" {{$c["id"] == $product->product_category_id ? 'selected' : ''}}>{{$c["name"]}}</option>
                  @endif

                  @if($c["sub_category"] != "")
                    @foreach($c["sub_category"] as $sc)
                      @if(in_array($sc["id"],$product_data_category))
                        <option value="{{$sc["id"]}}" {{$sc["id"] == $product->product_category_id ? 'selected' : ''}}>{{$sc["name"]}}</option>
                      @endif

                      @if($sc["sub_sub_category"] != "")
                        @foreach($sc["sub_sub_category"] as $cc)
                          @if(in_array($cc["id"],$product_data_category))
                            <option value="{{$cc["id"]}}" {{$cc["id"] == $product->product_category_id ? 'selected' : ''}}>{{$cc["name"]}}</option>
                          @endif
                        @endforeach
                      @endif
                    @endforeach
                  @endif
                @endforeach
              </select>
            </div>
            <!-- <div class="form-md-line-input form-md-floating-label col-md-6">
              <label for="brand_id">Brand</label>
              <select class="select2" name="product_brand_id" id="brand_id">
                @foreach($brand as $br)
                  <option value="{{$br->id}}" {{old('product_brand_id') ? (old('product_brand_id') == $br->id ? 'selected' : '') : ''}}>{{$br->brand_name}}</option>
                @endforeach
              </select>
            </div> -->
          </div>
          <br/>
          <div class="form-md-line-input form-md-floating-label">
            <label for="category_id">Category</label>
            <div class="portlet light bordered">
            @foreach($category as $c)
                <div class="form-md-checkboxes">
                  <div class="md-checkbox-inline">
                    <div class="md-checkbox">
                      <input type="checkbox" id="checkbox_form_{{$c['id']}}" class="md-check product_data_category" name="product_data_dategory_id[]" data-id="{{$c['id']}}" data-value="{{$c['name']}}" value="{{$c['id']}}" {{in_array($c["id"],$product_data_category) ? 'checked' : ''}}>
                      <label for="checkbox_form_{{$c['id']}}">
                        <span></span>
                        <span class="check"></span>
                        <span class="box"></span>
                        {{$c['name']}}
                      </label>
                    </div>
                  </div>
                </div>
                
                @if($c["sub_category"] != "")
                  @foreach($c["sub_category"] as $sc)
                    <div class="form-md-checkboxes">
                      <div class="md-checkbox-inline">
                        <div class="md-checkbox" style="margin-left: 32px;">
                          <input type="checkbox" id="checkbox_form_{{$sc['id']}}" class="md-check product_data_category" name="product_data_dategory_id[]" data-id="{{$sc['id']}}" data-value="{{$sc['name']}}"  value="{{$sc['id']}}" {{in_array($sc['id'],$product_data_category) ? 'checked' : ''}}>
                          <label for="checkbox_form_{{$sc['id']}}">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span>
                            {{$sc['name']}}
                          </label>
                        </div>
                      </div>
                    </div>

                    @if($sc["sub_sub_category"] != "")
                      @foreach($sc["sub_sub_category"] as $cc)
                        <div class="form-md-checkboxes">
                          <div class="md-checkbox-inline">
                            <div class="md-checkbox" style="margin-left: 64px;">
                              <input type="checkbox" id="checkbox_form_{{$cc['id']}}" class="md-check product_data_category" name="product_data_dategory_id[]" data-id="{{$cc['id']}}" data-value="{{$cc['name']}}"  value="{{$cc['id']}}" {{in_array($cc['id'],$product_data_category) ? 'checked' : ''}}>
                              <label for="checkbox_form_{{$cc['id']}}">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                {{$cc['name']}}
                              </label>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    @endif
                  @endforeach
                @endif
              @endforeach
            </div>
          </div>
        </div>
        <div class="tab-pane" id="image_images_name">
          <div class="tab-image-container">
            {!!view($view_path.'.builder.file',['name' => 'images_name','label' => 'Images','value' => $product_data_images,'type' => 'file','file_opt' => ['path' => 'components/front/images/product/'.$product->id.'/'],'upload_type' => 'multiple-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2 MB','attribute' => 'data-upload='.url($path).'/ext/upload_image data-delete='.url($path).'/ext/delete_image data-file_path=components/front/images/product/'.$product->id.'/ data-id='.$product->id.' data-name_real=multiple-images'])!!}
          </div>
        </div>
        <div class="tab-pane" id="shipping">
          <div class="row">
            <!-- {!!view($view_path.'.builder.text',['type' => 'text','name' => 'width','label' => 'Width (Cm)','value' => (old('width') ? old('width') : $product->width),'attribute' => 'required','form_class' => 'col-md-6'])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'height','label' => 'Height (Cm)','value' => (old('height') ? old('height') : $product->height),'attribute' => 'required','form_class' => 'col-md-6'])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'depth','label' => 'Depth (Cm)','value' => (old('depth') ? old('depth') : $product->depth),'attribute' => 'required','form_class' => 'col-md-6'])!!} -->
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'weight','label' => 'Weight (Kg)','value' => (old('weight') ? old('weight') : $product->weight),'attribute' => 'required','form_class' => 'col-md-6'])!!}
          </div>
      <!--     {!!view($view_path.'.builder.text',['type' => 'number','name' => 'additional_fees','label' => 'Additional shipping fees (for a single item)','value' => (old('additional_fees') ? old('additional_fees') : $product->additional_fees)])!!} -->
        </div>
        <div class="tab-pane" id="seo">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_title','label' => 'Meta Title','value' => (old('meta_title') ? old('meta_title') : $product->meta_title)])!!}
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'meta_keywords','label' => 'Meta Keywords','value' => (old('meta_keywords') ? old('meta_keywords') : $product->meta_keywords)])!!}
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'meta_description','label' => 'Meta Description','value' => (old('meta_description') ? old('meta_description') : $product->meta_description)])!!}
        </div>
      </div>
    </div>
  </div>
@push('scripts')

@endpush
@push('custom_scripts')
    <script>
      $(document).ready(function(){
        //Product Data Category
        $(document).on('click','.product_data_category',function(){
          a = $('.product_data_category:checked');
          b = '<option value="">--Select Category--</option>';
          $.each(a,function(i,v){
            b += '<option value="'+$(this).data('id')+'">'+$(this).data('value')+'</option>';
          });
          $('#product_category_id').html(b).val('').change();
        });

        //Attribute
        $(document).on('click','.add-combination',function(){
          $('.action-tab').val('create');
          $('.combination_stock').val(0);
          $('#combination_data').html('');
          $('.action-tab-value').val('');
          $('.combination-container').slideDown();
        });

        $(document).on('change','.combination_attribute',function(){
          a = $(this).val();
          b = {id:a};
          $.postdata('{{url($path.'/ext/attribute_value')}}',b).success(function(data){
            c = data.value;
            d = '<option value="">--Select Value--</option>';
            $.each(c,function(i,v){
              d += '<option value="'+i+'">'+v+'</option>';
            });
            $('.combination_value').html(d);
          });
        })

        $(document).on('click','.add-attribute',function(){
          a = $('.combination_attribute');
          b = $('.combination_value');
          if(a.val() && b.val()){
            c   = $('.combination_attribute option:selected').text();
            d   = $('.combination_value option:selected').text();
            //Check data
            temp_attr  = [];
            temp_value = [];
            $('#combination_data > option').each(function(i,v){
              temp_attr.push(String($(this).data('attr')));
              temp_value.push(String($(this).data('value')));
            });
            check_attr    = jQuery.inArray(a.val(),temp_attr);
            check_value   = jQuery.inArray(b.val(),temp_value);
            if(check_value < 0){
              if(check_attr < 0){
                data   = '<option data-attr="'+a.val()+'" data-value="'+b.val()+'">'+c+' : '+d+'</option>';
                $('.action-change').val('y');
                $('#combination_data').append(data);
              }else{
                alert('Cannot use more than 1 attribute');
              }
            }else{
              alert('Data already exist');
            }
          }else{
            alert('Select Attribute & Value');
          }
        });

        $(document).on('click','.remove-attribute',function(){
          $('.action-change').val('y');
          $('#combination_data option:selected').remove();
        });

        $(document).on('click','.save-combination',function(){
          action        = $('.action-tab').val();
          action_val    = $('.action-tab-value').val();
          action_change = $('.action-change').val();
          a = $('#combination_data option').length;
          if(a > 0){
            b = [];
            $('#combination_data > option').each(function(i,v){
              b.push($(this).data('value'));
            });
            c = $('.combination_stock').val();
            if(action == 'create'){
              url_action  = '{{url($path.'/ext/save_combination')}}';
              d = {id:{{$product->id}},data:b,stock:c};
            }else{
              url_action  = '{{url($path.'/ext/update_combination')}}';
              d = {id:{{$product->id}},data:b,combination_id:action_val,stock:c,action_change:action_change};
            }
            $.postdata(url_action,d).success(function(data){
              if(data.status == 'continue'){
                $('#combination').load($.cur_url()+' .tab-combination-container');
              }else{
                alert('Combination already exist');
              }
            });
          }else{
            alert('Please insert combination data');
          }
        });

        $(document).on('click','.edit-combination',function(){
          a   = $(this).data('id');
          b   = {id:a};
          $.postdata('{{url($path.'/ext/edit_combination')}}',b).success(function(data){
            b = '';
            $.each(data.combination.product_data_attribute,function(i,v){
              b += '<option data-attr="'+v.product_attribute_data.attribute.id+'" data-value="'+v.product_attribute_data.id+'">'+v.product_attribute_data.attribute.attribute_name+' : '+v.product_attribute_data.name+'</option>';
            });
            $('.combination_stock').val(data.combination.stock);
            $('#combination_data').html('');
            $('#combination_data').append(b);
            $('.action-tab').val('edit');
            $('.action-tab-value').val(a);
            $('.combination-container').slideDown();
          });
        });

        $(document).on('click','.delete-combination',function(){
          a = $(this);
          b = a.data('id');
          c = {id:b};
          $.postdata('{{url($path.'/ext/delete_combination')}}',c).success(function(data){
            $(a).closest('tr').remove();
          });
        });

        $(window).keydown(function(event){
          if(event.keyCode == 13) {
            event.preventDefault();
            return false;
          }
        });
        $('input,select,textarea,checkbox,.remove-single-image,button').prop('disabled',true);
        $('a > button').prop('disabled',false);
        tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });
      });
    </script>
@endpush
@endsection
