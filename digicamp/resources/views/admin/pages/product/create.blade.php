@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <ul class="nav nav-tabs ">
          <li class="active">
            <a href="#summary" data-toggle="tab" aria-expanded="true">Information</a>
          </li>
          <li>
            <a href="#price" data-toggle="tab" aria-expanded="false">Price</a>
          </li>
          <li>
            <a href="#discount" data-toggle="tab" aria-expanded="false">Discount</a>
          </li>
          <li>
            <a href="#combination" data-toggle="tab" aria-expanded="false">Combination</a>
          </li>
          <li>
            <a href="#association" data-toggle="tab" aria-expanded="false">Category</a>
          </li>
          <li>
            <a href="#image_images_name" data-toggle="tab" aria-expanded="false">Image</a>
          </li>
          <li>
            <a href="#shipping" data-toggle="tab" aria-expanded="false">Shipping</a>
          </li>
          <li>
            <a href="#seo" data-toggle="tab" aria-expanded="false">SEO</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="summary">
          <div class="row">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Product Name','value' => (old('name') ? old('name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'product_code','label' => 'Product Code','value' => (old('product_code') ? old('product_code') : ''),'form_class' => 'col-md-6'])!!}
          </div>
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'description','label' => 'Description','attribute' => 'maxlength=400','value' => (old('description') ? old('description') : '')])!!}

          {!!view($view_path.'.builder.file',['name' => 'image','label' => trans('general.image'),'value' => '','type' => 'file','upload_type' => 'single-image','note' => 'Note: File Must jpeg,png,jpg,gif','form_class' => 'col-md-6 pad-left', 'validation' => 'mimes:jpeg,png,jpg,gif','tab' => 'general'])!!}
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'content','class' => 'editor','label' => 'Content','value' => (old('content') ? old('content') : '')])!!}
          </div>
          <div class="tab-pane" id="price">
            <div class="alert alert-warning">
                <ul>
                    <li>You must save this product before adding more data.</li>
                </ul>
            </div>
          </div>
          <div class="tab-pane" id="discount">
            <div class="alert alert-warning">
                <ul>
                    <li>You must save this product before adding more data.</li>
                </ul>
            </div>
          </div>
          <div class="tab-pane" id="combination">
            <div class="alert alert-warning">
                <ul>
                    <li>You must save this product before adding more data.</li>
                </ul>
            </div>
          </div>
          <div class="tab-pane" id="association">
            <div class="alert alert-warning">
                <ul>
                    <li>You must save this product before adding more data.</li>
                </ul>
            </div>
          </div>
          <div class="tab-pane" id="image_images_name">
            <div class="alert alert-warning">
                <ul>
                    <li>You must save this product before adding more data.</li>
                </ul>
            </div>
          </div>
          <div class="tab-pane" id="shipping">
            <div class="alert alert-warning">
                <ul>
                    <li>You must save this product before adding more data.</li>
                </ul>
            </div>
          </div>
          <div class="tab-pane" id="seo">
            <div class="alert alert-warning">
                <ul>
                    <li>You must save this product before adding more data.</li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
    <script>

    </script>
@endpush
@endsection
