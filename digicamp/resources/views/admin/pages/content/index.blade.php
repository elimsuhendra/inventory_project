@extends($view_path.'.layouts.master')
@section('content')
@push('css')
  <link href="{{asset('components/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('components/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endpush
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-green">
      <i class="icon-layers font-green title-icon"></i>
      <span class="caption-subject bold uppercase"> {{$web_name}}</span>
    </div>
  </div>
  <div class="portlet-body form">
    @include('admin.includes.errors')
    <div class="tabbable-line">
      <ul class="nav nav-tabs ">
        <li class="active">
          <a href="#general" data-toggle="tab" aria-expanded="true">General</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="general">
          <div class="row">
            <div class="col-md-6">
              <form role="form" method="post" action="{{url($path)}}/ext/add_new_content_array" data-ask="n" enctype="multipart/form-data">
                <input type="hidden" name="type" value="single">
                <h4>Banner (Home Banner)</h4>
                <div class="form-group">
                  <div class="row">
                    {!!view($view_path.'.builder.file',['name' => 'image','label' => 'Image','type' => 'file','file_opt' => ['path' => 'components/front/images/content/'],'upload_type' => 'single-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 300 x 300 px','form_class' => 'col-md-6'])!!}
                  </div>
                  <div class="row">
                    {!!view($view_path.'.builder.text',['type' => 'text','name' => 'data[title]','label' => 'Title','attribute' => 'required','form_class' => 'col-md-6'])!!}
                    {!!view($view_path.'.builder.text',['type' => 'text','name' => 'data[button_text]','label' => 'Button Text','attribute' => 'required','form_class' => 'col-md-6'])!!}
                  </div>
                  <div class="clearfix"></div>
                  {!!view($view_path.'.builder.text',['type' => 'text','name' => 'data[url]','label' => 'URL','attribute' => 'required'])!!}
                </div>
                <div class="form-group">
                  <button type="button" class="btn green save-array-table" data-name="home_banner">Add New</button>
                </div>
              </form>
            </div>
            <div class="col-md-6">
              <br/><br/><br/>
              <div id="home_banner_container">
                <table class="table table-bordered array-table" data-id="home_banner" {{$no = 1}}>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Image</th>
                      <th>URL</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($home_banner as $hb)
                      <tr>
                        <td>{{$no++}}</td>
                        <td>
                          @if(isset($hb->image))
                            <img src="{{asset('components/front/images/content')}}/{{$hb->image}}" class="content-image"></td>
                          @else
                            <img src="{{asset('components/both/images/web/none.png')}}" class="content-image"></td>
                          @endif
                        <td>{{$hb->url}}</td>
                        <td>
                          <input type="hidden" name="array_table_id[]" class="array_table_id_home_banner" value="{{$hb->id}}">
                          <a><i class="fa fa-trash fa-lg font-red-thunderbird delete-array-table" data-id="{{$hb->id}}" data-name="home_banner"></i></a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="clearfix"></div>
          <!--   <div class="col-md-6">
              <h4>Popular Tag</h4>
              <form role="form" method="post" action="{{url($path)}}/ext/add_new_content_array" data-ask="n" enctype="multipart/form-data">
                <input type="hidden" name="type" value="relation">
                <div class="form-group">
                  <select class="select2" name="data">
                    @foreach($product_category as $p)
                      <option value="{{$p->id}}">{{$p->category_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <button type="button" class="btn green save-array-table" data-name="footer_product_category">Add New</button>
                </div>
              </form>
            </div> -->
         <!--    <div class="col-md-6">
              <br/><br/><br/>
              <div id="footer_product_category_container">
                <table class="table table-bordered array-table" data-id="footer_product_category" {{$no = 1}}>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Tag Name</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($footer_product_category as $pc)
                      <tr>
                        <td>{{$no++}}</td>
                        <td>{{$pc->category_name}}</td>
                        <td>
                          <input type="hidden" name="array_table_id[]" class="array_table_id_footer_product_category" value="{{$pc->id}}">
                          <a><i class="fa fa-trash fa-lg font-red-thunderbird delete-array-table" data-id="{{$pc->id}}" data-name="footer_product_category"></i></a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@push('scripts')
  <script src="{{asset('components/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('components/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      @if ($role->view == 'n')
        $('input,select,textarea').prop('disabled',true);
      @endif
      $.extend({
        array_table:function(){
          var table = $('.array-table').DataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
              // { extend: 'print', className: 'btn dark btn-outline' },
              // { extend: 'pdf', className: 'btn green btn-outline' },
              // { extend: 'csv', className: 'btn purple btn-outline ' }
            ],
            bRetrieve: true,
            rowReorder: true,
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [20, 40, 50, 60, -1],
                [20, 40, 50, 60, "All"] // change per page values here
            ],
            "pageLength": 20,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
          });
          table.on( 'row-reorder', function ( e, diff, edit ) {
            id  = $(this).data('id');
            array_table_id    = $('.array_table_id_'+id);
            array_table_pages = [];
            $.each(array_table_id,function(i,v){
              array_table_pages.push($(this).val());
            });
            data = {'content_name':id,'data':array_table_pages};
            c = '#'+id+'_container';
            $.post('{{url($path)}}/ext/sorting_content_array',data,function(){
              $.growl_alert('Finish sorting','info');
            });
          });
        }
      });

      $.array_table();

      $(document).on('click','.save-array-table',function(){
        that  = this;
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#32c5d2',
          cancelButtonColor: '#cf1c28',
          confirmButtonText: 'Yes, Continue'
        }).then(function() {
          name  = $(that).data('name');
          a     = $(that).parents('form');
          form  = new FormData(a[0]);
          form.append('content_name',name);
          $.postformdata("{{url($path)}}/ext/add_new_content_array",form).success(function(res){
            if(res.status == 'fail'){
              if(res.text){
                text = res.text;
              }else{
                text = 'Cannot add anymore';
              }
              $.growl_alert(text,'danger');
            }else{
              c = '#'+name+'_container';
              $(c).load($.cur_url()+' '+c+' > .array-table',function(){
                $.array_table();
                $.growl_alert('Data add','info');
              });
            }
            return false;
          });
        });
      });

      $(document).on('click','.delete-array-table',function(){
        that  = this;
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#32c5d2',
          cancelButtonColor: '#cf1c28',
          confirmButtonText: 'Yes, Continue'
        }).then(function() {
          a   = $(that).data('name');
          b   = $(that).data('id');
          data = {'content_name':a,'data':b};
          $.post('{{url($path)}}/ext/delete_content_array',data,function(res){
            c = '#'+a+'_container';
            $(c).load($.cur_url()+' '+c+' > .array-table',function(){
              $.array_table();
              $.growl_alert('Data delete','info');
            });
          });
        });
        return false;
      });

      $(document).on('click','.save-field',function(){
        tinyMCE.triggerSave();
        array_field       = $('.field');
        array_field_data  = [];
        $.each(array_field,function(i,v){
          tmp = {'name': $(this).attr('name'),'val':$(this).val()};
          array_field_data.push(tmp);
        });
        data  = {data:array_field_data};
        $.post('{{url($path)}}/ext/save_field',data,function(res){
          $.growl_alert('Data save','info');
        });
      });
      $('form').submit(function() {
        return false;
      });
    });
  </script>
@endpush
@endsection
