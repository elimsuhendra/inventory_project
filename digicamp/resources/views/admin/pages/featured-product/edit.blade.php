@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$feathd->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      {!!view($view_path.'.builder.text',['name' => 'featured_name','value'=>old("featured_name") ? old("featured_name") : $feathd->featured_name,'label'=>'Featured Name','attribute' => 'required autofocus'])!!}
      {!!view($view_path.'.builder.text',['type' => 'text','name' => 'valid_time','label' => 'Valid Time','value' => old('valid_time') ? old('valid_time') : date_format(date_create($feathd->valid_from),'d/m/Y').' - '.date_format(date_create($feathd->valid_to),'d/m/Y'),'attribute' => 'readonly required','class' => 'datetimerange'])!!}
      {!!view($view_path.'.builder.radio',['name' => 'status','value'=>old('status') ? old('status') : $feathd->status,'label'=>'Display','attribute' => 'required','data' => ['y' => 'Active','n' => 'Not Active']])!!}
      <div class="row">
        <div class="form-group col-md-6 form-drag">
          <h4>All Product</h4>
          <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">  
            @foreach($category as $c)
               <div class="panel panel-default">
                  <div class="panel-heading" id="heading{{$c->id}}">
                    <h4 class="panel-title">
                      <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion{{$c->id}}" href="#collapse{{$c->id}}"> {{$c->category_name}}</a>
                    </h4>
                  </div>
                  <div id="collapse{{$c->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      @foreach($c->product_data_category as $p)
                        <div class="form-control draggable" data-parentid="{{$p->product->id}}" {{in_array($p->product->id,$tempfeatdt) ? 'style=display:none' : ''}}>
                          <a href="{{url($root_path.'/catalog/product')}}/{{$p->product->id}}/edit" target="_blank">{{$p->product->name}}</a>
                          <a class="pull-right add_product" data-id="{{$p->product->id}}" data-name="{{$p->product->name}}"><i class="fa fa-arrow-right"></i></a>
                        </div>
                      @endforeach
                    </div>
                  </div>
                </div>
            @endforeach
          </div>
        </div>
        <div class="form-group col-md-6 droppable">
          <h4>All Featured Product</h4>
          @foreach($featdt as $p)
            <div class="form-control ui-draggable ui-draggable-handle ui-draggable-dragging editable">
              <a href="{{url($root_path.'/catalog/product')}}/{{$p->product->id}}/edit" target="_blank">{{$p->product->name}}</a>
              <input type="hidden" name="product[]" value="{{$p->product->id}}">
              <a class="text-danger remove pull-right" data-id="{{$p->product->id}}"><i class="fa fa-trash"></i></a>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $(".draggable").draggable({
          cursor: 'move',
          revert: 'invalid',
          helper: 'clone',
          distance: 20
      });
      $(".droppable").droppable({
          hoverClass: 'ui-state-active',
          tolerance: 'pointer',
          accept: function (event, ui) {
              return true;
          },
          drop: function (event, ui) {
              var obj;
              var id = ui.draggable.data('parentid');
              var total = $('.editable').length;
              if ($(ui.helper).hasClass('draggable')) {
                if(total_edit() == 12){
                  $.growl_alert('Maximum product for each featured product is 12','danger');
                }else{
                  obj = $(ui.helper).clone();
                  obj.removeClass('draggable').addClass('editable').removeAttr('style').find("i").remove();
                  obj.append('<input type="hidden" name="product[]" value="'+id+'"><a class="text-danger remove pull-right" data-id="'+id+'"><i class="fa fa-trash"></i></a>');
                  $('.draggable[data-parentid="'+id+'"]').hide();
                  $(this).append(obj);
                }
              }
          }
      }).sortable({
          revert: false
      });
      $(document).on('click','.add_product',function(){
        if(total_edit() == 12){
          $.growl_alert('Maximum product for each featured product is 12','danger');
        }else{
          var id = $(this).data('id');
          var name = $(this).data('name');
          $('.draggable[data-parentid="'+id+'"]').hide();
          var a = '<div class="form-control ui-draggable ui-draggable-handle ui-draggable-dragging editable">';
          a += name+'<input type="hidden" name="product[]" value="'+id+'">';
          a += '<a class="text-danger remove pull-right" data-id="'+id+'"><i class="fa fa-trash"></i></a></div>';                               
          $('.droppable').append(a);
        }
      });
      $(document).on('click','.remove',function(){
        var id = $(this).data('id');
        $('.draggable[data-parentid="'+id+'"]').show();
        $(this).parent().remove();
      });
      function total_edit(){
        var total = $('.editable').length;
        return total;
      }
    });
  </script>
@endpush
@endsection
