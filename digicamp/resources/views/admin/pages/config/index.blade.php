@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/update" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green">
          <i class="icon-layers font-green title-icon"></i>
          <span class="caption-subject bold uppercase"> {{$title}}</span>
        </div>
        <div class="actions">
          <div class="actions">
            {!!view($view_path.'.builder.button',['type' => 'submit','label' => trans('general.submit'),'ask' => 'y'])!!}
          </div>
        </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <ul class="nav nav-tabs ">
          <li class="active">
            <a href="#settings" data-toggle="tab" aria-expanded="true">{{trans('general.web-settings')}}</a>
          </li>
          <li>
            <a href="#technical" data-toggle="tab" aria-expanded="true">{{trans('general.technical-settings')}}</a>
          </li>
          <li>
            <a href="#seo" data-toggle="tab" aria-expanded="true">{{trans('general.web-seo')}}</a>
          </li>
          <li>
            <a href="#contact" data-toggle="tab" aria-expanded="true">{{trans('general.web-contact')}}</a>
          </li>
          <li>
            <a href="#store" data-toggle="tab" aria-expanded="true">Store</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="settings">
            <div class="row">
              {!!view($view_path.'.builder.text',['type' => 'email','name' => 'web_email','label' => 'Global Email','value' => (old('web_email') ? old('web_email') : $configs->web_email),'attribute' => 'required','form_class' => 'col-md-6'])!!}
              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'web_name','label' => 'Web Name','value' => (old('web_name') ? old('web_name') : $configs->web_name),'attribute' => 'required','form_class' => 'col-md-6'])!!}

              <!-- {!!view($view_path.'.builder.text',['type' => 'text','name' => 'background_color','label' => 'Background Color','value' => (old('background_color') ? old('background_color') : $configs->background_color),'attribute' => 'required readonly','form_class' => 'col-md-6','class' => 'colorpickers'])!!}
              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'font_color','label' => 'Font Color','value' => (old('font_color') ? old('font_color') : $configs->font_color),'attribute' => 'required readonly','form_class' => 'col-md-6','class' => 'colorpickers'])!!} -->
              <div class="col-md-12">
                <div class="row">
                    <div class="form-group form-md-line-input set_no_pad_top col-md-6">
                      <label>City</label>
                      @if(isset($city))
                        <select class="form-control select2 select2-hidden-accessible" name="city">
                          <option value="">--Select Your City--</option>
                            @foreach($city->rajaongkir->results as $city)
                              <option value="{{ $city->city_id }}" {{ $configs->city == $city->city_id ? 'selected' : '' }}>{{ $city->city_name }}</option>
                            @endforeach                         
                        </select>
                      @else
                        <div style="color:red;">No data for Raja Ongkir City</div>
                      @endif
                    </div>
                  </div>    
                </div>        
              </div>
              
              <br>

              {!!view($view_path.'.builder.file',['name' => 'favicon','label' => 'Favicon','value' => $configs->favicon,'type' => 'file','file_opt' => ['path' => 'components/both/images/web/'],'upload_type' => 'single-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 30 x 30 px','form_class' => 'col-md-6'])!!}
              {!!view($view_path.'.builder.file',['name' => 'web_logo','label' => 'Web Logo','value' => $configs->web_logo,'type' => 'file','file_opt' => ['path' => 'components/both/images/web/'],'upload_type' => 'single-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px','form_class' => 'col-md-6'])!!}  
          </div>
          <div class="tab-pane" id="technical">
            <!-- {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'maintenance_mode','label' => 'Maintenance Mode','value' => (old('maintenance_mode') ? old('maintenance_mode') : $configs->maintenance_mode), 'form_class' => 'col-md-6 pad-left'])!!} -->
            <div class="row">
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Yes','n' => 'No'],'name' => 'clean_cache','label' => 'Clean cache','value' => 'n', 'form_class' => 'col-md-6 pad-right'])!!}
            </div>
            <!-- <div class="clearfix"></div> -->
            <!-- {!!view($view_path.'.builder.text',['type' => 'text','name' => 'deadline_payment','label' => 'Deadline Payment (Days)','value' => (old('deadline_payment') ? old('deadline_payment') : $configs->deadline_payment)])!!} -->
            <!-- <a href="{{url($path)}}/sync"><button type="button" class="btn green">Sync Shipping & Price Database</button></a> -->
          </div>
          <div class="tab-pane" id="seo">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'web_title','label' => 'Meta Title','value' => (old('web_title') ? old('web_title') : $configs->web_title)])!!}
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'web_keywords','label' => 'Meta Keywords','value' => (old('web_keywords') ? old('web_keywords') : $configs->web_keywords)])!!}
            {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'web_description','label' => 'Meta Description','value' => (old('web_description') ? old('web_description') : $configs->web_description)])!!}
          </div>
          <div class="tab-pane" id="store">
            <div class="row">
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'shop_info','label' => 'Shop Info','value' => (old('shop_info') ? old('shop_info') : $configs->shop_info),'form_class' => 'col-md-6'])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'clock_1','label' => 'Clock 1','value' => (old('clock_1') ? old('clock_1') : $configs->clock_1),'form_class' => 'col-md-6'])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'clock_2','label' => 'Clock 2','value' => (old('clock_2') ? old('clock_2') : $configs->clock_2),'form_class' => 'col-md-6'])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'clock_3','label' => 'Clock 3','value' => (old('clock_3') ? old('clock_3') : $configs->clock_3),'form_class' => 'col-md-6'])!!}

            {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'foot_des','class' => 'editor','label' => 'Footer Description','value' => (old('foot_des') ? old('foot_des') : $configs->foot_des),'form_class' => 'col-md-12'])!!}
            <!-- <div class="row">
              {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'shipping_returns_product','label' => 'Shipping & Returns','value' => (old('shipping_returns_product') ? old('shipping_returns_product') : $configs->shipping_returns_product),'class' => 'editor','form_class' => 'col-md-6'])!!}
              {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'terms_condition_product','label' => 'Terms & Condition','value' => (old('terms_condition_product') ? old('terms_condition_product') : $configs->terms_condition_product),'class' => 'editor','form_class' => 'col-md-6'])!!}
            </div> -->
            </div>
          </div>
          <!-- Custom Tabs -->
          <div class="tab-pane" id="contact">
            <div class="row">
              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : $configs->phone),'form_class' => 'col-md-6'])!!}

              <!-- {!!view($view_path.'.builder.text',['type' => 'text','name' => 'fax','label' => 'Fax','value' => (old('fax') ? old('fax') : $configs->fax),'form_class' => 'col-md-6'])!!} -->

              <!-- Custom Setting -->
               {!!view($view_path.'.builder.text',['type' => 'text','name' => 'gmaps','label' => 'Google Maps','value' => (old('gmaps') ? old('gmaps') : $configs->gmaps),'form_class' => 'col-md-6'])!!}

               {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'address','class' => 'editor','label' => 'Address','value' => (old('address') ? old('address') : $configs->address),'form_class' => 'col-md-12'])!!}

               {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'address2','class' => 'editor','label' => 'Address 2','value' => (old('address2') ? old('address2') : $configs->address2),'form_class' => 'col-md-12'])!!}
              <!-- Custom Setting -->
            </div>
          </div>
          <!-- End Custom Tabs -->
        </div>
      </div>
    </div>
  </div>
</form>
@push('custom_scripts')
  @if ($role->view == 'n')
    <script>
      $(document).ready(function(){
        $('input,select,textarea').prop('disabled',true);
      });
    </script>
  @endif
@endpush
@endsection
