<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<head>
		<title>Invoices</title>
		<style>
			hr{
				color:#666666;
			}

			@font-face {
			    font-family: open_sans_light;
			    src: url(../components/both/fonts/OpenSans-Light.ttf);
			}

			@font-face {
			    font-family: open_sans_bold;
			    src: url(../components/both/fonts/OpenSans-Bold.ttf);
			}

			body{
				font-family: open_sans_light;
				color:#666666;
			}
		</style>
	</head>
	<body>
		<table width="100%">
			<tr>
				<td width="10%"><img style="width:200px;" src="{{ asset('components/front/images/logo/logo.png') }}" align="center" /></td>
				<td width="90%" align="center"><h1>Detail Order</h1></td>
			</tr>
		</table>

		<table width="100%" align="top">
			<tr>
				<td width="40%" align="left"><h3><b>Invoice Number</b> : #{{ $order->order_id }}</h3></td>
				<td width="20%"></td>
				<td width="40%" align="right"><h3><b>Order Date</b> : {{ date('d - F - Y') }} </h3></td>
			</tr>
		</table>

		<table width="30%">
			<tr>
				<td><h3>Detail Product</h3></td>
			</tr>
		</table>

		<table width="100%">
			<thead>
				<tr>
					<td colspan="7"><hr></td>
				</tr>
				<tr>
					<th>Product</th>
					<th>Unit Price</th>
					<th>Quantity</th>
					<th>Weight</th>
					<th>Total Price</th>
				</tr>
				<tr>
					<td colspan="7"><hr></td>
				</tr>
			</thead>
			<tbody>
				@foreach($order->orderdt as $dt)
				<tr style="border-top:1px solid #666666;border-bottom:1px solid #666666;" align="center">
					<td>
						<p>{{ $dt->product_name }}</p>
					</td>
					<td>Rp. {{number_format($dt->price),0,',','.'}}</td>
					<td>{{ $dt->qty }}</td>
					<td>{{ $dt->total_weight }} Kg</td>
					<td>Rp. {{number_format($dt->total_price),0,',','.'}}</td>
				</tr>
				<tr>
					<td colspan="7"><hr></td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<table width="100%">
			<tr>
				<td width="30%" style="padding-right:20px;"><h3>Delivery & Payment</h3></td>
				<td width="40%" style="padding-right:20px;"><h3>Address</h3></td>
				<td width="30%" style="padding-right:20px;"><h3>Summary</h3></td>
			</tr>

			<tr>
				<td width="30%" style="vertical-align:top;">
					<p><b>Bank Description : </b></p>
					<p>{{ $order->bank_info }}</p><br>
					<p><b>Shipping :</b></p>
					<p>{{ $order->orderaddress->delivery_service_name }}</p>
				</td>
				<td width="40%" style="vertical-align:top;">
					<ul style="list-style-type:none;padding-left:0;">
						<li><b>{{ ucfirst($order->orderaddress->first_name) }}</b></li>
						<li>{{ $order->orderaddress->address }}</li>
						<li>{{ $order->orderaddress->province }} - {{ $order->orderaddress->city }} - {{ $order->orderaddress->sub_district }}</li>
						<li><b>Phone : {{ $order->orderaddress->phone }}</b></li>
						<li><b>{{ $order->orderaddress->postal_code }}</b></li>
					</ul>
				</td>
				<td width="30%" style="vertical-align:top;">
					<ul style="list-style-type:none;padding-left:0;">
						<li><b>Sub Total :</b> RP. {{number_format($order->sub_total_amount),0,',','.'}}</li>
						<li><b>Kode Unik :</b> RP. {{ $order->kode_unik }}</li>
						<li><b>Voucher :</b>  RP. {{number_format($order->voucher_amount),0,',','.'}}</li>
						<li><b>Shipping :</b> RP. {{number_format($order->shipping_amount),0,',','.'}}</li>
						<li><b>Total Weight :</b> {{ $order->total_weight }} Kg</li>
						<li><b>Total Shipping :</b> RP. {{number_format($order->total_shipping_amount),0,',','.'}}</li>
						<li><b>Total Amount : RP. {{number_format($order->total_amount),0,',','.'}}</b></li>
					</ul>
				</td>
			</tr>
		</table>

		<table width="30%">
			<tr>
				<td><h3>Konfirmasi Pembayaran</h3></td>
			</tr>
		</table>

		<table width="100%">
			<tr>
				<td width="100%" style="vertical-align:middle;">
					<p><b>Setelah melakukan pembayaran, Anda dapat melakukan konfirmasi dengan membuka halaman <a href="{{url('/')}}/confirm-payment">{{url('/')}}/confirm-payment</a> untuk melakukan konfirmasi.</b></p>
					
					<p><b>Catatan :</b></p>
					<ol type="number">
						<li><b>Lakukan pembayaran sesuai jumlah yang tercantum pada invoice.</b></li>
						<li><b>Kirimkan pembayaran ke bank yang sesuai dengan bank pilihan Anda saat memesan.</b></li>
						<li><b>Lakukan konfirmasi segera setelah pembayaran dikirimkan.</b></li>
					</ol>
				</td>
			</tr>
		</table>
	</body>
</html>
