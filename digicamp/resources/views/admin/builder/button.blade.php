@if(isset($form_class))
	<div class="form-group {{isset($form_class) ? $form_class : ''}}">
@endif
	<button type="{{isset($type) ? $type : 'button'}}" class="btn green {{isset($class) ? $class : ''}} {{(isset($ask) && $ask == 'y') ? 'sweet-alert-confirmation' : '' }}"> {!!isset($label) ? $label : 'Submit'!!}</button>
@if(isset($form_class))
	</div>
@endif	