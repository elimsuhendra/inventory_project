<div class="{{isset($form_class) ? $form_class : ''}}">
	<div class="form-group form-md-line-input">
		<label>{{isset($label) ? $label : ''}}</label>
		<select class="form-control {{isset($class) ? $class : ''}}" name="{{isset($name) ? $name : ''}}" {{isset($attribute) ? $attribute : ''}}>
			<option value="" {{isset($value) ? ($value == '' ? 'selected' : '') : ''}}>--Select {{$label}}--</option>
			@if (count($data) > 0)
				@foreach($data as $d => $q)
					<option value={{$d}} {{$value == $d ? 'selected' : ''}}>{{$q}}</option>
				@endforeach
			@endif
		<select>
		<div class="form-control-focus"> </div>
		<small>{{isset($note) ? $note : ''}}</small>
	</div>
</div>
