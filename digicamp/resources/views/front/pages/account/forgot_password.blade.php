@extends($view_path.'.layouts.master')
@section('content')
<div class="row login_con" style="background-image:url('{{ asset('components/front/images/other/login.jpg') }}');">
    <form method="POST" action="{{ url('/forgot-password/confirm-password') }}">
    {{ csrf_field() }}
	
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row login_bg">
            <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-offset-1 col-xs-10 login_cons">
				@if($status == "found")
	                <div id="log_tl">New Password</div>

	                @if (count($errors) > 0)
	                <div class="row">
	                    <div class="col-md-12 col-sm-12 col-xs-12">
	                        <div class="alert alert-danger alert-dismissable">
	                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	                            <ul>
	                                @foreach ($errors->all() as $error)
	                                    <li>{{ $error }}</li>
	                                @endforeach
	                            </ul>
	                        </div>
	                    </div>
	                </div>
	                @endif

	                <input type="hidden" name="sc_tk" value="{{ $token }}" />

	                <div class="form-group cus_group">
	                  <label>New Password </label>
	                  <input type="password" class="form-control" name="password">
	                </div>

	                <div class="form-group cus_group">
	                  <label>Confirmation Password </label>
	                  <input type="password" class="form-control" name="password_confirmation">
	                </div>

	                <button type="submit" class="btn cus_button">Submit</button>
                @else
					<div id="log_tl">Sorry Your Request Goes Wrong Or Token Already Been Used, Please Forgot Password again or Contact us in {{ $web_email }}</div>
				@endif
            </div>
        </div>
    </div>
    </form>
</div>
@endsection