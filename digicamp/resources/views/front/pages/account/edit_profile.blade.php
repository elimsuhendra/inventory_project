@extends($view_path.'.layouts.master')
@section('content')
<div class="row profile_con">
	<div class="cus_container">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 sp_con1">
					<p class="ep_1"><b>Profile</b></p>
					<a href="{{ url('/profile') }}"><p><b>Your Profile</b></p></a>
					<a href="{{ url('order-histori') }}"><p><b>Order Histori</b></p></a>
					<a href="{{ url('confirm-payment') }}"><p><b>Confirm Payment</b></p></a>
					<a href="{{ url('change-password') }}"><p><b>Change Password</b></p></a>
				</div>
			</div>

			<div class="col-md-9 col-sm-7 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 sp_con2">
					@if (count($errors) > 0)
					<div class="row">
					    <div class="alert alert-danger alert-dismissable">
					    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					</div>
					@endif

					<div class="row">
						<h2>Edit Your Profile</b></h2>
					</div>

					<div class="row">
						<form method="POST" action="{{ url('update-profile') }}">
						{{csrf_field()}}

						<div class="col-md-12 col-sm-12 ep_2">
							<div class="row">
							  	<div class="form-group">
							  		<div class="col-md-12 ep_2_1">
							  			<div class="row">
							  				<label><b>Name :</b></label>
							  			</div>
							  		</div>

							  		<div class="col-md-8">
										<div class="row">
											<input type="text" class="form-control" placeholder="Name" name="pf_name" value="{{ $result['name'] }}" />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 ep_2">
						  	<div class="row">
								<div class="form-group">
									<div class="col-md-12 ep_2_1">
										<div class="row">
								  			<label><b>Province :</b></label>
								  		</div>
									</div>
									
									<div class="col-md-6">
										<div class="row">
											<select class="form-control get-city chk_province" data-target="city-delivery" name="pf_province">
												<option value="">Select your Province</option>
												@foreach($province->rajaongkir->results as $province)
											    	<option value="{{ $province->province_id }}" data-name="{{ $province->province }}"  {{ $result['province'] == $province->province_id ? 'selected' : '' }}>{{ $province->province }}</option>
											    @endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 ep_2">
							<div class="row">
								<div class="form-group">
								    <div class="col-md-12 ep_2_1">
								    	<div class="row">
											<label><b>City :</b></label>
										</div>
								  	</div>

								  	<div class="col-md-6">
								  		<div class="row">
											<select class="form-control city-delivery city-Ndelivery get-sub_district chk_city" data-target="subdistrict" name="pf_city">
												@foreach($city as $ct)
													<option value="{{ $ct->city_id }}" {{ $result['city'] == $ct->city_id ? 'selected' : '' }}>{{ $ct->city_name }}</option>
												@endforeach
											</select>
										</div>
								   	</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 ep_2">
						  	<div class="row">
								<div class="form-group">
									<div class="col-md-12 ep_2_1">
										<div class="row">
											<label><b>Sub District :</b></label>
										</div>
									</div>

									<div class="col-md-6">
										<div class="row">
											<select class="form-control subdistrict chk_subdistrict" name="pf_subdistrict">
												@foreach($sub_district as $sd)
													<option value="{{ $sd->subdistrict_id }}" {{ $result['sub_district'] == $sd->subdistrict_id ? 'selected' : '' }}>{{ $sd->subdistrict_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
							    </div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 ep_2">
							<div class="row">
							  	<div class="form-group">
							  		<div class="col-md-12 ep_2_1">
							  			<div class="row">
							  				<label><b>Postal Code :</b></label>
							  			</div>
							  		</div>

							  		<div class="col-md-8">
										<div class="row">
											<input type="text" class="form-control" placeholder="Postal Code" name="pf_postal" value="{{ $result['postal_code'] }}" />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 ep_2">
							<div class="row">
							  	<div class="form-group">
							  		<div class="col-md-12 ep_2_1">
							  			<div class="row">
							  				<label><b>Phone :</b></label>
							  			</div>
							  		</div>

							  		<div class="col-md-8">
										<div class="row">
											<input type="text" class="form-control" placeholder="Phone" name="pf_phone" value="{{ $result['phone'] }}" />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 ep_2">
							<div class="row">
							  	<div class="form-group">
							  		<div class="col-md-12 ep_2_1">
							  			<div class="row">
							  				<label><b>Address :</b></label>
							  			</div>
							  		</div>

							  		<div class="col-md-8">
										<div class="row">
											<textarea class="form-control" placeholder="Address" name="pf_address">{{ $result['address'] }}</textarea>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-sm-12">
							<div class="row">
								<div class="form-group">
									<div class="col-md-4 col-sm-4 ep_3">
										<div class="row">
											<a href="{{ url('/profile') }}" class="ep_10">
												Back</a>
										</div>
									</div>

									<div class="col-md-4 col-sm-4 ep_3">
										<div class="row">
											<button type="submit" class="ep_4">
												Submit
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

