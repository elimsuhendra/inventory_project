@extends($view_path.'.layouts.master')
@section('content')
<div class="row histori_con">
	<div class="cus_container">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 sp_con1">
					<p class="oh_1"><b>Profile</b></p>
					<a href="{{ url('/profile') }}"><p><b>Your Profile</b></p></a>
					<a href="{{ url('order-histori') }}"><p><b>Order Histori</b></p></a>
					<a href="{{ url('shop/confirm-payment') }}"><p><b>Confirm Payment</b></p></a>
					<a href="{{ url('change-password') }}"><p><b>Change Password</b></p></a>
				</div>
			</div>

			<div class="col-md-9 col-sm-7 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 sp_con2">
					<div class="row">
						<div class="col-md-8">
							<h2>Your Order Histori</b></h2>
						</div>
					</div>

					<div class="row oh_2">
						<table class="table table-bordered table-hover center oh_2_1">
							<thead class="oh_2_2">
						      <tr>
						        <th class="center">No Invoice</th>
						        <th class="center">No Resi</th>
						        <th class="center">Status</th>
						        <th class="center">Action</th>
						      </tr>
						    </thead>
						    <tbody>
						    @foreach($order as $od)
						    	<tr style="">
						    		<td>{{ $od->order_id }}</td>
						    		<td>{{ $od->tracking_code != NULL ? $od->tracking_code : '-' }}</td>
						    		<td>{{ $od->orderstatus->order_status_name }}</td>
						    		<td>
						    			<form method="POST" action="{{ url('order-view') }}">
										{{csrf_field()}}
						    			
						    			<input type="hidden" value="{{ $od->order_id }}" name="inv" />
						    			<button type="submit" class="btn btn-info">View</button>
						    			
						    			</form>
						    		</td>
						    	</tr>
						    @endforeach
						    </tbody>	
						</table>
					</div>

					<div class="row center">
						<p class="center">{{ $order->links() }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

