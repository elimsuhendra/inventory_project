@extends($view_path.'.layouts.master')
@section('content')
<div class="row login_con" style="background-image:url('{{ asset('components/front/images/other/login.jpg') }}');">
    <form method="POST" action="{{ url('/forgot-password') }}">
    {{ csrf_field() }}

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row login_bg">
            <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-offset-1 col-xs-10 login_cons">
                <div id="log_tl">Forgot Password</div>

                @if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
                
                <div class="form-group cus_group">
                  <label>EMAIL </label>
                  <input type="email" class="form-control" name="email">
                </div>

                <button type="submit" class="btn cus_button">Submit</button>
            </div>
        </div>
    </div>
    </form>
</div>
@endsection