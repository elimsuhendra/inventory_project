<!DOCTYPE html>
<html lang="id">
	<head>
		@include($view_path.'.includes.head_shop')
	</head>
	<body>
		<div class="container cDTO"> <!-- Container -->
			<div class="row dtl_order"> <!-- Header -->
				<div class="col-md-2 col-sm-4 col-xs-4 dtl_img">
					<img src="{{ asset('components/both/images/web') }}/{{ $web_logo }}" class="img-responsive img_center" />
				</div>

				<div class="col-md-8 col-sm-8 col-xs-8 center">
					<h1>Detail Order</h1>
				</div>
			</div> <!-- End Header -->

			<div class="row dtl_head"> <!-- Header 2 -->
				<div class="col-md-7">
					<h3><b>Invoice Number</b> : #{{ $inv->order_id }}</h3>
				</div>

				<div class="col-md-5 right">
				<h3><b>Order Date</b> : 
				@php 
					$date = date_create($inv->order_date);
				@endphp

				{{ date_format($date,"d - m - Y") }} </h3>
				</div>
			</div> <!-- End Header 2 -->

			<div class="row"> <!-- Product -->
				<div class="col-md-4 dtl_heading">
					<h3>Detail Product</h3>
				</div>

				<div class="col-md-12 table-responsive">
					<table  class="table table-hover dtl_table">
						<thead>
							<tr class="dtl_th">
								<th colspan="2">Product</th>
								<th>Unit Price</th>
								<th>Quantity</th>
								<th>Weight</th>
								<th>Total Price</th>
							</tr>
						</thead>
						<tbody>
							@foreach($product as $pd)
							<tr class="dtl_td">
								<td class="dtl_td"><img class="dtl_image" src="{{ asset($pd->thumbnail) }}" /></td>
								<td class="dtl_td"><b>{{ $pd->product_name }}</b></td>
								<td class="dtl_td"><b>Rp. {{number_format($pd->price),0,',','.'}}</b></td>
								<td class="dtl_td"><b>{{ $pd->qty }}</b></td>
								<td class="dtl_td"><b>{{ $pd->total_weight }} Kg</b></td>
								<td class="dtl_td"><b>Rp. {{number_format($pd->total_price),0,',','.'}}</b></td>
							</tr>	
							@endforeach
						</tbody>
					</table>
				</div>
			</div> <!-- End Product -->

			<div class="row"> <!-- Content 2 -->
				<div class="col-md-4"> <!-- Delivery -->
					<div class="row">
						<div class="col-md-10 dtl_heading">
							<h3>Delivery & Payment</h3>
						</div>

						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<p class="dtl_bold">Bank Description : </p>
								</div>

								<div class="col-md-12">
									<p><b>{{ $inv->bank_info }}</b></p>
								</div>

								<div class="col-md-12">
									<p class="dtl_bold">Shipping :</p>
								</div>

								<div class="col-md-12">
									<p><b>{{ $address->delivery_service_name }}</b></p>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- End Delivery -->

				<div class="col-md-4">
					<div class="row">
						<div class="col-md-8 dtl_heading"> <!-- Address -->
							<h3>Address</h3>
						</div>

						<div class="col-md-12 dtl_addressL">
							<ul>
								<li class="dtl_bold"><b>{{ ucfirst($address->first_name) }}</b></li>
								<li><b>{{ $address->address }}</b></li>
								<li><b>{{ $address->province }} - {{ $address->city }} - {{ $address->sub_district }}</b></li>
								<li><b>Phone : {{ $address->phone }}</b></li>
								<li><b>Postal : {{ $address->postal_code }}</b></li>
							</ul>
						</div>
					</div>
				</div> <!-- End Address -->

				<div class="col-md-4"> <!-- Summary -->
					<div class="row">
						<div class="col-md-8 dtl_heading">
							<h3>Summary</h3>
						</div>

						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<p class="dtl_bold">Sub Total : </p>
								</div>

								<div class="col-md-6">
									<p><b>RP. {{number_format($inv->sub_total_amount),0,',','.'}}</b></p>
								</div>

								<div class="col-md-6">
									<p class="dtl_bold">Kode Unik : </p>
								</div>

								<div class="col-md-6">
									<p><b>RP. {{ $inv->kode_unik }}</b></p>
								</div>

								<div class="col-md-6">
									<p class="dtl_bold">Voucher : </p>
								</div>

								<div class="col-md-6">
									<p><b>RP. {{number_format($inv->voucher_amount),0,',','.'}}</b></p>
								</div>
								<div class="col-md-6">
									<p class="dtl_bold">Shipping : </p>
								</div>

								<div class="col-md-6">
									<p><b>RP. {{number_format($inv->shipping_amount),0,',','.'}}</b></p>
								</div>


								<div class="col-md-6">
									<p class="dtl_bold">Total Weight : </p>
								</div>

								<div class="col-md-6">
									<p><b>{{ $inv->total_weight }} Kg</b></p>
								</div>

								<div class="col-md-6">
									<p class="dtl_bold">Total Shipping : </p>
								</div>

								<div class="col-md-6">
									<p><b>RP. {{number_format($inv->total_shipping_amount),0,',','.'}}</b></p>
								</div>

								<div class="col-md-6">
									<p class="dtl_bold">Total Amount : </p>
								</div>

								<div class="col-md-6">
									<p class="dtl_bold"><b>RP. {{number_format($inv->total_amount),0,',','.'}}</b></p>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- End Summary -->
			</div> <!-- End Content 2 -->

			<div class="row"> <!-- Content -->
				<div class="col-md-4 dtl_heading">
					<h3>Konfirmasi Pembayaran</h3>
				</div>

				<div class="col-md-12">
					<p><b>Setelah melakukan pembayaran, Anda dapat melakukan konfirmasi dengan membuka halaman <a href="{{url($root_path)}}/confirm-payment">{{url($root_path)}}/confirm-payment</a> untuk melakukan konfirmasi atau Whatsapp/sms kami di nomor {{ $phone }}.</b></p>
					<p><b>Catatan :</b></p>
					<ol type="number">
						<li><b>Lakukan pembayaran sesuai jumlah yang tercantum pada invoice.</b></li>
						<li><b>Kirimkan pembayaran ke bank yang sesuai dengan bank pilihan Anda saat memesan.</b></li>
						<li><b>Lakukan konfirmasi segera setelah pembayaran dikirimkan.</b></li>
					</ol>
				</div>
			</div> <!-- End Content -->

			<div class="row dtl_button">
				<div class="col-md-offset-2 col-md-4 dtl_Cbutton">
					<a href="{{ url('order-histori') }}"><button class="btn btn-info btn-block">Back</button></a>
				</div>

				<div class="col-md-4 dtl_Cbutton">
					<a href="{{ url('checkout/order-detail') }}/{{ $inv->order_id }}"><button class="btn btn-primary btn-block">Download</button></a>
				</div>				
			</div>
		</div> <!-- End Container -->

		<script type="text/javascript" src="{{asset('components/plugins/jquery.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('components/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	</body>
</html>