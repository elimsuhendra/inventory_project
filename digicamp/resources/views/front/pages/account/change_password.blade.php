@extends($view_path.'.layouts.master')
@section('content')
<div class="row change_password_con">
	<div class="cus_container">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 sp_con1">
					<p class="ch_1"><b>Profile</b></p>
					<a href="{{ url('/profile') }}"><p><b>Your Profile</b></p></a>
					<a href="{{ url('order-histori') }}"><p><b>Order Histori</b></p></a>
					<a href="{{ url('shop/confirm-payment') }}"><p><b>Confirm Payment</b></p></a>
					<a href="{{ url('change-password') }}"><p><b>Change Password</b></p></a>
				</div>
			</div>

			<div class="col-md-9 col-sm-7 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 sp_con2">
					@if (count($errors) > 0)
					<div class="row">
					    <div class="alert alert-danger alert-dismissable">
					    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					</div>
					@endif

					<div class="row">
						<div class="col-md-8">
							<h2>Change Password</b></h2>
						</div>
					</div>
					
					<form method="POST" action="{{ url('update-password') }}">
					{{csrf_field()}}

					<div class="col-md-12 col-sm-12 ch_2">
						<div class="row">
						  	<div class="form-group">
						  		<div class="col-md-12 ch_2_1">
						  			<div class="row">
						  				<label><b>Old Password :</b></label>
						  			</div>
						  		</div>

						  		<div class="col-md-8">
									<div class="row">
										<input type="password" class="form-control" placeholder="Old password" name="old_password" />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 ch_2">
						<div class="row">
						  	<div class="form-group">
						  		<div class="col-md-12 ch_2_1">
						  			<div class="row">
						  				<label><b>New Password :</b></label>
						  			</div>
						  		</div>

						  		<div class="col-md-8">
									<div class="row">
										<input type="password" class="form-control" placeholder="Password" name="password" />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 ch_2">
						<div class="row">
						  	<div class="form-group">
						  		<div class="col-md-12 ch_2_1">
						  			<div class="row">
						  				<label><b>Confirm Password :</b></label>
						  			</div>
						  		</div>

						  		<div class="col-md-8">
									<div class="row">
										<input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12">
						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 ch_3">
									<div class="row">
										<button type="submit" class="ch_3_1">
											Submit
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

