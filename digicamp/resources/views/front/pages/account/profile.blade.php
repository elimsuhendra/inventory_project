@extends($view_path.'.layouts.master')
@section('content')
<div class="row profile_con">
	<div class="cus_container">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 sp_con1">
					<p class="pl_1"><b>Profile</b></p>
					<a href="{{ url('profile') }}"><p><b>Your Profile</b></p></a>
					<a href="{{ url('order-histori') }}"><p><b>Order Histori</b></p></a>
					<a href="{{ url('shop/confirm-payment') }}"><p><b>Confirm Payment</b></p></a>
					<a href="{{ url('change-password') }}"><p><b>Change Password</b></p></a>
				</div>
			</div>

			<div class="col-md-9 col-sm-7 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 sp_con2">
					<div class="row">
						<div class="col-md-8">
							<h2>Your Profile</b></h2>
						</div>

						@if($result['status'] == "success")
						<div class="col-md-4">
							<a href="{{ url('/edit-profile') }}"><button class="pl_2">Edit</button></a>
						</div>
						@endif
					</div>
					@if($result['status'] == "success")
					<div class="row pl_3">
						<div class="col-md-4">
							<p>Name :</p>
						</div>

						<div class="col-md-8">
							{{ $result['name'] }}
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<p>Address :</p>
						</div>

						<div class="col-md-8">
							<p>{{ $result['address'] }}</p>
							<p>{{ $result['province'] }} - {{ $result['city'] }} - {{ $result['sub_district'] }}</p>
							<p>Postal Code : {{ $result['postal_code'] }}</p>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<p>Phone :</p>
						</div>

						<div class="col-md-8">
							{{ $result['phone'] }}
						</div>
					</div>
					@else
					<form method="POST" action="{{ url('add-profile') }}">
					{{csrf_field()}}

					<div class="col-md-12 col-sm-12 pl_4">
					  	<div class="row">
							<div class="form-group">
								<div class="col-md-12 pl_5">
									<div class="row">
							  			<label><b>Province :</b></label>
							  		</div>
								</div>
								
								<div class="col-md-6">
									<div class="row">
										<select class="form-control get-city chk_province" data-target="city-delivery" name="pf_province">
											<option value="">Select your Province</option>
											@foreach($province->rajaongkir->results as $province)
										    	<option value="{{ $province->province_id }}" data-name="{{ $province->province }}">{{ $province->province }}</option>
										    @endforeach
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 pl_4">
						<div class="row">
							<div class="form-group">
							    <div class="col-md-12 pl_5">
							    	<div class="row">
										<label><b>City :</b></label>
									</div>
							  	</div>

							  	<div class="col-md-6">
							  		<div class="row">
										<select class="form-control city-delivery city-Ndelivery get-sub_district chk_city" data-target="subdistrict" name="pf_city">
											<option value="">Select Your City</option>
										</select>
									</div>
							   	</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 pl_4">
					  	<div class="row">
							<div class="form-group">
								<div class="col-md-12 pl_5">
									<div class="row">
										<label><b>Sub District :</b></label>
									</div>
								</div>

								<div class="col-md-6">
									<div class="row">
										<select class="form-control subdistrict chk_subdistrict" name="pf_subdistrict">
											<option value="">Select Your Sub District</option>
										</select>
									</div>
								</div>
						    </div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 pl_4">
						<div class="row">
						  	<div class="form-group">
						  		<div class="col-md-12 pl_5">
						  			<div class="row">
						  				<label><b>Postal Code :</b></label>
						  			</div>
						  		</div>

						  		<div class="col-md-8">
									<div class="row">
										<input type="text" class="form-control" placeholder="Postal Code" name="pf_postal" />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 pl_4">
						<div class="row">
						  	<div class="form-group">
						  		<div class="col-md-12 pl_5">
						  			<div class="row">
						  				<label><b>Phone :</b></label>
						  			</div>
						  		</div>

						  		<div class="col-md-8">
									<div class="row">
										<input type="text" class="form-control" placeholder="Phone" name="pf_phone" />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 pl_4">
						<div class="row">
						  	<div class="form-group">
						  		<div class="col-md-12 pl_5">
						  			<div class="row">
						  				<label><b>Address :</b></label>
						  			</div>
						  		</div>

						  		<div class="col-md-8">
									<div class="row">
										<textarea class="form-control" placeholder="Address" name="pf_address"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12">
						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 pl_6">
									<div class="row">
										<button type="submit" class="pl_7">
											Submit
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

