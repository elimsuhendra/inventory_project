@if($cart != 'no_data')
<div class="row cart_shop_head"> <!-- Row Data -->
    <div class="cart_container">        
        <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Column -->
            @foreach($cart as $q => $c)
            <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Stock Message -->
                <div class="st_stock_{{ $q }}"></div>
            </div> <!--  Stock Message -->
            @endforeach

            <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Success Message -->
                <div class="st_success"></div>
            </div> <!-- Success Message -->

            <div class="col-md-12 col-sm-12 col-xs-12 cart_total_up_con">
                <div class="row">
                    <div class="col-lg-offset-4 col-lg-3 col-md-offset-1 col-md-4 col-sm-12 col-xs-12 cart_total_hd">
                       {{ count($cart) }} produk di keranjang belanja
                    </div>

                    <div class="col-lg-5 col-md-7 col-sm-12 col-xs-12 cart_total_head">
                        <div class="row cart_total_up">
                            <div class="col-md-5 col-sm-5 col-xs-12 cart_total_tl">Total <span>:</span></div>
                            <div class="col-md-7 col-sm-7 col-xs-12 cart_total_prc">Rp {{number_format($checkout_data['total'])}}</div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 cart_btns">
                                <div class="col-md-12 cart_button_check">
                                    <a href="{{ url('/shop') }}"><button>Lanjutkan Belanja</button></a>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 cart_btns">
                                <div class="col-md-12 cart_con4_button">
                                    <a href="{{url('checkout')}}"><button>Lanjutkan Pembayaran</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="col-md-12 col-sm-12 cart_head"> --> <!-- Data Title -->
       <!--          <div class="col-lg-7 col-md-6 col-sm-4">
                    <p>PRODUK</p>
                </div>

                <div class="col-lg-5 col-md-6 col-sm-8">
                    <div class=" col-lg-offset-1 col-lg-5 col-md-5 col-sm-6 center">
                        <p>JUMLAH</p>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 center">
                        <p>HARGA</p>
                    </div>
                </div>
            </div> --> <!-- End Data Title -->

            @foreach($cart as $q => $c)
            <div class="col-md-12 col-sm-12 col-xs-12 flex_table" style="margin-bottom:20px;padding-bottom:10px;"> <!-- Produk -->
                <div class="col-md-2 col-sm-3 col-xs-12 cart_margin_min">
                    <img src="{{asset('components/front/images')}}/product/{{$c['data']['id']}}/{{$c['data']['images']}}" class="img-responsive img_cus1 img_center" />
                </div>

                <div class="col-md-10 col-sm-9 col-xs-12 cart_body cart_margin_min">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 flex_table">
                            <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12 cart_margin_min">
                                <p class="cart_category">{{$c['data']['category']}}</p>
                                <p class="cart_name_pro">{{$c['data']['name']}}</p>
                                <p class="cart_name_prc">Rp. {{number_format($c['data']['price']),0,',','.'}}</p>

                                <p class="cart_atr">
                                @foreach($c['data']['attribute'] as $atr_cart)
                                   {{ $atr_cart['attribute'] }} : {{ $atr_cart['name'] }} 

                                   @if(!$loop->last)
                                   ,
                                   @endif
                                @endforeach
                                </p>
                            </div>

                           <!--  <div class="col-md-2 col-sm-3 col-xs-12 center">
                                <p>Rp. {{number_format($c['data']['price']),0,',','.'}}</p>
                            </div> -->

                            <div class="col-lg-2 col-md-1 col-sm-2 col-xs-12 cart_cus_qty quantity-selector center cart_margin_min">
                                <!-- <div class="entry number-minus2 minus-product" data-id="{{$q}}">&nbsp;</div> -->
                                <input type="text" class="entry number2 qty-product cart_input cart_qty upd_cart" data-id="{{ $q }}" type="text" value="{{$c['qty']}}" />
                                <!-- <div class="entry number-plus2 plus-product" data-id="{{$q}}">&nbsp;</div> -->
                            </div>

                            <div class="col-lg-4 col-md-5 col-sm-10 col-xs-12 cart_span cart_margin_min">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <span class="cart_ttl">Total</span>
                                    </div>

                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <span class="cart_ttl_prc">Rp. {{number_format($c['data']['sub_total']),0,',','.'}}</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-offfset-4 col-md-8 col-sm-offset-4 col-sm-8 col-xs-12">
                                        <a class="remove-cart" data-id="{{$q}}">
                                            <button class="cart_button">
                                               <img src="{{asset('components/front/images')}}/other/trash.png" class="img-responsive" />  
                                               <span>Hapus</span>
                                            </button>
                                        </a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Produk -->
            @endforeach

            @if($checkout_data['coupon'] != 0)
            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom:40px;margin-bottom:20px;border-bottom:1px solid #666666;"> <!-- Voucher -->
                <div class="col-md-2 col-sm-3 col-xs-12">
                </div>

                <div class="col-md-10 col-sm-9 col-xs-12 cart_body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 flex_table">
                            <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12 cart_name_pro">
                                <p>{{ $checkout_data['coupon_name'] }}</p>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-4 center">
                                <p class="align_center">{{ $checkout_data['coupon_qty'] }}</p>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8 cart_span2 center">
                                <span>
                                    @if($checkout_data['coupon_type'] == "v")
                                        Rp. {{ number_format($checkout_data['coupon_amount']),0,',','.' }}
                                    @else 
                                        {{ $checkout_data['coupon_amount'] }} %
                                    @endif
                                </span>
                                <a class="remove_coupon" data-id="coupon"><button class="cart_button">X</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Voucher -->
            @endif

            <!-- <div class="col-md-12 col-sm-12 col-xs-12 cart_con2">
                <div class="col-md-offset-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="col-md-6 col-sm-6">
                        <a href="{{ url('/') }}"><button class="cart_button_check">Continue Shopping</button></a>
                    </div>

                    <div class="col-md-6 col-sm-6">
                        <button class="cart_button_check upd_cart">Update Shopping Cart</button>
                    </div>
                </div>
            </div> -->

            <div class="col-md-12 col-sm-12 col-xs-12 cart_con3">
                <div class="col-md-3">
                    <div class="row">
                        <h3><b>Have a Coupon Codes?</b></h3>

                        <div class="row">
                            <div class="col-md-10">
                                <input type="text" class="cart_con3_input" placeholder="Enter Code" id="ch_coupon" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <button class="cart_con3_button check_coupon">Apply Coupon</button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="coupon_message"></div>
                            </div>
                        </div>
                    </div>
                </div> <!-- first Column -->

                <div class="col-md-4">
                    <div class="row">
                        <h3><b>Estimate Shipping</b></h3>
                        <p class="cart_con3_body">Enter your destination to get a shipping estimate.</p>    
                        <input type="hidden" class="ch_weight" value="{{ $checkout_data['total_weight'] }}">

                        <h4 class="cart_con3_title">State/Province</h4>
                        <div class="row cart_con3_body">
                            <div class="col-md-8">
                                <select class="cart_con3_select get-city" name="delivery[province_id]" id="ch_province" data-target="city-delivery" placeholder="Province" required>
                                    <option value="">Select your state/Province</option>
                                    @foreach($province->rajaongkir->results as $province)
                                        <option value="{{ $province->province_id }}">{{ $province->province }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <h4 class="cart_con3_title">City</h4>
                        <div class="row cart_con3_body">
                            <div class="col-md-8">
                                <select class="cart_con3_select city-delivery get-sub_district" id="ch_city" data-target="subdistrict">
                                    <option value="">Select Your City</option>
                                </select>
                            </div>
                        </div>

                        <h4 class="cart_con3_title">Sub district</h4>
                        <div class="row cart_con3_body">
                            <div class="col-md-8">
                                <select class="cart_con3_select subdistrict get-courier" id="chk_subdistrict" data-target="courier">
                                    <option value="">Select Your Sub district</option>
                                </select>
                            </div>
                        </div>

                        <h4 class="cart_con3_title">Courier</h4>
                        <div class="row cart_con3_body">
                            <div class="col-md-8">
                                 <select class="cart_con3_select courier" id="ch_courier">
                                     <option value="">Select Your Courier</option>
                                </select>
                            </div>
                        </div>

                        <div class="row cart_con3_body">
                            <div class="col-md-6">
                                <button class="cart_con3_button check_cost">Check</button>
                            </div>
                        </div>

                        <div class="row cart_con3_body">
                            <div class="form-message"></div>
                        </div>
                    </div>
                </div> <!-- Second Column -->

                <div class="col-md-5 cart_con4">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 cart_margin_min">
                                <p class="up_desc">Total Belanja <span class="up_span">:</span></p>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 up_num cart_margin_min">
                                <p>Rp {{number_format($checkout_data['sub_total'])}}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 cart_margin_min">
                                <p class="up_desc">Shipping <span class="up_span">:</span></p>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 up_num cart_margin_min">
                                <p class="shipping_cost">Rp 0</p>
                            </div>
                        </div>

                        @if($checkout_data['coupon'] != 0) 
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 cart_margin_min">
                                <p class="up_desc">Coupon <span class="up_span">:</span></p>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 up_num cart_margin_min">
                                <p>Rp {{number_format($checkout_data['coupon'])}}</p>
                            </div>
                        </div>
                        @endif

                        <div class="row cart_con4_total">
                            <div class="col-md-6 col-sm-6 col-xs-12 cart_margin_min">
                                <p class="up_desc">Total <span class="up_span">:</span></p>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 price-data cart_margin_min">
                                <p class="up_total">Rp {{number_format($checkout_data['total'])}}</p>
                            </div>
                        </div>

                        <div class="row cart_btns_dis">
                            <div class="col-md-6 col-sm-6 col-xs-12 cart_btns cart_margin_min">
                                <div class="col-md-12 cart_button_check">
                                    <a href="{{ url('/shop') }}"><button>Lanjutkan Belanja</button></a>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 cart_btns cart_margin_min">
                                <div class="col-md-12 cart_con4_button">
                                    <a href="{{url('checkout')}}"><button>Lanjutkan Pembayaran</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- Third Column -->
            </div> <!-- End Submit Button -->
        </div> <!-- End column -->
    </div> <!-- End Container -->
</div> <!-- End Row Data -->
@else
    <div class="row center cart_empty">
        <h1>Cart Is Empty</h1>
    </div>
@endif