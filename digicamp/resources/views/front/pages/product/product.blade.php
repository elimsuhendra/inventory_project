@extends($view_path.'.layouts.master')

@push('css')
    <link rel ="stylesheet" href="{{asset('components/plugins/jquery-light-slider/css/lightslider.css')}}">
@endpush

@section('content')
<div class="row product_con">
    <div class="container">
        <div class="row">
          <div class="col-md-5 col-sm-6 col-xs-12 product_l" id="product_slide">
            <ul id="imageGallery">
                @if(count($product_image) > 0)
                  @foreach($product_image as $proimg)
                  <li data-thumb="{{ asset('components/front/images/product') }}/{{ $proimg->product_id }}/{{ $proimg->images_name }}" data-src="{{ asset('components/front/images/product') }}/{{ $proimg->product_id }}/{{ $proimg->images_name }}">
                    <img src="{{ asset('components/front/images/product') }}/{{ $proimg->product_id }}/{{ $proimg->images_name }}" class="img-responsive img_center" />
                  </li>
                  @endforeach
                @else
                  <li data-thumb="{{ asset('components/both/images/not_found/detail-not-found.png') }}" data-src="{{ asset('components/both/images/not_found/detail-not-found.png') }}">
                    <img src="{{ asset('components/both/images/not_found/detail-not-found.png') }}" class="img-responsive img_center" />
                  </li>
                @endif
            </ul>
            </div>

            <div class="col-md-offset-1 col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <input type="hidden" value="{{ $product['id'] }}" id="ids" />

                    <div class="col-md-12 col-sm-12 col-xs-12 product_name">
                       {{ $product['name'] }}
                    </div>
                </div>

                <div class="row product_price dis_flex">
                    <div class="col-lg-5 col-md-6 col-sm-8 col-xs-9">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 product_category">
                                {{ $product['category'] }}
                            </div>

                            @if($product['discount_status'] == 'y')
                                <div class="col-md-12 col-sm-12 col-xs-12 product_disc">
                                    Rp. {{ number_format($product['price']),0,',','.' }}
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 product_rl">
                                    Rp. {{ number_format($product['discount']),0,',','.' }}
                                </div>
                            @else
                                <div class="col-md-12 col-sm-12 col-xs-12 product_rl">
                                    Rp. {{ number_format($product['price']),0,',','.' }}
                                </div>
                            @endif
                        </div>
                    </div>

                    @if($product['discount_status'] == 'y')
                    <div class="col-lg-4 col-md-4 col-sm-2 col-xs-2 product_img_disc">
                        <div>
                            {{ $product['discount_amount'] }}%
                        </div>
                    </div>
                    @endif
                </div>

                <div class="row product_qty">
                    <div class="col-lg-4 col-md-5 col-sm-5 col-xs-6 quantity-selector">
                        <div class="entry number-minus">&nbsp;</div>
                        <input type="text" class="entry number product-qty" value="1" />
                        <div class="entry number-plus">&nbsp;</div>
                    </div>

                    @if(auth()->guard($guard)->check())
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 product_icon {{ $product['status_wishlist'] == 'y' ? 'home_product_icon_y' : 'home_product_icon_n' }}" data-id="{{ $product['id'] }}">
                        <div class="row">
                            <i class="fa fa-heart fa-2x" aria-hidden="true"></i>
                        </div>
                    </div>
                    @endif
                </div>  

                @foreach($product_attr as $at => $attr)
                <div class="row atr_pro_con">
                    <div class="col-md-12 col-sm-12 col-xs-12 product_attr">
                        {{ $attr['name'] }} :
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                        @foreach($attr['data'] as $atr_data)
                            @if($attr['type'] == 'color')
                            <div class="col-md-2 col-sm-3 col-xs-3 product_attr_con{{ $at }} product_attr_con {{$loop->first ? 'active' : ''}}">
                                <div class="product_attr_color product_attr_color{{ $at }} product-attr product_attr_hov {{$loop->first ? 'active' : ''}}" style="background-color:{{ $atr_data['value'] }};" data-id="{{ $atr_data['id'] }}" data-target="{{ $at }}">
                                    <div class="product_dot"></div>
                                </div>

                                <div class="product_attr_name">
                                    {!! str_replace(' ', '<br />', $atr_data['name']) !!}
                                </div>
                            </div>
                            @else
                            <div class="col-md-2 col-sm-3 col-xs-3 product_attr_con">
                                <div class="product_attr_text product_attr_text{{ $at }} product-attr product_attr_hov {{$loop->first ? 'active' : ''}}" data-id="{{ $atr_data['id'] }}" data-target="{{ $at }}">{{ $atr_data['value'] }}</div>
                            </div>
                            @endif
                        @endforeach 
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="row">
                    <input type="hidden" data-value="{{ $product['weight'] }}" class="weight" />

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row product_button">
                            <div class="col-md-6 col-sm-6 col-xs-12 cart_btns">
                                <div class="row">
                                    <div class="col-md-11 col-sm-11 col-xs-12 product_button_l">
                                        <button class="add-cart-custom" data-id="{{ $product['id'] }}">BELI SEKARANG</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 cart_btns">
                                <div class="row">
                                    <div class="col-md-11 col-sm-11 col-xs-12 product_button_r">
                                        <button class="add-cart" data-id="{{ $product['id'] }}">TAMBAH KE BAG</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row product_desc">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 product_desc_tl">
                <div class="row">
                    KETERANGAN :
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 product_desc_con">
                <div class="row">
                    {!! $product['content'] !!}
                </div>
            </div>
        </div>
    </div>
</div>

@if($product_rekomendasi != "")
<div class="row product_rekom">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 product_rekom_tl">
                <span>Produk Rekomendasi</span>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                @php 
                    $rek = 0;
                    $count_rek = count($product_rekomendasi);
                @endphp

                @foreach($product_rekomendasi as $rekom => $prd_rek)
                    @if($rek == 0)
                        <div class="row">
                    @endif

                    <div class="col-md-3 col-sm-6 col-xs-12 product_rekom_con">
                        @foreach($prd_rek->attr_id as $prd_at)
                            <input type="hidden" class="product-attr_pr_{{ $prd_rek->id }}" data-id="{{ $prd_at }}" />
                        @endforeach

                        <input type="hidden" class="product-qty" value="1" />
                        <input type="hidden" class="weight" data-value="{{ $prd_rek->weight }}" />
                    
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ url('/product') }}/{{ $prd_rek->name_alias }}">
                                    <img src="{{ asset('components/front/images/product') }}/{{ $prd_rek->id }}/{{ $prd_rek->image }}" class="img-responsive img_center" />
                                </a>

                                @if($prd_rek->discount_status == 'y' && $prd_rek->discount_type == 'p')
                                <div class="rekom_img_disc">
                                    <div class="rekom_div_disc">{{ $prd_rek->discount_amount }}%</div>
                                </div>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 rekom_product_name">
                                <a href="{{ url('/product') }}/{{ $prd_rek->name_alias }}" class="name_elipse">{{ $prd_rek->name }}</a>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 rekom_product_cat">
                                {{ $prd_rek->category }}
                            </div>

                            @if($prd_rek->discount_status == 'y')
                                <div class="col-md-12 col-sm-12 col-xs-12 rekom_product_disc">
                                    Rp {{ number_format($prd_rek->price),0,',','.' }}
                                </div>

                                @php
                                    $price = $prd_rek->discount;
                                @endphp
                            @else
                                @php
                                    $price = $prd_rek->price;
                                @endphp

                                <div class="col-md-12 col-sm-12 col-xs-12 rekom_product_disc_none">
                                    &nbsp;
                                </div>
                            @endif     
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 rekom_product_rl">
                                <div class="row rekom_product_block">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="row">
                                            Rp {{ number_format($price),0,',','.' }}
                                        </div>
                                    </div>

                                    @if(auth()->guard($guard)->check())
                                    <div class="col-md-2 col-sm-2 col-xs-2 {{ $prd_rek->status_wishlist == 'y' ? 'home_product_icon_y' : 'home_product_icon_n' }}" data-id="{{ $prd_rek->id }}">
                                        <div class="row">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>  

                            <div class="col-md-12 col-sm-12 col-xs-12 rekom_cart_button">
                                <button class="btn add-cart-front" data-id="{{ $prd_rek->id }}" data-name="pr">TAMBAH KE BAG</button>
                            </div>            
                        </div>
                    </div>

                    @php
                        $rek++;
                    @endphp

                    @if($count_rek > $lmt)
                        @php
                            $rekom = $rekom + 1;
                        @endphp

                        @if($count_rek - $rekom != 0)
                            @if($rek == $lmt)
                                </div>

                                @php
                                    $rek = 0;
                                @endphp
                            @endif
                        @else
                            </div>
                        @endif
                    @else
                        @if($rek == $count_rek)
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@push('custom_scripts')
<script type="text/javascript" src="{{asset('components/plugins/jquery-light-slider/js/lightslider.js')}}"></script>
<script>
    $(document).ready(function() {
        var slider = $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:4,
            slideMargin:0,
            enableDrag: true,
            controls: false,
            currentPagerPosition:'middle',
            thumbMargin:10,
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:1,
                        slideMove:1,
                        thumbItem:3,
                        pager: true,
                        enableTouch: true,
                      }
                },
                {
                    breakpoint:480,
                    settings: {
                        item:1,
                        slideMove:1,
                        thumbItem:3,
                        pager: true,
                        enableTouch: true,
                      }
                }
            ]
        });

        $(document).on('click', '.ct_hm_click', function(){
            var id = $(this).data('id');
            $('#form_'+id).submit();
        });

        $('.product_attr_color').on('click', function(){
            $('.load-container').fadeIn();
            var target = $(this).data('target');

            $('.product_attr_con'+target).removeClass('active');
            $('.product_attr_color'+target).removeClass('active');
            $(this).addClass('active');
            $(this).parent().addClass('active');

            var productAttr = $('.product-attr.active'); 
            var id = $('#ids').val();
            var attr = [];

            productAttr.each(function(i, v) {
                var attrId = $(this).data('id');
                attr.push(attrId);
            });

            var data = {
                    'id':id,
                    'atr': attr,
                };

            $.postdata($.root() + 'v1/atr_img', data).success(function(res) {
                $('.load-container').fadeOut();
                var result = res.result;
                var count = result.length;
                var vw = '<ul id="imageGallery">';

                if(count > 0){
                    result.forEach(function(v, i) {
                        vw += '<li data-thumb="'+ $.root() +'/components/front/images/product/'+ id +'/'+ v.images_name +'" data-src="'+ $.root() +'/components/front/images/product/'+ id +'/'+ v.images_name +'"><img src="'+ $.root() +'/components/front/images/product/'+ id +'/'+ v.images_name +'" class="img-responsive img_center" /></li>';
                    });

                 } else{
                    vw += '<li data-thumb="'+ $.root() +'/components/both/images/not_found/detail-not-found.png" data-src="'+ $.root() +'/components/both/images/not_found/detail-not-found.png"><img src="'+ $.root() +'/components/both/images/not_found/detail-not-found.png" class="img-responsive img_center" /></li>';
                }

                vw += '</ul>';

                slider.destroy();  
                $('#imageGallery').replaceWith(vw);

                if (!slider.lightSlider) {
                    slider = $('#imageGallery').lightSlider({
                        gallery:true,
                        item:1,
                        loop:true,
                        thumbItem:4,
                        slideMargin:0,
                        enableDrag: true,
                        controls: false,
                        currentPagerPosition:'middle',
                        thumbMargin:10,
                        responsive : [
                            {
                                breakpoint:800,
                                settings: {
                                    item:1,
                                    slideMove:1,
                                    thumbItem:3,
                                    pager: true,
                                    enableTouch: true,
                                  }
                            },
                            {
                                breakpoint:480,
                                settings: {
                                    item:1,
                                    slideMove:1,
                                    thumbItem:3,
                                    pager: true,
                                    enableTouch: true,
                                  }
                            }
                        ]
                    });  
                }
            }).error(function(res) {
                $('.load-container').fadeOut();
                $.growl_alert("Sorry, there a problem with your request");
            });
        });

        $('.product_attr_text').on('click', function(){
            $('.load-container').fadeIn();

            var target = $(this).data('target');
            $('.product_attr_text'+target).removeClass('active');
            $(this).addClass('active');
            $(this).parent().addClass('active');

            var productAttr = $('.product-attr.active'); 
            var id = $('#ids').val();
            var attr = [];

            productAttr.each(function(i, v) {
                var attrId = $(this).data('id');
                attr.push(attrId);
            });

            var data = {
                    'id':id,
                    'atr': attr,
                };

            $.postdata($.root() + 'v1/atr_img', data).success(function(res) {
                $('.load-container').fadeOut();
                var result = res.result;
                var count = result.length;
                var vw = '<ul id="imageGallery">';

                if(count > 0){
                    result.forEach(function(v, i) {
                        vw += '<li data-thumb="'+ $.root() +'/components/front/images/product/'+ id +'/'+ v.images_name +'" data-src="'+ $.root() +'/components/front/images/product/'+ id +'/'+ v.images_name +'"><img src="'+ $.root() +'/components/front/images/product/'+ id +'/'+ v.images_name +'" class="img-responsive img_center" /></li>';
                    });

                 } else{
                    vw += '<li data-thumb="'+ $.root() +'/components/both/images/not_found/detail-not-found.png" data-src="'+ $.root() +'/components/both/images/not_found/detail-not-found.png"><img src="'+ $.root() +'/components/both/images/not_found/detail-not-found.png" class="img-responsive img_center" /></li>';
                }

                vw += '</ul>';

                slider.destroy();  
                $('#imageGallery').replaceWith(vw);

                if (!slider.lightSlider) {
                    slider = $('#imageGallery').lightSlider({
                        gallery:true,
                        item:1,
                        loop:true,
                        thumbItem:4,
                        slideMargin:0,
                        enableDrag: true,
                        controls: false,
                        currentPagerPosition:'middle',
                        thumbMargin:10,
                        responsive : [
                            {
                                breakpoint:800,
                                settings: {
                                    item:1,
                                    slideMove:1,
                                    thumbItem:3,
                                    pager: true,
                                    enableTouch: true,
                                  }
                            },
                            {
                                breakpoint:480,
                                settings: {
                                    item:1,
                                    slideMove:1,
                                    thumbItem:3,
                                    pager: true,
                                    enableTouch: true,
                                  }
                            }
                        ]
                    });  
                }
            }).error(function(res) {
                $('.load-container').fadeOut();
                $.growl_alert("Sorry, there a problem with your request");
            });
        });
    });
</script>
@endpush
