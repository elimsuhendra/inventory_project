<div class="col-md-12 col-sm-12 col-xs-12 checkout_PNlogin">
	<div class="row">
		<form method="POST" action="{{ url('checkout/order') }}">
		{{csrf_field()}}

	<!-- 	<input type="hidden" value="" id="chk_Nprovince" name="chk_Nprovince" />
	  	<input type="hidden" value="" id="chk_Ncity" name="chk_Ncity" />
	  	<input type="hidden" value="" id="chk_Nsubdistrict" name="chk_Nsubdistrict" /> -->
	  	<input type="hidden" value="" id="chk_courier" name="chk_courier" />
	  	<input type="hidden" value="1" class="ch_weight" />
	  	<input type="hidden" name="chk_Nprovince_name" id="chk_Nprovince_name" value="">
		<input type="hidden" name="chk_Ncity_name" id="chk_Ncity_name" value="">

		<div class="col-md-12">  	
			<div class="row">
			  	<input type="hidden" value="user_none_address" name="chk_type" />

				<div class="col-md-5 col-sm-5"> <!-- Column left -->
					<div class="row">
					  	<div class="form-group">
							<input type="text" class="form-control" placeholder="Name" name="chk_name" />
						</div>

						<!-- <div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control" id="chk_country" name="chk_country">
									    <option value="indonesia">Indonesia</option>
									</select>
								</div>
							</div>
						</div> -->
						<!-- <input type="hidden" name="province_name" id="chk_province_name" value="">
						<input type="hidden" name="city_name" id="chk_city_name" value="">
						<input type="hidden" name="subdistrict_name" id="chk_subdistrict_name" value=""> -->
						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control get-city chk_province" data-target="city-delivery" name="chk_province">
										<option value="">Select your Province</option>
										@foreach($province->rajaongkir->results as $province)
									    	<option value="{{ $province->province_id }}" data-name="{{ $province->province }}">{{ $province->province }}</option>
									    @endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control city-delivery get-sub_district chk_city" data-target="subdistrict" name="chk_city">
										<option value="">Select Your City</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control subdistrict get-courier chk_subdistrict" data-target="courier" name="chk_subdistrict">
										<option value="">Select Your Sub District</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<textarea class="form-control" rows="6" placeholder="Address" name="chk_address"></textarea>
						</div>
					</div> <!-- End Row -->
				</div> <!-- End Column Left -->

				<div class="col-md-offset-1 col-md-6 col-sm-offset-1 col-sm-6"> <!-- Column Right -->
					<div class="row">
						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control courier chk_shipping" name="chk_shipping">
										<option value="">Select Your Courier</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="text" class="form-control" placeholder="Post Code" name="chk_post" />
						</div>

						<div class="form-group">
							<input type="text" class="form-control" placeholder="Phone" name="chk_phone" />
						</div>

						<div class="form-group">
							<input type="email" class="form-control" placeholder="Email" name="chk_email" value="{{ auth()->guard($guard)->user()->email }}" readonly />
						</div>

						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control chk_Npayment_method" name="chk_payment_method">
										<option value="">Payment Method</option>
									    @foreach($payment_method as $pm)
									    	<option value="{{ $pm->id }}">{{ $pm->payment_method_name}}</option>
									    @endforeach
									</select>
								</div>

								<div class="form-group">
									<select class="form-control chk_Nbank" name="chk_bank" style="display: none;">
										<!-- <option value="">Bank</option> -->
									    @foreach($bank as $b)
									    	<option value="{{ $b->id }}">{{ $b->bank_account_name}}</option>
									    @endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<textarea class="form-control" rows="6" placeholder="Notes" name="chk_notes"></textarea>
						</div>
					</div>
				</div> <!-- End Column Right -->
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
			  	<div class="row">
					<div class="col-md-offset-6 col-md-2 col-sm-offset-4 col-sm-4 col-xs-12">
						<a href="{{ url('cart') }}" class="shop_checkout_guest_buttona">Back To Cart</a>
					</div>
					
					<div class="col-md-2 col-sm-4 col-xs-12">
						<button class="shop_checkout_guest_buttonb" id="chk_confirm">Confirm Your Order</button>
					</div>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>