@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="container">
		<div class="row shop_conf_con">
			<div class="col-md-12 col-sm-12 col-xs-12">
				@if (count($errors) > 0)
				<div class="col-md-12 col-sm-12 col-xs-12">
				    <div class="alert alert-danger alert-dismissable">
				    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				</div>
				@endif

				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1 class="shop_conf_1"><b>Confirm Payment</b></h1>
				</div>
			
				<div class="col-md-12 col-sm-12 col-xs-12">
					<p class="shop_conf_2">Confirm Your Payment And Get Your Order</p>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="ch_message"></div>
				</div>

				<form method="POST" action="ch-confirm-payment" enctype="multipart/form-data">
		  		{{csrf_field()}}

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-5 col-sm-5 col-xs-12"> <!-- Column left -->
						<div class="row"> <!-- Row -->
						  	<div class="form-group">
						  		<label>No. Invoice :</label>
								<input type="number" class="form-control" id="ch_inv" autocomplete="off" placeholder="Nomor Invoice" name="ch_no_inv" />
							</div>

							<div class="form-group">
						  		<label>Name :</label>
								<input type="text" class="form-control" id="chk_names" placeholder="Name" name="ch_name" />
							</div>

							<div class="col-md-8 col-sm-8 col-xs-10">
							  	<div class="row">
								  <div class="form-group">
								  	<label>Payment Destination :</label>
									<select class="form-control" id="chk_banks" name="ch_bank">
										<option>Bank Name</option>
									</select>
								  </div>
								</div>
							</div>

							<div class="col-md-10 col-sm-10 col-xs-12">
								<div class="row">
									<div class="form-group">
										<label>Upload Your Transaction Script:</label>
										<input type="file" name="ch_file" />
									</div>
								</div>
							</div>
						</div> <!-- End Row -->
					</div> <!-- End Column Left -->

					<div class="col-md-offset-1 col-md-6 col-sm-offset-1 col-sm-6"> <!-- Column Right -->
						<div class="row">
							<div class="form-group">
							  	<label>Transfer Date :</label>
								<input type="date" class="form-control" name="ch_date" />
							</div>
						</div>

						<div class="row">
							<div class="form-group">
						  		<label>Amount :</label>
								<input type="text" class="form-control" id="chk_amounts" placeholder="Amount" name="ch_amount" />
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<label>Notes :</label>
								<textarea class="form-control" rows="5" placeholder="Notes" name="ch_notes"></textarea>
							</div>
						</div>
						

					</div> <!-- End Column Right -->
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-md-offset-6 col-md-6 col-sm-offset-6 col-sm-6">
							<div class="col-md-offset-8 col-md-4 col-sm-offset-8 col-sm-4 col-xs-12">
								<div class="row shop_conf_3">
									<button class="shop_conf_button btn">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection