@extends($view_path.'.layouts.master')
@section('content')
<div class="content-push">
  <div class="information-blocks">
  	<div class="loader-container cart-loader">
      <div class="cssload-spin-box-mini"></div>
    </div>
    
    <div class="loader-custom">
      <div id="floatingCirclesH">
        <div class="f_circleH" id="frotateH_01"></div>
        <div class="f_circleH" id="frotateH_02"></div>
        <div class="f_circleH" id="frotateH_03"></div>
        <div class="f_circleH" id="frotateH_04"></div>
        <div class="f_circleH" id="frotateH_05"></div>
        <div class="f_circleH" id="frotateH_06"></div>
        <div class="f_circleH" id="frotateH_07"></div>
        <div class="f_circleH" id="frotateH_08"></div>
      </div>
    </div>
      
    <div class="cart-parts"></div>
  </div>
</div>
@endsection