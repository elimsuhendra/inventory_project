<div class="col-md-12">
  <div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12">
      <p class="shop_checkout_guest_tl"><b>Checkout & Payment Method</b></p>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12 right">
      <button type="button" class="btn btn-default hd_btn_login2" data-toggle="modal" data-target="#modalLogin2">Sudah Punya Akun klik disini</button>
    </div>
  </div>
</div>

<!-- modal Login -->
<div id="modalLogin2" class="modal fade" role="dialog">
  <div class="modal-dialog cus_modal">

    <!-- Modal content-->
    <div class="modal-content cus_modalCon">
      <div class="modal-body">
        <div class="row flex_table">
          <div class="col-md-6 col-sm-6 col-xs-12 cus_modalHide">
            <div class="row cus_modalCenter">
              <div clas="col-md-12 col-sm-12 col-xs-12">
                <img src="{{ asset('components/both/images/web') }}/{{ $web_logo }}" class="img-responsive img_center" />
                <p>Produk Berkualitas dan dijamin Ori</p>
              </div>
            </div>

            <div class="row cus_modal_bg"></div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 cus_modal_input">
            <form method="post" action="{{url('chk-login')}}">
              @include('front.includes.errors')
              
              <div class="row cus_modal_input1">
                <p class="cus_modal_tl">LOGIN</p>

                <div class="form-group">
                  <label class="cus_modal_label">EMAIL</label>
                  <input type="email" class="form-control" placeholder="Your Email" name="email" required>
                    <div class="error-text text-danger">{{$errors->login->first('email')}}</div>
                </div>

                <div class="form-group">
                  <label class="cus_modal_label">PASSWORD</label>
                  <input type="password" class="form-control" placeholder="Your Password" name="password" required>
                    <div class="error-text text-danger">{{$errors->login->first('password')}}</div>
                </div>

                <div class="form-group flex_table"> 
                  <div class="col-sm-12 col-xs-12">
                    <div class="row cus_modal_submit">
                      <button type="submit" class="btn">Login</button>
                    </div>
                  </div>

                  <div class="col-sm-12 col-xs-12 cus_modal_link">
                    <div class="row">
                      <a href="#" data-toggle="modal" data-target="#modalForgot" data-dismiss="modal">Forgot Your Password?</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Login -->

<div class="col-md-12 shop_checkout_guest_desc">
  <p>Please fill in this form and we will help you to  the checkout process.</p>
</div>

<div class="col-md-12 shop_checkout_guest_input">
  <form method="POST" action="{{ url('checkout/order') }}">
    {{csrf_field()}}
    
  <input type="hidden" value="guest" name="chk_type" />
  <div class="col-md-5 col-sm-5"> <!-- Column left -->
    <div class="row">
        <div class="form-group">
        <input type="text" class="form-control" placeholder="Name" name="chk_name" />
      </div>

      <div class="col-md-8 col-sm-8 col-xs-8">
          <div class="row">
          <div class="form-group">
            <select class="form-control get-city chk_province" data-target="city-delivery" name="chk_province">
              <option value="">Select your Province</option>
              @foreach($province->rajaongkir->results as $province)
                  <option value="{{ $province->province_id }}" data-name="{{ $province->province }}">{{ $province->province }}</option>
                @endforeach
            </select>
          </div>
        </div>
      </div>

      <div class="col-md-8 col-sm-8 col-xs-8">
          <div class="row">
          <div class="form-group">
            <select class="form-control city-delivery get-sub_district chk_city" data-target="subdistrict" name="chk_city">
              <option value="">Select Your City</option>
            </select>
          </div>
        </div>
      </div>

      <div class="col-md-8 col-sm-8 col-xs-8">
          <div class="row">
          <div class="form-group">
            <select class="form-control subdistrict chk_subdistrict" name="chk_subdistrict">
              <option value="">Select Your Sub District</option>
            </select>
          </div>
        </div>
      </div>

      <div class="col-md-8 col-sm-8 col-xs-8">
        <div class="row">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Post Code" name="chk_post" />
          </div>
        </div>
      </div>
    </div> <!-- End Row -->     
  </div> <!-- End Column Left -->

  <div class="col-md-offset-1 col-md-6 col-sm-offset-1 col-sm-6"> <!-- Column Right -->   
    <div class="row">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Phone" name="chk_phone" />
      </div>

      <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" name="chk_email" />
      </div>

      <div class="form-group">
        <textarea class="form-control" rows="6" placeholder="Address" name="chk_address"></textarea>
      </div>
    </div>
  </div> <!-- End Column Right -->
</div>

<div class="col-md-12">
    <div class="row">
    <div class="col-md-offset-6 col-md-2 col-sm-offset-4 col-sm-4 col-xs-12">
      <a href="{{ url('/cart') }}" class="shop_checkout_guest_buttona">Back To Cart</a>
    </div>
    
    <div class="col-md-2 col-sm-4 col-xs-12">
      <button class="shop_checkout_guest_buttonb" id="chk_confirm">Confirm Your Order</button>
    </div>
  </div>
</div>
</form>

<!-- modal Login -->
<div id="modalLogin" class="modal fade" role="dialog">
  <div class="modal-dialog cus_modal">

    <!-- Modal content-->
    <div class="modal-content cus_modalCon">
      <div class="modal-body">
        <div class="row flex_table">
          <div class="col-md-6 col-sm-6 col-xs-6 cus_modalHide">
            <div class="row">
              <img src="{{ url('/components/front/images/other/modal.jpg') }}" class="img-responsive img_center img_width" />
            </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 cus_modal_input">
            <form method="post" action="{{url('do-login')}}">
              @include('front.includes.errors')
              
              <div class="row cus_modal_input1">
                <p class="cus_modal_tl">Masuk</p>

                <div class="form-group">
                  <label class="cus_modal_label">EMAIL</label>
                  <input type="email" class="form-control" placeholder="Your Email" name="email" required>
                    <div class="error-text text-danger">{{$errors->login->first('email')}}</div>
                </div>

                <div class="form-group">
                  <label class="cus_modal_label">PASSWORD</label>
                  <input type="password" class="form-control" placeholder="Your Password" name="password" required>
                    <div class="error-text text-danger">{{$errors->login->first('password')}}</div>
                </div>

                <div class="form-group flex_table"> 
                  <div class="col-sm-4">
                    <div class="row cus_modal_submit">
                      <button type="submit" class="btn">Login</button>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="row cus_modal_link">
                      <a href="#" data-toggle="modal" data-target="#modalForgot" data-dismiss="modal">Lupa Password</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Login -->

