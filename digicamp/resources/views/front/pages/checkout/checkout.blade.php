@extends($view_path.'.layouts.master')
@section('content')
<div class="row shop_checkout_con">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					@if (count($errors) > 0)
					<div class="col-md-12 col-sm-12 col-xs-12">
					    <div class="alert alert-danger alert-dismissable">
					    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					</div>
					@endif

					@if(auth()->guard($guard)->check())
						@include($view_path.'.pages.checkout.checkout_login')
					@else 
						@include($view_path.'.pages.checkout.checkout_guest')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection