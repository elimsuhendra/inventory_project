<div class="col-md-12 col-sm-12 col-xs-12 checkout_Plogin">
	<label class="checkbox-entry">
	    <input type="checkbox" name="check_out_type" class="choose-address" value="same" checked> <span class="check"></span> Use Your Address
	</label>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 checkout_Plogin ch_your_address">
	<div class="row">
		<form method="POST" action="{{ url('checkout/order') }}">
			{{csrf_field()}}

	<!-- 		<input type="hidden" value="" id="chk_Nprovince" name="chk_Nprovince" />
		  	<input type="hidden" value="" id="chk_Ncity" name="chk_Ncity" />
		  	<input type="hidden" value="" id="chk_Nsubdistrict" name="chk_Nsubdistrict" /> -->
		  	<input type="hidden" value="" id="chk_courier" name="chk_courier" />
		  	<input type="hidden" value="1" class="ch_weight" />
		  	<input type="hidden" name="province_name" id="chk_province_name2" value="">
			<input type="hidden" name="city_name" id="chk_city_name2" value="">
			<input type="hidden" name="subdistrict_name" id="chk_subdistrict_name2" value=""> 


			<div class="col-md-12">
			  	<input type="hidden" value="user" id="ch_type" name="chk_type" />

				<div class="col-md-5 col-sm-5"> <!-- Column left -->
					<div class="row">
					  	<div class="form-group">
							<input type="text" class="form-control ch_name" placeholder="Name" name="chk_name" value="{{$customer->first_name}}" readonly />
						</div>

						<!-- <div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control ch_country" id="chk_country" name="chk_country" readonly>
									    <option value="indonesia">Indonesia</option>
									</select>
								</div>
							</div>
						</div> -->

						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control chk_province ch_province" name="chk_province" id="chk_province_id" readonly>
										<option value="" {{ $customer->province_id != NULL ? 'disabled' : '' }}>Select your Province</option>
										@foreach($province->rajaongkir->results as $province)
									    	<option value="{{ $province->province_id }}" {{ $customer->province_id == $province->province_id ? 'selected' : 'disabled' }}>{{ $province->province }}</option>
									    @endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control city-delivery get-sub_district chk_city ch_city" id="chk_city_id" data-target="subdistrict" name="chk_city" readonly>
										<option value="{{ $customer->city_id != NULL ? $customer->city_id : '' }}" {{ $customer->city_id != NULL ? 'selected' : '' }}" readonly>{{ $customer->city_id != NULL ? $address['city'] : 'Select Your City' }}</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control subdistrict get-courier chk_subdistrict ch_subdistrict" data-target="courier" name="chk_subdistrict" readonly>
										<option value="{{ $customer->sub_district_id != NULL ? $customer->sub_district_id : '' }}" selected="{{ $customer->sub_district_id != NULL ? 'true' : 'false' }}" readonly>{{ $customer->sub_district_id != NULL ? $address['sub_district'] : 'Select Your Sub District' }}</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control courier chk_shipping ch_shipping" name="chk_shipping">
										<option value="">Select Your Courier</option>
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<textarea class="form-control ch_address" rows="6" placeholder="Address" name="chk_address" readonly>{{ $customer->address }}</textarea>
						</div>
					</div> <!-- End Row -->
				</div> <!-- End Column Left -->

				<div class="col-md-offset-1 col-md-6 col-sm-offset-1 col-sm-6"> <!-- Column Right -->
					<div class="row">
						<div class="form-group">
							<input type="text" class="form-control ch_post" placeholder="Post Code" name="chk_post" value="{{ $customer->postal_code }}" readonly />
						</div>

						<div class="form-group">
							<input type="text" class="form-control ch_phone" placeholder="Phone" name="chk_phone" value="{{ $customer->phone }}" readonly />
						</div>

						<div class="form-group">
							<input type="email" class="form-control ch_email" placeholder="Email" name="chk_email" value="{{ auth()->guard($guard)->user()->email }}" readonly />
						</div>

						<div class="col-md-8 col-sm-8 col-xs-8">
						  	<div class="row">
								<div class="form-group">
									<select class="form-control chk_payment_method" name="chk_payment_method" id="chk_payment_method_1">
										<option value="">Payment Method</option>
									   	 @foreach($payment_method as $pm)
								    	<option value="{{ $pm->id }}">{{ $pm->payment_method_name }}</option>
								   		@endforeach
									</select>
								</div>

								<div class="form-group">
									<select class="form-control chk_bank" name="chk_bank" style="display: none;"> 
										<!-- <option value="">Bank</option> -->
									    @foreach($bank as $b)
									    	<option value="{{ $b->id }}">{{ $b->bank_account_name}}</option>
									    @endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<textarea class="form-control" rows="9" placeholder="Notes" name="chk_notes"></textarea>
						</div>
					</div>
				</div> <!-- End Column Right -->
			</div>

			<div class="row">
				<div class="col-md-12">
				  	<div class="row">
						<div class="col-md-offset-6 col-md-2 col-sm-offset-4 col-sm-4 col-xs-12">
							<a href="{{ url('cart') }}" class="shop_checkout_guest_buttona">Back To Cart</a>
						</div>
						
						<div class="col-md-2 col-sm-4 col-xs-12">
							<button class="shop_checkout_guest_buttonb">Confirm Your Order</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@push('custom_scripts')
<script>
	$(document).on('ready',function(e){
        var id      = $('.city-delivery').val();
        var weight  = $('.ch_weight').val();

        if(id != '' && weight != '') {
	        var temp1   = '<option value="">Select Your Courier</option>'
	        var data    = {'id':id, 'weight':weight};
	        $.getdata($.root()+'v1/get-courier',data).success(function(res){
	          var prov   		= $('.chk_province  :selected').data('name');
	          var city   		= $('.chk_city  :selected').data('name');
	          var subdistrict   = $('.chk_subdistrict  :selected').data('name');
	          var courier  = res.courier;

	          $.each(courier, function(index, value) {
	            temp1      += '<option class="chk_courier" value="'+value.cost.value+'" data-name="'+ value.code.toUpperCase() + ' - ' + value.name+'">'+ value.code.toUpperCase() + ' - ' + value.name+'</option>';
	            $('.courier').html(temp1);
	        });

	          $('#chk_Nprovince').val(prov);
			  $('#chk_Ncity').val(city);
		      $('#chk_Nsubdistrict').val(subdistrict);
	        });
	    }

	    var province_id = $('#chk_province_id').val();
	    var city_id 	= $('#city_id').val();
	    if(province_id != ''){
	    	var pn = $('#chk_province_id option:selected').text();
	    	$('#chk_province_name2').val(pn);
	    }

	    if(city_id != ''){
	    	var cn = $('#chk_city_id option:selected').text();
	    	$('#chk_city_name2').val(cn);
	    }
	    // console.log(province_id);
  	});
</script>
@endpush