<div class="row foot_con1">
	<div class="col-md-12 col-sm-12 col-xs-12 foot_con1_1">
		<div class="cus_container2">
			<div class="row foot_dis">
				<div class="col-md-12 col-sm-12 col-xs-12 foot_cons2">
					<div class="row foot_cons2_1">
						<img src="{{ asset('components/both/images/web') }}/{{ $web_logo }}" class="img-responsive img_center" />
					</div>
	        	</div>

	        	<div class="col-md-12 col-sm-12 col-xs-12 foot_cons">
	        		<div class="row">
	        			<div class="col-md-12 col-sm-12 col-xs-12 foot_tl">
	        				<div class="row">
		        				<p>INFORMASI</p>
		        			</div>
	        			</div>

	        			<div class="col-md-12 col-sm-12 col-xs-12">
	        				<div class="row">
		        				<ul class="foot_tl_info">
		        					<li>
		        						<a href="{{ url('/') }}">Beranda</a>
		        					</li>

		        					@foreach($menus as $menu)
		        					<li>
		        						<a href="{{ url('/') }}/{{ strtolower($menu->pages_name_alias) }}">{{ strtolower($menu->pages_name) }}</a>
		        					</li>
	 								@endforeach
		        				</ul>
		        			</div>
	        			</div>
	        		</div>
	        	</div>

	        	<div class="col-md-12 col-sm-12 col-xs-12 foot_cons">
	        		<div class="row">
	        			<div class="col-md-12 col-sm-12 col-xs-12 foot_tl">
	        				<div class="row">
		        				<p>PEMBAYARAN</p>
		        			</div>
	        			</div>

	        			<div class="col-md-12 col-sm-12 col-xs-12">
	        				<div class="row">
		        				@foreach($bank_account as $bank)
		        				<div class="col-md-12 col-sm-12 col-xs-12">
		        					<div class="row foot_pembayaran">
			        					<img src="{{ asset('components/front/images/bank_account') }}/{{ $bank->image }}" class="img-responsive" />
			        					<p>{{ $bank->description }}</p>
										<p>An. {{ $bank->account_name }}</p>
			        				</div>
		        				</div>
		        				@endforeach
		        			</div>
	        			</div>
	        		</div>
	        	</div>

	        	<div class="col-md-12 col-sm-12 col-xs-12 foot_cons">
	        		<div class="row">
	        			<div class="col-md-12 col-sm-12 col-xs-12 foot_tl">
	        				<div class="row">
		        				<p>CUSTOMER SERVICES</p>
		        			</div>
	        			</div>

	        			<div class="col-md-12 col-sm-12 col-xs-12">
	        				@foreach($ctc as $cnt)
		        				<div class="row foot_contact">
		        					<img src="{{ asset('components/front/images/contact') }}/{{ $cnt->image }}" class="img-responsive img_center" />
									<p>{{ $cnt->value }}</p>
			        			</div>
			        		@endforeach
	        			</div>
	        		</div>
	        	</div>

	        	<div class="col-md-12 col-sm-12 col-xs-12 foot_cons">
	        		<div class="row">
	        			<div class="col-md-12 col-sm-12 col-xs-12 foot_tl">
	        				<div class="row">
		        				<p>TENTANG KAMI</p>
		        			</div>
	        			</div>

	        			<div class="col-md-12 col-sm-12 col-xs-12">
	        				<div class="row">
	        					<div class="col-md-12 col-sm-12 col-xs-12 foot_tt_k">
	        						<div class="row">
		        						<!-- <p>All Rights Reserved. &copy; 2016</p>
										<p>www.Intrac.com</p><br>
										<p>Jam Kerja:</p><br>
										<p>Senin - Jumat : 09:00 - 17:00 WIB</p>
										<p>Sabtu : 09:00 - 12:00 WIB</p><br>
										<p>Di luar jam kerja akan tetap di respon apabila memungkinkan. Terimakasih. :)</p> -->
										{!! $foot_des !!}
									</div>
	        					</div>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
			</div>
		</div>
	</div>
</div>

<div class="row foot_con2"></div>

<!-- Script -->
<script type="text/javascript" src="{{asset('components/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/swiper-slider/swiper.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/jquery.matchHeight.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/global.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/jquery.mousewheel.js')}}"></script> 
<script type="text/javascript" src="{{asset('components/front/js/jquery.jscrollpane.min.js')}}"></script>
<script src="{{asset('components/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js')}}"></script> 
<script src="{{asset('components/plugins/select2/js/select2.full.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('components/front/js/kyubi-head.js')}}"></script> 
@stack('custom_scripts')
