<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="format-detection" content="telephone=no" />
<meta http-equiv="Content-Language" content="id">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
<meta name="robots" content="noindex">
<title>{{$web_name}}</title>

<meta content="{{ $met_tl != '' ? $met_tl : $met_title }}" name="title" />
<meta content="{{ $met_key != '' ? $met_key : $met_description }}" name="keywords" />
<meta content="{{ $met_des != '' ? $met_des : $met_keywords }}" name="description" />
<meta name="geo.placename" content="Indonesia">
<meta name="geo.country" content="ID">
<meta name="language" content="Indonesian">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="root_url" content="{{url($root_path)}}/" />

<!-- Core CSS -->
<link rel="icon" href="{{asset('components/both/images/web')}}/{{$favicon}}" type="image/x-icon">
<link rel ="stylesheet" href="{{asset('components/plugins/bootstrap/css/bootstrap.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/plugins/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel ="stylesheet" href="{{asset('components/plugins/swiper-slider/swiper.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/plugins/owl-carousel/owl.carousel.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/plugins/owl-carousel/owl.theme.default.min.css')}}">
<!-- <link href="{{asset('components/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('components/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" /> -->
<link rel ="stylesheet" href="{{asset('components/front/css/style_custom.css')}}">
<link rel="stylesheet" href="{{asset('components/plugins/jquery-light-slider/css/lightslider.css')}}" />      
@stack('css')

<script type="text/javascript" src="{{asset('components/plugins/jquery.min.js')}}"></script>

