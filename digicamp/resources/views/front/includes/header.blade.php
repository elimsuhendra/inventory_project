<div class="row head1_con flex_table">
    <div class="col-md-7 col-sm-8 col-xs-12 head1_l">
        <ul class="head1_ul">
            <li><i class="fa fa-phone" aria-hidden="true"></i> {{ $phone }}</li>
            <li><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $clock_1 }}</li>
            <li>{{ $clock_2 }}</li>
            <li>{{ $clock_3 }}</li>
            <li id="header-link"><a class="font-white" href="{{ url('desain-interior') }}">Desain Interior Gratis<a/></li>
        </ul>
    </div>

    <div class="col-md-5 col-sm-4 col-xs-12 head1_r">
        <ul class="head1_ul">
            <li class="font-white dropdown hov_dropdown">
              @if(auth()->guard($guard)->check())
                <a class="dropdown-toggle font-white" data-toggle="dropdown" href="#" aria-expanded="false">Hi, {{ ucfirst(auth()->guard($guard)->user()->name) }}</a>
                <ul class="dropdown-menu dropdown-content cus_drop">
                  <li class="li_dropdown"><a class="font-white" href="{{ url('profile') }}"><b>Your Profile</b></a></li>
                  <li class="li_dropdown"><a class="font-white" href="{{ url('order-histori') }}"><b>Order History</b></a></li>
                  <li class="li_dropdown"><a class="font-white" href="{{ url('shop/confirm-payment') }}"><b>Confirm Payment</b></a></li>
                  <li class="li_dropdown"><a class="font-white" href="{{ url('wishlist') }}"><b>Wishlist</b></a></li>
                  <li class="li_dropdown"><a class="font-white" href="{{url('logout')}}"><b>Log Out</b></a></li>
                </ul>
              @else
                <a class="dropdown-toggle font-white" data-toggle="dropdown" href="#" aria-expanded="false">Account</a>
                <ul class="dropdown-menu dropdown-content cus_drop">
                  <li class="li_dropdown"><a class="font-white" href="{{url('/login')}}">Masuk</a></li>
                  <li class="li_dropdown"><a class="font-white" href="{{url('/register')}}">Daftar</a></li>
                </ul>
              @endif
            </li>
            <li class="li_cust">
              <a href="{{url('cart')}}" class="header-functionality-entry open-cart-popup floating-badge">
                <img src="{{ asset('components/front/images/other') }}/shooping_chart.png" class="img-responsive img_center" style="width: 34px;"/>
              </a>

              <div class="col-md-12 col-sm-12">
                <!-- Pop up cart -->
                @if(request::path() != 'cart' && request::path() != 'checkout')
                <div class="cart-box popup">
                  <div class="popup-container">
                      <div class="loader-container mini-cart-loader">
                        <div class="cssload-spin-box-mini"></div>
                      </div>
                      <div class="mini-cart"></div>
                  </div>
                </div>
                @endif
              </div>
            </li>
            <li id="search_in"><a class='font-white'><i class="glyphicon glyphicon-search"></i></a></li>
            <div class="search_cust">
              <form class="head2_search" method="POST" action="{{ url('/search') }}">
              {{csrf_field()}}
              
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search Product" id="hd_search" name="hd_search" autocomplete="off">

                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <b>Search</b>
                  </button>
                </div>
              </div>
            </form>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="rs_search"></div>
              </div>
            </div>  
            </div>
        </ul>
    </div>
</div>

<div class="row head2_con flex_table">
  <div class="col-md-4 col-xs-8 head2_padding">
    <a href="{{ url('/') }}">
      <img src="{{ asset('components/both/images/web') }}/{{ $web_logo }}" class="img-responsive img_center" style="width: 350px;"/>
    </a>
  </div>

  <div class="col-sm-4 col-xs-4 head_flex_1 head_2 menu-mobile" style="display:none;">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar" id="menu_button" aria-expanded="true">
      <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
    </button>
  </div>

    <div class="col-md-8 col-sm-12 col-xs-12 menu-mobile" style="display:none;">
      <nav class="navbar-collapse collapse in" id="bs-navbar" aria-expanded="true" style="">
          <ul class="nav navbar-nav" data-toggle="dropdown" href="#" aria-expanded="false"> 
            @foreach($product_category as $pc)
                  <li class="font-white dropdown hov_dropdown">
                      <a  class="dropdown-toggle font-white" data-toggle="dropdown" href="#" aria-expanded="false">
                        <img src="{{ asset('components/front/images/product_category') }}/{{ $pc->image }}" class="img-responsive img_menu2"/>
                        <p class="name_pc2 display-inline">{{$pc->category_name}}</p>
                        @php
                          $flagdropdown = 0;
                          foreach($product_subcategory2 as $ps2){
                            if($ps2->category_id == $pc->id){
                                $flagdropdown = 1;
                                echo '<i class="fa fa-plus-circle fa_cust" aria-hidden="true"></i>';
                                break;
                            }
                          }
                        @endphp
                        <!-- <i class="fa fa-plus-circle fa_cust" aria-hidden="true"></i> -->
                      </a>
                      @if($flagdropdown == 1)
                        <div class="row dropdown-menu dropdown-content cus_dropdown" style="min-width: 500px;">                  
                            <ul class="cus_drop2">
                            @foreach($product_subcategory as $key => $ps)
                              @if($ps->category_id == $pc->id)
                                <div class="col-md-4 col-sm-4 col-xs-12" style="padding-left:0;padding-top:0;padding-bottom:0;margin-bottom:5px;">
                                  <li class="li_dropdown2" style="width: -webkit-fill-available !important;"><a class="" href="{{ url('/category') }}/{{ $ps->category_name_alias }}">{{$ps->category_name}}</a></li>
                                </div>
                              @endif
                            @endforeach 
                            </ul>
                        </div>
                      @endif                   
                  </li>
            @endforeach 
          </ul>
      </nav>
    </div>

    <div class="col-md-8 col-sm-12 col-xs-12 head2_padding category_menu_overflow" style="">
        <div class="col-menu" style="min-width: 500px;">
          <div class="col-md-12">
            <ul class="nav navbar-nav head1_ul head1_ul_cust">
              @foreach($product_category as $pc)
                  <li class="font-white dropdown hov_dropdown">
                      <a class="dropdown-toggle font-white dt_cust" id="dt_cust-{{$pc->id}}" data-toggle="dropdown" href="{{ url('/test') }}" aria-expanded="false">
                        <img src="{{ asset('components/front/images/product_category') }}/{{ $pc->image }}" class="img-responsive img_center img_menu" id="img_menu_id-{{$pc->id}}"/>
                        <img src="{{ asset('components/front/images/product_category') }}/{{ $pc->image_active }}" class="img-responsive img_center img_menu img_active_menu" id="img_active_menu_id-{{$pc->id}}" style="display: none;" />
                        <p class="name_pc">{{$pc->category_name}}</p>
                      </a>
                      <div class="row dropdown-menu dropdown-content cus_dropdown" style="min-width: 500px;">                  
                          <ul class="cus_drop2">
                          @foreach($product_subcategory as $key => $ps)
                            @if($ps->category_id == $pc->id)
                              <div class="col-md-4 col-sm-4 col-xs-12" style="padding-left:0;padding-top:0;padding-bottom:0;margin-bottom:5px;">
                                <li class="li_dropdown2" style="width: -webkit-fill-available !important;"><a class="" href="{{ url('category') }}/{{ $ps->category_name_alias}}">{{$ps->category_name}}</a></li>
                              </div>
                            @endif
                          @endforeach 
                          </ul>
                      </div>                   
                  </li>
              @endforeach
            </ul>
      </div>
    </div>
      <!-- Swiper -->
      <!-- <div class="swiper-container swiper-1" style="display:none;">
        <div class="swiper-wrapper">
          <div class="swiper-slide">Slide 1</div>
          <div class="swiper-slide">Slide 2</div>
          <div class="swiper-slide">Slide 3</div>
          <div class="swiper-slide">Slide 4</div>
          <div class="swiper-slide">Slide 5</div>
          <div class="swiper-slide">Slide 6</div>
          <div class="swiper-slide">Slide 7</div>
          <div class="swiper-slide">Slide 8</div>
          <div class="swiper-slide">Slide 9</div>
          <div class="swiper-slide">Slide 10</div>
        </div>
        <div class="swiper-pagination-1"></div>
      </div> -->

      <!-- <ul id="lightSlider">
        <li>
            <h3>First Slide</h3>
            <p>Lorem ipsum Cupidatat quis pariatur anim.</p>
        </li>
        <li>
            <h3>Second Slide</h3>
            <p>Lorem ipsum Excepteur amet adipisicing fugiat velit nisi.</p>
        </li>
        <li>
            <h3>Second Slide</h3>
            <p>Lorem ipsum Excepteur amet adipisicing fugiat velit nisi.</p>
        </li>
      </ul> -->
  </div>
</div>

<div class="row flex_table head_3_con">
  <!-- <div class="col-md-2 col-sm-2 col-xs-12 head_3_mn">
      <a href="{{ url('/') }}" class="center {{ Request::segment(1) == '' ? 'head_3_mn_active' : '' }}"><p>Beranda</p></a>
      <div class="head_under"></div>
  </div> -->

  <!-- @foreach($menus as $menu)
    <div class="col-md-2 col-sm-2 col-xs-12 head_3_mn collapse">
        <a href="{{ url('/') }}/{{ $menu->pages_name_alias }}" class="center {{ Request::segment(1) == $menu->pages_name_alias ? 'head_3_mn_active' : '' }}"><p>{{ $menu->pages_name }}</p></a>
        <div class="head_under"></div>
    </div>
  @endforeach -->
</div>

@push('custom_scripts')
<script>
$(".btn-primary").click(function(){
    $(".collapse").collapse('toggle');
});

$(document).on('click', '#search_in', function(){
    var windowWidth = $(window).width();
    if(windowWidth <= 991){
        $(this).css('display', 'none');
    }
    $('.search_cust').css('display', 'block');
    $('#search_input').focus();
});

$(document).mouseup(function(e) {
    var windowWidth = $(window).width();
    var container = $(".search_cust");

    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.hide();
        if(windowWidth <= 991){
            $('#search_in').css('display', 'block');
        }
    }
});

$(document).ready(function(){
  $('.dropdown-submenu a.test').on("click", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
  });


  menuHoverClick(0); //active = 1, deactive = 0; on click and hover
  function menuHoverClick($active){
    if($active == 1){
        var flagClick = 0;
      $(".dt_cust").click(function(e) {
          flagClick = $(this).attr('id');
      });


      $(".dt_cust").mouseover(function(e) {
          var id = $(this).attr('id');
          console.log(id);
          id = id.split('-');
          console.log(id);

          $(".img_menu").css("display", "block");
          $(".img_active_menu").css("display", "none");
          $("#img_menu_id-"+id[1]).css("display", "none");
          $('#img_active_menu_id-'+id[1]).css("display", "block");
      })

      $(".dt_cust").mouseout(function(e) {
          var id = $(this).attr('id');
          console.log(id);
          id = id.split('-');
          console.log(id);

          if(flagClick == 0){
              $(".img_menu").css("display", "block");
              $(".img_active_menu").css("display", "none");
              $("#img_menu_id-"+id[1]).css("display", "block");
              $('#img_active_menu_id-'+id[1]).css("display", "none");
          }else{
              $(".img_menu").css("display", "block");
              $(".img_active_menu").css("display", "none");
              $("#img_menu_id-"+id[1]).css("display", "none");
              $('#img_active_menu_id-'+id[1]).css("display", "block");
          }
      })
    }
  }
  

 // var swiper = new Swiper('.swiper-1', {
 //    slidesPerView: 4,
 //    spaceBetween: 30,
 //    loop: true,
 //    pagination: {
 //      el: '.swiper-pagination-1',
 //      clickable: true,
 //    },
 //  });

  // $("#lightSlider").lightSlider(); 
});

</script>
@endpush